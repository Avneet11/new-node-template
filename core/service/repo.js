

import { Context } from '../../helpers/context';
/**
 * @export
 * @class CoreRepo
 */
export class CoreRepo {
  /**
   * Creates an instance of CoreRepo.
   * @param {*} model
   * @memberof CoreRepo
   */
  constructor(model) {
    if (typeof model.modelName === 'undefined') {
    }

    this.model = model;
    this.context = new Context();
  }
  create(data) {
    return this.model.create(data);
  }
  findOne(data) {
    return this.model.findOne(data);
  }
  find(data) {
    return this.model.find(data);
  }

  findOneAndUpdate(key, value, options) {
    return this.model.findOneAndUpdate(key, value, options).exec();
  }

  findByIdAndUpdate(key, value, options) {
    return this.model.findByIdAndUpdate(key, value, options).exec();
  }

  deleteOne(id) {
    return this.model.deleteOne(id);
  }
}
