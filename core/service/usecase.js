


import { Context } from "../../helpers/context";
import { AppError } from "../errors";
import { CoreRepo } from "./repo";

export class CoreUsecase {
  /**
   * Creates an instance of CoreUsecase.
   * @param {CoreRepo} repo
   * @memberof CoreUsecase
   */
  constructor(repo) {
    if (!(repo instanceof CoreRepo)) {
    }

    this.repo = repo;
    this.context = new Context();
  }

  async checkEmail(email) {
    const user = await this.repo.findOne(email);
    if (user === null) {
      throw new AppError("User Not exist", 404);
    }
  }
}
