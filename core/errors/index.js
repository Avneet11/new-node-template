


export class AppError extends Error {
  constructor(message, status) {
    super();

    this.name = this.constructor.name;
    Error.captureStackTrace(this, this.constructor);
    this.message = message || "Something wrong. Please try again.";
    this.status = status || 500;
  }
}
