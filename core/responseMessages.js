/*  ©2020 Ozvid Technologies Pvt. Ltd. All Rights Reserved.Hosted by jiWebHosting.com */
module.exports = {
  SUCCESS_MESSAGE: 'Success',
  REGISTERED: 'User Registered Successfully.',
  LOGIN: 'Signed In Successfully',
  OTP_REGISTERED: (email) => {
    return 'OTP Is Sent Successfully to ' + email;
  },
  ERROR: 'Something went wrong',
  VALIDATE_MESSAGE: 'Email And Password Are Required',
  DATA_UPDATED: 'Data Updated Successfully',
  DATA_DELETED: 'Data deleted Successfully',
  ADDED: (data) => {
    return data + ' Added Successfully ';
  },
  DELETED:(name)=>{
    return name + " Deleted Successfully"
  },
  BLOCKED: 'User is blocked',
  PASSWORD_NOT_MATCH: 'Incorrect password',
  EMAIL_NOT_MATCH: 'Incorrect Email',
  NO_USER_FOUND: 'No User Found',
  NO_DATA_FOUND: 'No Data Found',
  UNAUTHORIZED: 'Unauthorized , unable to do the action.',
  EMAIL_IS_REQUIRED: 'Email Is Required',
  PASSWORD_CHANGED: 'Password Changed Successfully',
  INVALID_TOKEN: 'Invalid token',
  TOKEN_NOT_PROVIDED: 'Token is not provided',
  NO_IMAGE: 'No Image, Please Pass An Image ',
  DUPLICATE_EMAIL: 'Email Already exist',
  WRONG_ROLE: 'Wrong Role Passed',
  VERIFY_EMAIL: 'Please Verify your Email',
  VERIFY_ACCOUNT: 'Please Verify your Account',
  OTP_VERIFIED: 'OTP Is Successfully Verified',
  OTP_INCORRECT: 'OTP Is Incorrect',
  CODE_VERIFIED: 'Promo CODE Is Successfully Verified',
  CODE_INCORRECT: 'Invalid Promo Code',
  CONTENT_ADDED: 'content added sucessfully ',
  CONTENT_FETCHED: 'content fetched successfully',
  FETCHED: 'Fetched successfully',
  PROFILE_SENT: 'Profile Sent Successfully to Admin',
  PAYMENT_UNSUCCESS: 'Payment UnSuccessful',
  PAYMENT_SUCCESS: 'Payment Successful',
  PAGE_INVALID: 'Page Not Valid',
  DATA_FOUND:'Data Found'
};
