

'use strict';

const mongoose = require('mongoose');
const validator = require('validator');
const mongoosePaginate = require('mongoose-paginate');

const schema = mongoose.Schema(
  {
    professionType: { type: String, required: true, text: true },
    industry: { type: mongoose.Schema.Types.ObjectId, ref: 'Industry' },
    members: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    peopleRelated: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Profile' }],
    status: { type: String, enum: ['Live', 'Draft'], default: 'Draft' },
    createdBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    createdAt: { type: Date },
    updatedAt: { type: Date },
  },
  {
    collection: 'profession',
    versionKey: false,
  }
);

schema.plugin(mongoosePaginate);

schema.methods.toJSON = function () {
  const obj = this.toObject();
  return obj;
};

schema.pre('save', function (next) {
  const now = new Date();
  this.updatedAt = now;
  if (!this.createdAt) {
    this.createdAt = now;
  }
  next();
});

export const Profession = mongoose.model('Profession', schema);
