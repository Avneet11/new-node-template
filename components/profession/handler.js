

import { ProfessionUseCase } from './usecase';
import * as message from '../../core/responseMessages';
import { Context } from '../../helpers/context';
/**
 * @export
 * @class ProfessionHandler
 */

/**
 * @export
 * @class ProfessionHandler
 */
export class ProfessionHandler {
  /**
   * Creates an instance of ProfessionHandler.
   * @memberof ProfessionHandler
   */
  constructor() {
    this.professionUseCase = new ProfessionUseCase();
    this.context = new Context();
  }

  /* Add Profession function */
  async addProfession(req, res) {
    try {
      let role = this.context.currentUser.role;
      if (role === 2 || role === 3) {
        let payloadData = req.body;
        const result = await this.professionUseCase.addProfession(payloadData);
        if (!result) {
          res.send({
            success: false,
            message: message.NO_USER_FOUND,
          });
        } else {
          res.status(201).send({
            message: message.ADDED('Profession'),
            success: true,
            count: result.members.length,
            data: result,
          });
        }
      } else {
        res.send({
          success: false,
          message: message.UNAUTHORIZED,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: err,
      });
    }
  }

  async updateProfession(req, res) {
    try {
      let role = this.context.currentUser.role;
      if (role === 2 || role === 3) {
        let id = req.params.id;
        let payloadData = req.body;
        let options = { new: true };
        payloadData.updatedAt = new Date();
        const result = await this.professionUseCase.updateProfession(
          { _id: id },
          payloadData,
          options
        );
        if (!result) {
          res.send({
            success: false,
            message: message.NO_USER_FOUND,
          });
        } else {
          res.send({
            message: message.DATA_UPDATED,
            success: true,
            result,
          });
        }
      } else {
        res.send({
          success: false,
          message: message.UNAUTHORIZED,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  /*  Profession */
  async getProfession(req, res) {
    try {
      let id = req.params.id;
      const result = await this.professionUseCase
        .getProfession({
          _id: id,
        })
        .populate('industry');
      if (!result) {
        res.send({
          success: false,
          message: message.NO_DATA_FOUND,
        });
      } else {
        res.status(200).send({
          success: true,
          message: message.FETCHED,
          result,
        });
      }
    } catch (err) {
      res.status(500).send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  /*  Profession List*/
  async getProfessionList(req, res) {
    try {
      let criteria = {
        status: 'Live',
      };
      const result = await this.professionUseCase
        .getProfessionList(criteria)
        .populate('industry');
      if (!result) {
        res.send({
          success: false,
          message: message.NO_DATA_FOUND,
        });
      } else {
        res.status(200).send({
          success: true,
          count: result.length,
          result,
        });
      }
    } catch (err) {
      res.status(500).send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  /*  Profession Pagination*/
  async getProfessionPagination(req, res) {
    try {
      const result = await this.professionUseCase.paginate(req.query);
      if (!result) {
        res.send({
          success: false,
          message: message.NO_DATA_FOUND,
        });
      } else {
        res.status(200).send({
          success: true,
          result: result.result,
          total: result.total,
        });
      }
    } catch (err) {
      res.status(500).send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  /* Delete Profession */
  async deleteProfession(req, res) {
    try {
      let role = this.context.currentUser.role;
      if (role === 2 || role === 3) {
        let id = req.params.id;
        const result = await this.professionUseCase.deleteProfession({
          _id: id,
        });
        if (!result) {
          res.send({
            success: false,
            message: message.NO_DATA_FOUND,
          });
        } else {
          res.status(200).send({
            success: true,
            message: message.DATA_DELETED,
          });
        }
      } else {
        res.send({
          success: false,
          message: message.UNAUTHORIZED,
        });
      }
    } catch (err) {
      res.status(500).send({
        success: false,
        message: message.ERROR,
      });
    }
  }
}
