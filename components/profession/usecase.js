

import { CoreUsecase } from '../../core/service/usecase';
import { ProfessionRepo } from './repo';

/**
 * @export
 * @class ProfessionUseCase
 * @extends {ProfessionUseCase}
 */
export class ProfessionUseCase extends CoreUsecase {
  /**
   * Creates an instance of ProfessionUseCase.
   * @memberof ProfessionUseCase
   */
  constructor() {
    const professionRepo = new ProfessionRepo();
    super(professionRepo);
    this.professionRepo = professionRepo;
  }

  async paginate({ page, limit }) {
    const query = this.buildPaginateQuery({});
    const result = await this.professionRepo.paginate(query, page, limit);

    return {
      result: result.docs,
      total: result.total,
    };
  }

  buildPaginateQuery({}) {
    const query = {};
    return query;
  }

  /**
   * @param {object}
   * @returns
   * @memberof ProfessionUseCase
   */
  addProfession(data) {
    data.createdBy = this.context.currentUser._id;
    return this.professionRepo.create(data);
  }

  getProfessionList(criteria) {
    return this.professionRepo.find(criteria);
  }

  getProfession(id) {
    return this.professionRepo.findOne(id);
  }

  async byType(data) {
    const result = await this.professionRepo.byType(data);
    return result;
  }

  async resultByType(data, queryToSend) {
    const result = await this.professionRepo.resultByType(data, queryToSend);
    return result;
  }

  updateProfession(criteria, dataToSet, option) {
    return this.professionRepo.findOneAndUpdate(criteria, dataToSet, option);
  }

  deleteProfession(id) {
    return this.professionRepo.deleteOne(id);
  }
}
