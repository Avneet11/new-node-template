

import { CoreRepo } from '../../core/service/repo';
import { Profession } from './model';

/**
 * @export
 * @class ProfessionRepo
 * @extends {CoreRepo}
 */
export class ProfessionRepo extends CoreRepo {
  /**
   * Creates an instance of ProfessionRepo.
   * @memberof ProfessionRepo
   */
  constructor() {
    super(Profession);
  }

  /**
   * @param {object} query
   * @param {number} [page]
   * @param {number} [limit]
   * @returns
   * @memberof ProfessionRepo
   */
  paginate(query, page, limit) {
    return this.model.paginate(query, {
      page: parseInt(page) || 1,
      limit: parseInt(limit) || 10,
      sort: { updatedAt: -1 },
      lean: true,
      new: true,
      leanWithId: false,
      populate: ['industry'],
    });
  }

  byType(data) {
    let det = '^' + data;
    return this.model.aggregate([
      {
        $lookup: {
          from: 'profile',
          let: { peopleRelated: '$peopleRelated' },
          pipeline: [
            {
              $match: {
                $expr: { $in: ['$_id', { $ifNull: ['$$peopleRelated', []] }] },
              },
            },
          ],
          as: 'profile',
        },
      },
      {
        $match: {
          status: 'Live',
          professionType: { $regex: det, $options: 'si' },
          profile: { $ne: [] },
        },
      },
    ]);
  }

  resultByType(data, queryToSend) {
    let det = '^' + data;
    let pageNo = parseInt(queryToSend.page) || 1;
    let pageSize = parseInt(queryToSend.limit) || 10;
    if (pageNo <= 0) {
      return;
    }
    return this.model.aggregate([
      {
        $lookup: {
          from: 'profile',
          let: { peopleRelated: '$peopleRelated' },
          pipeline: [
            {
              $match: {
                $expr: { $in: ['$_id', { $ifNull: ['$$peopleRelated', []] }] },
              },
            },
          ],
          as: 'profile',
        },
      },
      {
        $match: {
          status: 'Live',
          professionType: { $regex: det, $options: 'si' },
          profile: { $ne: [] },
        },
      },
      {
        $unwind: '$profile',
      },
      {
        $addFields: {
          _id: '$profile._id',
          otherLangs: '$profile.otherLangs',
          fullName: '$profile.fullName',
          btwfScore: '$profile.btwfScore',
          primaryLang: '$profile.primaryLang',
          nationality: '$profile.nationality',
          zodiacSign: '$profile.zodiacSign',
          netWorthUnit: '$profile.netWorthUnit',
          netWorth: '$profile.netWorth',
          profession: '$profile.profession',
          currentProvince: '$profile.currentProvince',
          provinceOfBirth: '$profile.provinceOfBirth',
          mainResidence: '$profile.mainResidence',
          provinceOfBirth: '$profile.provinceOfBirth',
          home1: '$profile.home1',
          profileImg: '$profile.profileImg',
          isBtwf: '$profile.isBtwf',
        },
      },
      {
        $lookup: {
          from: 'profession',
          localField: 'profession',
          foreignField: '_id',
          as: 'profession',
        },
      },
      {
        $project: {
          _id: 1,
          otherLangs: 1,
          fullName: 1,
          btwfScore: 1,
          primaryLang: 1,
          nationality: 1,
          zodiacSign: 1,
          netWorthUnit: 1,
          netWorth: 1,
          profession: 1,
          currentProvince: 1,
          provinceOfBirth: 1,
          mainResidence: 1,
          provinceOfBirth: 1,
          home1: 1,
          profileImg: 1,
          isBtwt: 1,
        },
      },
      {
        $facet: {
          paginatedResults: [
            { $skip: pageSize * (pageNo - 1) },
            { $limit: pageSize },
          ],
          totalCount: [
            {
              $count: 'count',
            },
          ],
        },
      },
    ]);
  }
}
