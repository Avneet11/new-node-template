

import { CoreRepo } from '../../core/service/repo';
import { Group } from './model';
const mongoose = require('mongoose');

/**
 * @export
 * @class CompanyRepo
 * @extends {CoreRepo}
 */
export class GroupRepo extends CoreRepo {
  /**
   * Creates an instance of CompanyRepo.
   * @memberof GroupRepo
   */
  constructor() {
    super(Group);
  }
  /**
   * @param {object} query
   * @param {number} [page]
   * @param {number} [limit]
   * @returns
   * @memberof GroupRepo
   */
  paginate(query, page, limit) {
    return this.model.paginate(query, {
      page: parseInt(page) || 1,
      limit: parseInt(limit) || 10,
      sort: { updatedAt: -1 },
      lean: true,
      leanWithId: false,
      select: '',
      populate: ['media_id', 'social', 'profile'],
    });
  }

  getGroupWithType() {
    return this.model.aggregate([
      {
        $match: {
          status: 'Live',
        },
      },
      {
        $group: {
          _id: '$groupType',
          groupName: {
            $addToSet: {
              name: '$name',
              _id: '$_id',
            },
          },
        },
      },
    ]);
  }

  byGroupName(data) {
    let det = '^' + data;
    return this.model.aggregate([
      {
        $lookup: {
          from: 'profile',
          let: { peopleRelated: '$peopleRelated' },
          pipeline: [
            {
              $match: {
                $expr: { $in: ['$_id', { $ifNull: ['$$peopleRelated', []] }] },
              },
            },
          ],
          as: 'profile',
        },
      },
      {
        $match: {
          status: 'Live',
          name: { $regex: det, $options: 'si' },
          profile: { $ne: [] },
        },
      },
    ]);
  }

  resultByGroupName(data, queryToSend) {
    let det = '^' + data;
    let groupTypeToFind = '^' + queryToSend.id.slice(0, -1);
    let pageNo = parseInt(queryToSend.page) || 1;
    let pageSize = parseInt(queryToSend.limit) || 10;
    if (pageNo <= 0) {
      return;
    }
    return this.model.aggregate([
      {
        $lookup: {
          from: 'profile',
          let: { peopleRelated: '$peopleRelated' },
          pipeline: [
            {
              $match: {
                $expr: { $in: ['$_id', { $ifNull: ['$$peopleRelated', []] }] },
              },
            },
          ],
          as: 'profile',
        },
      },
      {
        $match: {
          status: 'Live',
          name: { $regex: det, $options: 'si' },
          groupType: { $regex: groupTypeToFind, $options: 'si' },
          profile: { $ne: [] },
        },
      },
      {
        $unwind: '$profile',
      },
      {
        $addFields: {
          _id: '$profile._id',
          otherLangs: '$profile.otherLangs',
          fullName: '$profile.fullName',
          btwfScore: '$profile.btwfScore',
          primaryLang: '$profile.primaryLang',
          nationality: '$profile.nationality',
          zodiacSign: '$profile.zodiacSign',
          netWorthUnit: '$profile.netWorthUnit',
          netWorth: '$profile.netWorth',
          profession: '$profile.profession',
          currentProvince: '$profile.currentProvince',
          provinceOfBirth: '$profile.provinceOfBirth',
          mainResidence: '$profile.mainResidence',
          provinceOfBirth: '$profile.provinceOfBirth',
          home1: '$profile.home1',
          profileImg: '$profile.profileImg',
          isBtwf: '$profile.isBtwf',
        },
      },
      {
        $lookup: {
          from: 'profession',
          localField: 'profession',
          foreignField: '_id',
          as: 'profession',
        },
      },
      {
        $project: {
          _id: 1,
          otherLangs: 1,
          fullName: 1,
          btwfScore: 1,
          primaryLang: 1,
          nationality: 1,
          zodiacSign: 1,
          netWorthUnit: 1,
          netWorth: 1,
          profession: 1,
          currentProvince: 1,
          provinceOfBirth: 1,
          mainResidence: 1,
          provinceOfBirth: 1,
          home1: 1,
          profileImg: 1,
          isBtwf: 1,
        },
      },
      {
        $facet: {
          paginatedResults: [
            { $skip: pageSize * (pageNo - 1) },
            { $limit: pageSize },
          ],
          totalCount: [
            {
              $count: 'count',
            },
          ],
        },
      },
    ]);
  }

  byGroup(data) {
    let det = '^' + data;
    return this.model.aggregate([
      {
        $lookup: {
          from: 'profile',
          let: { peopleRelated: '$peopleRelated' },
          pipeline: [
            {
              $match: {
                $expr: { $in: ['$_id', { $ifNull: ['$$peopleRelated', []] }] },
              },
            },
          ],
          as: 'profile',
        },
      },
      {
        $match: {
          status: 'Live',
          name: { $regex: det, $options: 'si' },
          profile: { $ne: [] },
        },
      },
    ]);
  }
}
