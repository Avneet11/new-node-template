

'use strict';

const mongoose = require('mongoose');
const validator = require('validator');
const mongoosePaginate = require('mongoose-paginate');

const schema = mongoose.Schema(
  {
    groupType: {
      type: String,
      required: true,
      enum: [
        'Band',
        'PrivateCompany',
        'Family',
        'Movie',
        'SocialMedia',
        'SportsTeam',
        'TVShow',
      ],
    },
    name: { type: String, required: true, text: true },
    ceo: { type: String },
    link: { type: String },
    logo: { type: String },
    btwfScore: { type: Number, default: 0 },
    btwfCount: [{ type: String }],
    foundation: { type: Date },
    shortDescription: { type: String },
    description: { type: String },
    region: { type: String },
    city: { type: String },
    country: { type: String },
    genre: { type: String },
    runningTime: { type: String },
    director: { type: String },
    budget: { type: String },
    boxOffice: { type: String },
    spokenlanguage: { type: String },
    release: { type: Date },
    launch: { type: String },
    platform: { type: String },
    location: { type: String },
    industry: { type: String },
    sport: { type: String },
    league: { type: String },
    owner: { type: String },
    arenaStadium: { type: String },
    status: { type: String, default: 'Draft' },
    yearsActive: { type: String },
    network: { type: String },
    teamLogo: { type: String },
    isBtwf: { type: Boolean, default: false },
    social: { type: mongoose.Schema.Types.ObjectId, ref: 'SocialLink' },
    media_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Media' },
    collaborators: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Profile' }],
    famousPeopleRelated: [
      { type: mongoose.Schema.Types.ObjectId, ref: 'Profile' },
    ],
    peopleRelated: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Profile' }],
    createdBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    updatedBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    createdAt: { type: Date },
    updatedAt: { type: Date },
  },
  {
    collection: 'group',
    versionKey: false,
  }
);

schema.plugin(mongoosePaginate);

schema.methods.toJSON = function () {
  const obj = this.toObject();
  return obj;
};

schema.pre('save', function (next) {
  const now = new Date();
  this.updatedAt = now;
  if (!this.createdAt) {
    this.createdAt = now;
  }
  next();
});

export const Group = mongoose.model('Group', schema);
