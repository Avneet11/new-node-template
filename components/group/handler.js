
const fs = require('fs');
const multer = require('multer');
const dir = './uploads/company';
import { GroupUseCase } from './usecase';
import { MediaUseCase } from '../media/usecase';
import { AppError } from '../../core/errors';
import { Common } from '../../helpers/common';
//import { SocialLinkUseCase } from '../socialLink/usecase';
import * as message from '../../core/responseMessages';
import { Context } from '../../helpers/context';
/**
 * @export
 * @class ProductHandler
 */

const storage = multer.diskStorage({
  /* destination*/
  destination: function (req, file, cb) {
    cb(null, dir);
  },
  filename: function (req, file, cb) {
    cb(null, new Date().getTime().toString() + '-' + file.originalname);
  },
});
const upload = multer({ storage: storage }).fields([
  { name: 'images' },
  { name: 'logo' },
  { name: 'teamLogo' },
  { name: 'videos' },
]);
/**
 * @export
 * @class GroupHandler
 */
export class GroupHandler {
  /**
   * Creates an instance of GroupHandler.
   * @memberof GroupHandler
   */
  constructor() {
    this.groupUseCase = new GroupUseCase();
    this.mediaUsecase = new MediaUseCase();
    //this.socialLinkUseCase = new SocialLinkUseCase();
    this.context = new Context();
  }

  /* Add Company function */
  async addGroup(req, res) {
    try {
      let role = this.context.currentUser.role;
      if (role === 2 || role === 3) {
        if (!fs.existsSync(dir)) {
          fs.mkdirSync(dir);
        }
        upload(req, res, async (err) => {
          if (err) {
            res.json({
              success: false,
              message: err,
            });
          } else {
            let payloadData = req.body;
            let images = [];
            let Media = req.files;
            if (Media) {
              if (Media.images) {
                Media.images.map((data) => {
                  images.push(data.path);
                });
                payloadData.images = images;
              }
             
              if (Media.logo) {
                payloadData.logo = Media.logo[0].path;
              }
              if (Media.teamLogo) {
                payloadData.teamLogo = Media.teamLogo[0].path;
              }
            }
            payloadData.videoLink = payloadData.videoLink;
            let socialResult = await this.socialLinkUseCase.SocialLink(
              payloadData
            );
            payloadData.social = socialResult._id;
            const mediaResult = await this.mediaUsecase.Content(payloadData);
            payloadData.media_id = mediaResult._id;
            const result = await this.groupUseCase.addGroup(payloadData);
            if (!result) {
              res.send({
                success: false,
                message: message.NO_USER_FOUND,
              });
            } else {
              res.send({
                message: message.DATA_UPDATED,
                success: true,
                result,
              });
            }
          }
        });
      } else {
        res.send({
          success: false,
          message: message.UNAUTHORIZED,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  async editGroup(req, res) {
    try {
      let role = this.context.currentUser.role;
      if (role === 2 || role === 3) {
        if (!fs.existsSync(dir)) {
          fs.mkdirSync(dir);
        }
        upload(req, res, async (err) => {
          let id = req.params.id;
          let payloadData = req.body;
          let image = payloadData.images ? payloadData.images : [];
          let video = payloadData.videos ? payloadData.videos : [];
          let Media = req.files;
          let images = [];
          let videos = [];
          if (Media) {
            if (Media.images) {
              Media.images.map((data) => {
                images.push(data.path);
              });
              if (!Array.isArray(image)) {
                image = [image];
              }
              let imageToPush = [...image, ...images];
              payloadData.images = imageToPush;
            }
           
            if (Media.logo) {
              payloadData.logo = Media.logo[0].path;
            }
            if (Media.teamLogo) {
              payloadData.teamLogo = Media.teamLogo[0].path;
            }
          }
          payloadData.updatedAt = new Date();
          payloadData.updatedBy = this.context.currentUser._id;
          let options = { new: true };
          const result = await this.groupUseCase.updateGroup(
            { _id: id },
            payloadData,
            options
          );
          if (!result) {
            res.send({
              success: false,
              message: message.NO_USER_FOUND,
            });
          } else {
            if (result.social) {
              await this.socialLinkUseCase.updateSocialLink(
                { _id: result.social },
                payloadData,
                options
              );
            }
            if (result.media_id) {
              await this.mediaUsecase.updateByMediaId(
                { _id: result.media_id },
                payloadData,
                options
              );
            }
            res.send({
              message: message.DATA_UPDATED,
              success: true,
              result,
            });
          }
        });
      } else {
        res.send({
          success: false,
          message: message.UNAUTHORIZED,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  async getGroup(req, res) {
    try {
      let id = req.params.id;
      let queryToSend = req.query;
      let paginate = {};
      let pageNo = parseInt(queryToSend.page) || 1;
      let pageSize = parseInt(queryToSend.limit) || 10;
      (paginate.skip = pageSize * (pageNo - 1)), (paginate.limit = pageSize);
      if (pageNo <= 0) {
        return;
      }
      let sortScore;
      if (queryToSend.popularity == 'mostPopular') {
        sortScore = -1;
      }
      if (queryToSend.popularity == 'leastPopular') {
        sortScore = 1;
      }
      const result = await this.groupUseCase
        .getOneGroup({ _id: id })
        .populate('media_id')
        .populate({
          path: 'peopleRelated',
          populate: {
            path: 'profession',
          },
          options: { sort: { btwfScore: sortScore } },
          populate: {
            path: 'industry',
            select: { peopleRelated: 0 },
          },
        })
        .populate({
          path: 'peopleRelated',
          populate: {
            path: 'profession',
            select: { peopleRelated: 0 },
          },
          options: { sort: { btwfScore: sortScore } },
          skip: paginate.skip,
          limit: paginate.limit,
        })
        .populate({
          path: 'famousPeopleRelated',
          populate: {
            path: 'profile',
            select: { peopleRelated: 0 },
          },
          populate: {
            path: 'profession',
            select: { peopleRelated: 0 },
          },
          skip: paginate.skip,
          limit: paginate.limit,
        })
        .populate({
          path: 'collaborators',
          populate: {
            path: 'profile',
            select: { peopleRelated: 0 },
          },
          populate: {
            path: 'profession',
            select: { peopleRelated: 0 },
          },
          skip: paginate.skip,
          limit: paginate.limit,
        })
        .populate('social');
      if (!result) {
        res.send({
          success: false,
          message: message.NO_DATA_FOUND,
        });
      } else {
        const result2 = await this.groupUseCase
          .getOneGroup({ _id: id })
          .populate({
            path: 'peopleRelated',
            populate: {
              path: 'profession',
              select: { peopleRelated: 0 },
            },
          });
        res.status(200).send({
          success: true,
          message: message.SUCCESS_MESSAGE,
          result,
          count: result2.peopleRelated ? result2.peopleRelated.length : 0,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  async getAllGroup(req, res) {
    try {
      let role = this.context.currentUser.role;
      if (role === 2 || role === 3) {
        let payloadData = req.query;
        const result = await this.groupUseCase.paginate(payloadData);
        if (!result) {
          res.send({
            success: false,
            message: message.NO_DATA_FOUND,
          });
        } else {
          res.send({
            success: true,
            message: message.CONTENT_FETCHED,
            data: result,
          });
        }
      } else {
        res.send({
          success: false,
          message: message.UNAUTHORIZED,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: err.message,
      });
    }
  }

  async getGroups(req, res) {
    try {
      let role = this.context.currentUser.role;
      if (role === 2 || role === 3) {
        let payloadData = { status: 'Live' };
        const result = await this.groupUseCase.getGroup(payloadData);
        if (!result) {
          res.send({
            success: false,
            message: message.NO_DATA_FOUND,
          });
        } else {
          res.send({
            success: true,
            message: message.CONTENT_FETCHED,
            data: {
              result,
              total: result.length,
            },
          });
        }
      } else {
        res.send({
          success: false,
          message: message.UNAUTHORIZED,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: err.message,
      });
    }
  }

  async getGroupWithType(req, res) {
    try {
      let payloadData = req.query;
      const result = await this.groupUseCase.getGroupWithType(payloadData);
      if (!result) {
        res.send({
          success: false,
          message: message.NO_DATA_FOUND,
        });
      } else {
        res.send({
          success: true,
          message: message.CONTENT_FETCHED,
          data: result,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: err.message,
      });
    }
  }

  async deleteGroup(req, res) {
    try {
      let role = this.context.currentUser.role;
      if (role == 2 || role == 3) {
        let id = req.params.id;
        const result = await this.groupUseCase.getOneGroup({
          _id: id,
        });
        if (result) {
          if (result.media) {
            const result1 = await this.mediaUsecase.deleteMedia({
              _id: result.media,
            });
            if (!result1) {
              res.send({
                sucess: false,
                message: 'Media is not deleted properly',
              });
            }
          }
          const result2 = await this.groupUseCase.deleteGroup({
            _id: id,
          });
          if (!result2) {
            res.send({
              success: false,
              message: message.NO_USER_FOUND,
            });
          } else {
            res.send({
              message: message.DATA_DELETED,
              success: true,
              result,
            });
          }
        }
      }
    } catch (err) {
      res.send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  /* Get Group function User */
  async groupUser(req, res) {
    try {
      let payloadData = { status: 'Live' };
      const result = await this.groupUseCase.getGroup(payloadData);
      if (!result) {
        res.send({
          success: false,
          message: message.NO_DATA_FOUND,
        });
      } else {
        res.send({
          success: true,
          message: message.CONTENT_FETCHED,
          data: {
            result,
            total: result.length,
          },
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: err.message,
      });
    }
  }

  /* Update Btwf Score function */
  async updateBtwfScoreGroup(req, res) {
    try {
      let idToSet = req.body.userId;
      let idToUpdate = req.params.id;
      let profileResult = await this.groupUseCase.getOneGroup({
        _id: idToUpdate,
        btwfCount: { $in: idToSet },
      });
      let datatoset;
      if (!profileResult) {
        datatoset = {
          $addToSet: { btwfCount: idToSet },
          $inc: { btwfScore: 1 },
          $set: { isBtwf: true },
        };
      } else {
        datatoset = {
          $pull: { btwfCount: idToSet },
          $inc: { btwfScore: -1 },
          $set: { isBtwf: false },
        };
      }
      datatoset.updatedAt = new Date();
      let result = await this.groupUseCase.updateGroup(
        { _id: idToUpdate },
        datatoset,
        { new: true }
      );
      if (!result) {
        res.send({
          success: false,
          message: message.NO_USER_FOUND,
        });
      } else {
        res.status(200).send({
          success: true,
          message: message.SUCCESS_MESSAGE,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: message.ERROR,
      });
    }
  }
}
