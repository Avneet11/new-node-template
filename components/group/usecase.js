

import { CoreUsecase } from '../../core/service/usecase';
import { GroupRepo } from './repo';

/**
 * @export
 * @class ProductUseCase
 * @extends {ProductUseCase}
 */
export class GroupUseCase extends CoreUsecase {
  /**
   * Creates an instance of CompanyUseCase.
   * @memberof GroupUseCase
   */
  constructor() {
    const groupRepo = new GroupRepo();
    super(groupRepo);
    this.groupRepo = groupRepo;
  }

  /**
   * @param {object}
   * @returns
   * @memberof GroupUseCase
   */

  async paginate({ page, limit }) {
    const query = this.buildPaginateQuery({});
    const result = await this.groupRepo.paginate({}, page, limit);
    return {
      result: result.docs,
      total: result.total,
    };
  }

  buildPaginateQuery({}) {
    const query = {};
    return query;
  }

  async getGroupWithType() {
    const result = await this.groupRepo.getGroupWithType();
    return result;
  }

  addGroup(data) {
    data.createdBy = this.context.currentUser._id;
    return this.groupRepo.create(data);
  }

  updateGroup(criteria, dataToSet, option) {
    return this.groupRepo.findOneAndUpdate(criteria, dataToSet, option);
  }

  async byGroupName(data) {
    const result = await this.groupRepo.byGroupName(data);
    return result;
  }

  async resultByGroupName(data, queryToSend) {
    const result = await this.groupRepo.resultByGroupName(data, queryToSend);
    return result;
  }

  deleteGroup(id) {
    return this.groupRepo.deleteOne(id);
  }
  getGroup(data) {
    return this.groupRepo.find(data);
  }
  getOneGroup(data) {
    return this.groupRepo.findOne(data);
  }
}
