

'use strict';

const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const disableSchema = new mongoose.Schema({
  updatedAt: { type: Date, default: Date.now },
  disabled: [{ type: Object }],
  zodiacSign: { type: Array },
  primaryLang: { type: Array },
  currPForfiter: { type: Array },
  currCityForfiter: { type: Array },
  cobForfiter: { type: Array },
  accAge: { type: Array },
  birthYear: { type: Array },
  birthDay: { type: Array },
});

const schema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    icon: { type: String },
    options: { type: Array },
    status: { type: String, enum: ['Live', 'Draft'], default: 'Draft' },
    createdBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    createdAt: { type: Date },
    updatedAt: { type: Date },
  },
  {
    collection: 'database',
    versionKey: false,
  }
);

schema.plugin(mongoosePaginate);

schema.methods.toJSON = function () {
  const obj = this.toObject();
  return obj;
};

disableSchema.methods.toJSON = function () {
  const obj = this.toObject();
  return obj;
};

schema.pre('save', function (next) {
  const now = new Date();
  this.updatedAt = now;
  if (!this.createdAt) {
    this.createdAt = now;
  }
  next();
});

export const Disable = mongoose.model('Disable', disableSchema);

export const Database = mongoose.model('Database', schema);
