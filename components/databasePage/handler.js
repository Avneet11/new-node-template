

const fs = require('fs');
const multer = require('multer');
const dir = './uploads/database';
import { DatabaseUseCase } from './usecase';
import { ProfileUseCase } from '../profile/usecase';
import * as message from '../../core/responseMessages';
import { Context } from '../../helpers/context';
/**
 * @export
 * @class DatabaseHandler
 */

const storage = multer.diskStorage({
  /* destination*/
  destination: function (req, file, cb) {
    cb(null, dir);
  },
  filename: function (req, file, cb) {
    cb(null, new Date().getTime().toString() + '-' + file.originalname);
  },
});
const upload = multer({ storage: storage }).single('icon');
/**
 * @export
 * @class DatabaseHandler
 */
export class DatabaseHandler {
  /**
   * Creates an instance of DatabaseHandler.
   * @memberof DatabaseHandler
   */
  constructor() {
    this.databaseUseCase = new DatabaseUseCase();
    this.profileUseCase = new ProfileUseCase();
    this.context = new Context();
  }

  /* Add Database function */
  async addDatabase(req, res) {
    try {
      let role = this.context.currentUser.role;
      if (role === 2 || role === 3) {
        if (!fs.existsSync(dir)) {
          fs.mkdirSync(dir);
        }
        upload(req, res, async (err) => {
          if (err) {
            res.json({
              success: false,
              message: err,
            });
          } else {
            let payloadData = req.body;
            if (req.file) {
              payloadData.icon = req.file && req.file.path ? req.file.path : '';
            }
            const result = await this.databaseUseCase.addDatabase(payloadData);
            if (!result) {
              res.send({
                success: false,
                message: message.NO_USER_FOUND,
              });
            } else {
              res.status(200).send({
                message: message.ADDED('Database'),
                success: true,
                data: result,
              });
            }
          }
        });
      } else {
        res.send({
          success: false,
          message: message.UNAUTHORIZED,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  async updateDatabase(req, res) {
    try {
      let role = this.context.currentUser.role;
      if (role === 2 || role === 3) {
        if (!fs.existsSync(dir)) {
          fs.mkdirSync(dir);
        }
        upload(req, res, async (err) => {
          let id = req.params.id;
          let payloadData = req.body;
          payloadData.updatedAt = new Date();
          let options = { new: true };
          if (req.file) {
            payloadData.icon = req.file && req.file.path ? req.file.path : '';
          }
          const result = await this.databaseUseCase.updateDatabase(
            { _id: id },
            payloadData,
            options
          );
          if (!result) {
            res.send({
              success: false,
              message: message.NO_USER_FOUND,
            });
          } else {
            res.send({
              message: message.DATA_UPDATED,
              success: true,
              result,
            });
          }
        });
      } else {
        res.send({
          success: false,
          message: message.UNAUTHORIZED,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  /*  Database */
  async getDatabase(req, res) {
    try {
      let id = req.params.id;
      const result = await this.databaseUseCase.getDatabase({ _id: id });
      if (!result) {
        res.send({
          success: false,
          message: message.NO_DATA_FOUND,
        });
      } else {
        res.status(200).send({
          success: true,
          message: message.FETCHED,
          result,
        });
      }
    } catch (err) {
      res.status(500).send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  /* Search Count */
  async getSearchNumber(req, res) {
    try {
      let role = this.context.currentUser.role;
      if (role === 2 || role === 3) {
        let queryField = req.query;
        let keyToSearch = queryField.key;
        let valueToSearch = queryField.value;
        let criteria;
        let count;
        switch (keyToSearch) {
          case 'birthDate':
            count = await this.profileUseCase.birthDateNumber(valueToSearch);
            break;
          case 'dob':
            let year = queryField.value;
            criteria = {
              [year]: { $year: '$dob' },
            };
            count = await this.profileUseCase.birthYearNumber(criteria);
            break;
          case 'age':
            count = await this.profileUseCase.ageCount(criteria);
            break;
          default:
            criteria = {
              [keyToSearch]: valueToSearch,
            };
            count = await this.profileUseCase.getProfileAccToSearch(
              keyToSearch
            );
        }
        if (!count) {
          res.send({
            success: false,
            message: message.NO_DATA_FOUND,
          });
        } else {
          res.status(200).send({
            success: true,
            message: message.FETCHED,
            result: count,
          });
        }
      } else {
        res.send({
          success: false,
          message: message.UNAUTHORIZED,
        });
      }
    } catch (err) {
      res.status(500).send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  /* Enable/Disable search */
  async enableDisable(req, res) {
    try {
      let role = this.context.currentUser.role;
      if (role === 2 || role === 3) {
        let disableDataToFind = req.body.dataToDisable;
        let dataResult = await this.databaseUseCase.getOneData({
          disabled: { $in: disableDataToFind },
        });
        let datatoset;
        if (!dataResult) {
          datatoset = {
            $addToSet: { disabled: disableDataToFind },
          };
        } else {
          datatoset = {
            $pull: { disabled: disableDataToFind },
          };
        }
        let options = { new: true, upsert: true };
        const result = await this.databaseUseCase.addData(
          {},
          datatoset,
          options
        );
        if (!result) {
          res.send({
            success: false,
            message: message.NO_DATA_FOUND,
          });
        } else {
          res.status(200).send({
            success: true,
            message: message.FETCHED,
            result,
          });
        }
      } else {
        res.send({
          success: false,
          message: message.UNAUTHORIZED,
        });
      }
    } catch (err) {
      res.status(500).send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  async updateEnableDisable(req, res) {
    try {
      let role = this.context.currentUser.role;
      if (role === 2 || role === 3) {
        let options = { new: true, upsert: true };
        let datatoset = req.body;
        let dataToShow = Object.keys(datatoset).toString();
        const result = await this.databaseUseCase.addData(
          {},
          datatoset,
          options
        );
        if (!result) {
          res.send({
            success: false,
            message: message.NO_DATA_FOUND,
          });
        } else {
          res.status(200).send({
            success: true,
            message: message.FETCHED,
            result: result[dataToShow],
          });
        }
      } else {
        res.send({
          success: false,
          message: message.UNAUTHORIZED,
        });
      }
    } catch (err) {
      res.status(500).send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  /* Enable/Disable list */
  async getEnableDisableList(req, res) {
    try {
      let role = this.context.currentUser.role;
      if (role === 2 || role === 3) {
        let disableDataToFind = Object.keys(req.query).toString();
        let dataResult = await this.databaseUseCase.getOneData({});
        if (!dataResult) {
          res.send({
            success: false,
            message: message.NO_DATA_FOUND,
          });
        } else {
          res.send({
            success: true,
            result: dataResult[disableDataToFind],
            message: message.FETCHED,
          });
        }
      } else {
        res.send({
          success: false,
          message: message.UNAUTHORIZED,
        });
      }
    } catch (err) {
      res.status(500).send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  /* Enable/Disable list */
  async getEnableDisable(req, res) {
    try {
      let role = this.context.currentUser.role;
      if (role === 2 || role === 3) {
        let key = Object.keys(req.query);
        let value = Object.values(req.query).toString();
        let disableDataToFind = req.query;
        let dataResult = await this.databaseUseCase.getOneData({
          disabled: { $in: disableDataToFind },
        });
        if (!dataResult) {
          res.send({
            success: false,
            message: message.NO_DATA_FOUND,
          });
        } else {
          res.send({
            success: true,
            message: message.FETCHED,
          });
        }
      } else {
        res.send({
          success: false,
          message: message.UNAUTHORIZED,
        });
      }
    } catch (err) {
      res.status(500).send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  /*  Database List*/
  async getDatabaseList(req, res) {
    try {
      const result = await this.databaseUseCase.paginate(req.query);
      if (!result) {
        res.send({
          success: false,
          message: message.NO_DATA_FOUND,
        });
      } else {
        res.status(200).send({
          success: true,
          result: result.result,
          total: result.total,
        });
      }
    } catch (err) {
      res.status(500).send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  /* Delete Database */
  async deleteDatabase(req, res) {
    try {
      let role = this.context.currentUser.role;
      if (role === 2 || role === 3) {
        let id = req.params.id;
        const result = await this.databaseUseCase.deleteDatabase({
          _id: id,
        });
        if (!result) {
          res.send({
            success: false,
            message: message.NO_DATA_FOUND,
          });
        } else {
          res.status(200).send({
            success: true,
            message: message.DATA_DELETED,
          });
        }
      } else {
        res.send({
          success: false,
          message: message.UNAUTHORIZED,
        });
      }
    } catch (err) {
      res.status(500).send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  /* Update Database */
  async updateDatabaseStatus(req, res) {
    try {
      let role = this.context.currentUser.role;
      if (role === 2 || role === 3) {
        let id = req.params.id;
        let payloadData = req.body;
        let options = { new: true };
        const result = await this.databaseUseCase.updateDatabase(
          { _id: id },
          { $set: { status: payloadData.status } },
          options
        );
        if (!result) {
          res.send({
            success: false,
            message: message.NO_DATA_FOUND,
          });
        } else {
          res.status(200).send({
            success: true,
            message: message.DATA_UPDATED,
            result,
          });
        }
      } else {
        res.send({
          success: false,
          message: message.UNAUTHORIZED,
        });
      }
    } catch (err) {
      res.status(500).send({
        success: false,
        message: message.ERROR,
      });
    }
  }
}
