

import { CoreRepo } from '../../core/service/repo';
import { Disable } from './model';

/**
 * @export
 * @class DisableRepo
 * @extends {CoreRepo}
 */
export class DisableRepo extends CoreRepo {
  /**
   * Creates an instance of DisableRepo.
   * @memberof DisableRepo
   */
  constructor() {
    super(Disable);
  }

  /**
   * @param {object} query
   * @param {number} [page]
   * @param {number} [limit]
   * @returns
   * @memberof DisableRepo
   */
}
