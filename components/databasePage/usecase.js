

import { CoreUsecase } from '../../core/service/usecase';
import { DatabaseRepo } from './repo';
import { DisableRepo } from './disableRepo';

/**
 * @export
 * @class DatabaseUseCase
 * @extends {DatabaseUseCase}
 */
export class DatabaseUseCase extends CoreUsecase {
  /**
   * Creates an instance of IndustryUseCase.
   * @memberof DatabaseUseCase
   */
  constructor() {
    const databaseRepo = new DatabaseRepo();
    const disableRepo = new DisableRepo();
    super(databaseRepo);
    this.databaseRepo = databaseRepo;
    this.disableRepo = disableRepo;
  }

  /**
   * @param {object}
   * @returns
   * @memberof DatabaseUseCase
   */

  async paginate({ page, limit }) {
    const query = this.buildPaginateQuery({});
    const result = await this.databaseRepo.paginate(query, page, limit);

    return {
      result: result.docs,
      total: result.total,
    };
  }

  buildPaginateQuery({}) {
    const query = {};
    return query;
  }

  addDatabase(data) {
    data.createdBy = this.context.currentUser._id;
    return this.databaseRepo.create(data);
  }

  addData(criteria, dataToSet, option) {
    return this.disableRepo.findOneAndUpdate(criteria, dataToSet, option);
  }

  getOneData(data) {
    return this.disableRepo.findOne(data);
  }

  getDatabaseList(criteria) {
    return this.databaseRepo.find(criteria);
  }

  getDatabase(id) {
    return this.databaseRepo.findOne(id);
  }

  updateDatabase(criteria, dataToSet, option) {
    return this.databaseRepo.findOneAndUpdate(criteria, dataToSet, option);
  }

  deleteDatabase(id) {
    return this.databaseRepo.deleteOne(id);
  }
}
