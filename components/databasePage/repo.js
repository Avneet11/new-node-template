

import { CoreRepo } from '../../core/service/repo';
import { Database } from './model';

/**
 * @export
 * @class DatabaseRepo
 * @extends {CoreRepo}
 */
export class DatabaseRepo extends CoreRepo {
  /**
   * Creates an instance of DatabaseRepo.
   * @memberof DatabaseRepo
   */
  constructor() {
    super(Database);
  }

  /**
   * @param {object} query
   * @param {number} [page]
   * @param {number} [limit]
   * @returns
   * @memberof DatabaseRepo
   */
  paginate(query, page, limit) {
    return this.model.paginate(query, {
      page: parseInt(page) || 1,
      limit: parseInt(limit) || 10,
      lean: true,
      leanWithId: false,
      select: '',
      sort: {
        createdAt: 'desc',
      },
    });
  }

  paginateType(query, filter, page, limit) {
    let queryToFind = -1;
    if (filter == 'latestOne') {
      queryToFind = -1;
    }
    if (filter == 'oldOne') {
      queryToFind = 1;
    }
    return this.model.paginate(query, {
      sort: { updatedAt: queryToFind },
      page: parseInt(page) || 1,
      limit: parseInt(limit) || 5,
      lean: true,
      leanWithId: false,
      select: '',
      populate: ['media'],
    });
  }

  BirthDay(data, queryToSend) {
    let dataToSendFurther = '^' + data;
    let pageNo = parseInt(queryToSend.page) || 1;
    let pageSize = parseInt(queryToSend.limit) || 10;
    if (pageNo <= 0) {
      return;
    }
    return this.model.aggregate([
      {
        $match: {
          status: 'Live',
        },
      },
      {
        $addFields: {
          month: { $month: '$dob' },
        },
      },
      {
        $addFields: {
          day: { $dayOfMonth: '$dob' },
        },
      },
      {
        $addFields: {
          month: {
            $let: {
              vars: {
                monthsInString: [
                  'January',
                  'January',
                  'February',
                  'March',
                  'April',
                  'May',
                  'June',
                  'July',
                  'August',
                  'September',
                  'October',
                  'November',
                  'December',
                ],
              },
              in: {
                $arrayElemAt: ['$$monthsInString', '$month'],
              },
            },
          },
        },
      },
      {
        $addFields: {
          age: {
            $divide: [
              { $subtract: [new Date(), '$dob'] },
              365 * 24 * 60 * 60 * 1000,
            ],
          },
        },
      },
      {
        $addFields: {
          ageToCalculate: { $ceil: '$age' },
          ageToCaslculate: { $floor: '$age' },
        },
      },
      {
        $addFields: {
          birthDate: { $toString: '$day' },
          ageToCaslculate: { $toString: '$ageToCaslculate' },
        },
      },
      {
        $addFields: {
          birthday: { $concat: ['$birthDate', ' ', '$month'] },
        },
      },
      {
        $match: {
          birthday: { $regex: dataToSendFurther, $options: 'si' },
        },
      },
      {
        $lookup: {
          from: 'profession',
          localField: 'profession',
          foreignField: '_id',
          as: 'profession',
        },
      },
      {
        $facet: {
          paginatedResults: [
            { $skip: pageSize * (pageNo - 1) },
            { $limit: pageSize },
          ],
          totalCount: [
            {
              $count: 'count',
            },
          ],
        },
      },
    ]);
  }
}
