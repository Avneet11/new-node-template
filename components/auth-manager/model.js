

'use strict';

const mongoose = require('mongoose');
const validator = require('validator');

const schema = mongoose.Schema(
  {
    name: { type: String },
    email: {
      type: String,
      required: true,
      unique: true,
      validate: (value) => {
        return validator.isEmail(value);
      },
    },
    password: { type: String, required: true },
    profileImg: { type: String },
    phoneNumber: { type: Number },
    resetToken: { type: String },
    role: {
      type: Number,
      enum: [1, 2, 3],
      default: 1,
    },
    isFirst: { type: Boolean, default: true },
    googleAnalyticCode: { type: String },
    googleAdwordCode: { type: String },
    isVerified: { type: Boolean, default: false },
    status: { type: String },
    isAdmin: { type: Boolean, default: false },
    lastLogin: { type: Date },
    createdAt: { type: Date },
    updatedAt: { type: Date },
  },
  {
    collection: 'user',
    versionKey: false,
  }
);

schema.methods.toJSON = function () {
  const obj = this.toObject();
  return obj;
};

schema.pre('save', function (next) {
  const now = new Date();
  this.updatedAt = now;
  if (!this.createdAt) {
    this.createdAt = now;
  }
  next();
});

export const User = mongoose.model('User', schema);
