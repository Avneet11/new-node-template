

import { CoreUsecase } from '../../core/service/usecase';
import * as auth from '../../middlewares/auth';
import { UserRepo } from './repo';

/**
 * @export
 * @class UserUsecase
 * @extends {UserUsecase}
 */
export class UserUsecase extends CoreUsecase {
  /**
   * Creates an instance of UserUsecase.
   * @memberof UserUsecase
   */
  constructor() {
    const userRepo = new UserRepo();
    super(userRepo);
    this.userRepo = userRepo;
  }

  /**
   * @param {object}
   * @returns
   * @memberof UserUsecase
   */
  register(data) {
    return this.userRepo.create(data);
  }

  login(email) {
    return this.userRepo.findOne({
      email,
    });
  }

  getOneUser(id) {
    return this.userRepo.findOne({
      _id: id,
    });
  }

  getAdmin(id) {
    return this.userRepo.findOne(id);
  }

  getUsers(criteria) {
    return this.userRepo.find(criteria);
  }

  async getUsersList(criteria) {
    const result = await this.userRepo.getUsersList(criteria);
    return result;
  }

  getUser() {
    let user_id = this.context.currentUser._id;
    return this.userRepo.findOne({
      _id: user_id,
    });
  }

  updateUser(data) {
    const id = this.context.currentUser._id;
    return this.userRepo.findOneAndUpdate(id, data);
  }

  getUserById(user_id) {
    return this.userRepo.findOne({
      _id: user_id,
    });
  }

  updateUserData(criteria, dataToSet, option) {
    return this.userRepo.findOneAndUpdate(criteria, dataToSet, option);
  }

  deleteUser(id) {
    return this.userRepo.deleteOne(id);
  }
}
