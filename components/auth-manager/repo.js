

import { CoreRepo } from '../../core/service/repo';
import { User } from './model';
import { Context } from '../../helpers/context';
const mongoose = require('mongoose');

/**
 * @export
 * @class UserRepo
 * @extends {CoreRepo}
 */
export class UserRepo extends CoreRepo {
  /**
   * Creates an instance of UserRepo.
   * @memberof UserRepo
   */
  constructor() {
    super(User);
  }

  getUsersList(criteria) {
    let pageNo = parseInt(criteria.page) || 1;
    let pageSize = parseInt(criteria.limit) || 10;
    if (pageNo <= 0) {
      return;
    }
    return this.model.aggregate([
      {
        $match: {
          _id: { $ne: mongoose.Types.ObjectId(criteria.user_id) },
        },
      },
      {
        $addFields: {
          role: {
            $switch: {
              branches: [
                {
                  case: { $eq: ['$role', 1] },
                  then: 'User',
                },
                {
                  case: { $eq: ['$role', 2] },
                  then: 'Admin',
                },
                {
                  case: { $eq: ['$role', 3] },
                  then: 'Writer',
                },
              ],
              default: -1,
            },
          },
        },
      },
      { $sort: { updatedAt: -1 } },
      {
        $facet: {
          paginatedResults: [
            { $skip: pageSize * (pageNo - 1) },
            { $limit: pageSize },
          ],
          totalCount: [
            {
              $count: 'count',
            },
          ],
        },
      },
    ]);
  }

  paginateHouseTour(query) {
    let btwfScore;
    if (query.popularity == 'highestFirst') {
      btwfScore = -1;
    }
    if (query.popularity == 'lowestFirst') {
      btwfScore = 1;
    }
    return this.model.paginate(
      {},
      {
        page: parseInt(query.page) || 1,
        limit: parseInt(query.limit) || 10,
        sort: { btwfScore: btwfScore },
        lean: true,
        new: true,
        leanWithId: false,
        populate: [
          'social',
          'media',
          {
            path: 'profession',
            model: 'Profession',
          },
          {
            path: 'industry',
            model: 'Industry',
          },
        ],
      }
    );
  }

  getUser(criteria) {
    let pageNo = parseInt(criteria.page) || 1;
    let pageSize = parseInt(criteria.limit) || 10;
    if (pageNo <= 0) {
      return;
    }
    return this.model.aggregate([
      {
        $match: {
          _id: { $ne: mongoose.Types.ObjectId(criteria.user_id) },
        },
      },
      {
        $addFields: {
          role: {
            $switch: {
              branches: [
                {
                  case: { $eq: ['$role', 1] },
                  then: 'User',
                },
                {
                  case: { $eq: ['$role', 2] },
                  then: 'Admin',
                },
                {
                  case: { $eq: ['$role', 3] },
                  then: 'Writer',
                },
              ],
              default: -1,
            },
          },
        },
      },
      { $sort: { updatedAt: -1 } },
      {
        $facet: {
          paginatedResults: [
            { $skip: pageSize * (pageNo - 1) },
            { $limit: pageSize },
          ],
          totalCount: [
            {
              $count: 'count',
            },
          ],
        },
      },
    ]);
  }

  paginateHouseTour(query) {
    let btwfScore;
    if (query.popularity == 'highestFirst') {
      btwfScore = -1;
    }
    if (query.popularity == 'lowestFirst') {
      btwfScore = 1;
    }
    return this.model.paginate(
      {},
      {
        page: parseInt(query.page) || 1,
        limit: parseInt(query.limit) || 10,
        sort: { btwfScore: btwfScore },
        lean: true,
        new: true,
        leanWithId: false,
        populate: [
          'social',
          'media',
          {
            path: 'profession',
            model: 'Profession',
          },
          {
            path: 'industry',
            model: 'Industry',
          },
        ],
      }
    );
  }
}
