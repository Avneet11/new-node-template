

import { UserUsecase } from './usecase';
import { AppError } from '../../core/errors';
import { Common } from '../../helpers/common';
import * as message from '../../core/responseMessages';
import mailer from '../../helpers/mailer';
import { Context } from '../../helpers/context';
const fs = require('fs');
const multer = require('multer');
const dir = './uploads/user';
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const awsMailer = require('../../helpers/awsMailer');

/**
 * @export
 * @class ItemHandler
 */

const storage = multer.diskStorage({
  /* destination*/
  destination: function (req, file, cb) {
    cb(null, dir);
  },
  filename: function (req, file, cb) {
    cb(null, new Date().getTime().toString() + '-' + file.originalname);
  },
});
const upload = multer({ storage: storage }).single('profileImg');

const encodeRole = (role) => {
  switch (role) {
    case 'User':
      role = 1;
      break;
    case 'Admin':
      role = 2;
      break;
    case 'Writer':
      role = 3;
      break;
    default:
      role = -1;
  }
  return role;
};

const decodeRole = (role) => {
  switch (role) {
    case 1:
      role = 'User';
      break;
    case 2:
      role = 'Admin';
      break;
    case 3:
      role = 'Writer';
      break;
    default:
      role = -1;
  }
  return role;
};

/**
 * @export
 * @class AuthHandler
 */
export class AuthHandler {
  /**
   * Creates an instance of AuthHandler.
   * @memberof AuthHandler
   */
  constructor() {
    this.context = new Context();
    this.userUsecase = new UserUsecase();
  }

  async register(req, res) {
    try {
      if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
      }
      upload(req, res, async (err) => {
        if (err) {
          res.json({
            success: false,
            message: err,
          });
        } else {
          let payloadData = req.body;
          let passwordToSend = payloadData.password;
          payloadData.role = encodeRole(payloadData.role);
          if (payloadData.role == -1) {
            res.send({
              success: false,
              message: message.WRONG_ROLE,
            });
            return false;
          }
          let hash = await bcrypt.hash(
            payloadData.password,
            parseInt(process.env.SALT_ROUNDS)
          );
          if (req.file) {
            payloadData.profileImg =
              req.file && req.file.path ? req.file.path : '';
          }
          payloadData.password = hash;
          const result = await this.userUsecase
            .register(payloadData)
            .catch((err) => {
              if (err.name === 'MongoError' && err.code === 11000) {
                res.send({
                  success: false,
                  message: message.DUPLICATE_EMAIL,
                });
              } else {
                res.send({
                  success: false,
                  message: err,
                });
              }
            });
          if (result) {
            if (result.role == 1) {
              let subject = 'New User Registered ';
              let html = awsMailer.newUserRegisterHtml(
                result.name,
                result.email,
                passwordToSend
              );
              await awsMailer.sendMail(result.email, subject, html);
              res.send({
                success: true,
                message: message.REGISTERED,
                user_id: result._id,
              });
            } else {
              res.send({
                success: true,
                message: message.REGISTERED,
                data: result,
              });
            }
          }
        }
      });
    } catch (err) {
      if (err.name === 'MongoError' && err.code === 11000) {
        res.send({
          success: false,
          message: message.DUPLICATE_EMAIL,
        });
      } else {
        res.send({
          success: false,
          message: err,
        });
      }
    }
  }

  async login(req, res, next) {
    try {
      let payloadData = req.body;
      let result = await this.userUsecase.login(payloadData.email);
      if (result) {
        if (!result.isVerified) {
          res.send({
            success: false,
            message: message.VERIFY_ACCOUNT,
          });
          return;
        } else {
          let pwPresent = await bcrypt.compare(
            payloadData.password,
            result.password
          );
          if (pwPresent === true) {
            let token_Data = {
              email: result.email,
              _id: result._id,
              role: result.role,
            };
            let token = await Common.createjwtToken(token_Data);
            if (token) {
              let role = decodeRole(result.role);
              result.role = role;
              let data = {
                email: result.email,
                role: role,
              };
              result.token = token;
              await this.userUsecase.updateUserData(
                { _id: result._id },
                { updatedAt: new Date(), lastLogin: new Date() },
                { new: true }
              );
              res.send({
                success: true,
                data: data,
                id: result._id,
                name: result.name,
                message: message.LOGIN,
                token: token,
              });
            }
          } else {
            res.send({
              success: false,
              message: message.PASSWORD_NOT_MATCH,
            });
            return;
          }
        }
      } else {
        res.send({
          success: false,
          message: message.EMAIL_NOT_MATCH,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: err,
      });
    }
  }

  async accountSetting(req, res) {
    try {
      if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
      }
      upload(req, res, async (err) => {
        let payloadData = req.body;
        let id = this.context.currentUser._id;
        let options = { new: true };
        if (req.file) {
          payloadData.profileImg =
            req.file && req.file.path ? req.file.path : '';
        }
        if (payloadData.password) {
          let hash = await bcrypt.hash(
            payloadData.password,
            parseInt(process.env.SALT_ROUNDS)
          );
          if (hash) {
            let dataToSet = {
              password: hash,
            };
            payloadData.password = dataToSet.password;
          }
        } else {
          delete payloadData.password;
        }
        payloadData.updatedAt = new Date();
        const result = await this.userUsecase.updateUserData(
          { _id: id },
          payloadData,
          options
        );
        if (!result) {
          res.send({
            success: false,
            message: message.NO_USER_FOUND,
          });
        } else {
          res.send({
            message: message.DATA_UPDATED,
            success: true,
          });
        }
      });
    } catch (err) {
      res.send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  /*  User */
  async getUser(req, res) {
    try {
      const result = await this.userUsecase.getUser().sort({ updatedAt: -1 });
      if (!result) {
        res.send({
          success: false,
          message: message.NO_DATA_FOUND,
        });
      } else {
        res.status(200).send({
          success: true,
          message: message.FETCHED,
          result,
        });
      }
    } catch (err) {
      res.status(500).send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  /*  Users List*/
  async getUsers(req, res) {
    try {
      let user_id = this.context.currentUser._id;
      let criteria = {
        user_id: user_id,
        page: req.query.page,
        limit: req.query.limit,
      };
      const result = await this.userUsecase.getUsersList(criteria);
      if (!result) {
        res.send({
          success: false,
          message: message.NO_DATA_FOUND,
        });
      } else {
        res.status(200).send({
          success: true,
          message: message.FETCHED,
          result: result[0].paginatedResults,
          total: result[0].totalCount[0] ? result[0].totalCount[0].count : 0,
        });
      }
    } catch (err) {
      res.status(500).send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  /*  User By Id */
  async getUserById(req, res) {
    try {
      let id = req.params.id;
      const result = await this.userUsecase.getUserById(id);
      if (!result) {
        res.send({
          success: false,
          message: message.NO_DATA_FOUND,
        });
      } else {
        res.status(200).send({
          success: true,
          message: message.FETCHED,
          result,
        });
      }
    } catch (err) {
      res.status(500).send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  /* Edit User */
  async userUpdate(req, res) {
    try {
      let payloadData = req.body;
      let id = req.params.id;
      let options = { new: true };
      if (payloadData.password) {
        let hash = await bcrypt.hash(
          payloadData.password,
          parseInt(process.env.SALT_ROUNDS)
        );
        if (hash) {
          let dataToSet = {
            password: hash,
          };
          payloadData.password = dataToSet.password;
        }
      }
      payloadData.updatedAt = new Date();
      const result = await this.userUsecase.updateUserData(
        { _id: id },
        payloadData,
        options
      );
      if (!result) {
        res.send({
          success: false,
          message: message.NO_USER_FOUND,
        });
      } else {
        res.send({
          message: message.DATA_UPDATED,
          success: true,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  /* Register User */
  async signUp(req, res) {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        throw new Error(errors.array()[0].msg, 422);
      }
      let payloadData = req.body;
      if (!payloadData.email || !payloadData.password) {
        res.send({
          success: false,
          message: message.VALIDATE_MESSAGE,
        });
        return;
      }
      payloadData.role = encodeRole(payloadData.role);
      if (payloadData.role == -1) {
        res.send({
          success: false,
          message: message.WRONG_ROLE,
        });
        return false;
      }
      let hash = await bcrypt.hash(
        payloadData.password,
        parseInt(process.env.SALT_ROUNDS)
      );
      payloadData.password = hash;
      payloadData.otp = crypto.randomBytes(4).toString('hex');
      const result = await this.userUsecase.register(payloadData);
      if (result) {
        let subject = 'Email Verification';
        let html = awsMailer.newUserRegisterHtml(result.email, result.otp);
        await awsMailer.sendMail(result.email, subject, html);
        res.status(200).send({
          success: true,
          message: message.REGISTERED,
        });
      }
    } catch (err) {
      if (err.name === 'MongoError' && err.code === 11000) {
        res.send({
          success: false,
          message: message.DUPLICATE_EMAIL,
        });
      } else {
        res.send({
          success: false,
          message: err,
        });
      }
    }
  }

  /* Delete User */
  async deleteUser(req, res) {
    try {
      let role = this.context.currentUser.role;
      if (role === 2) {
        let id = req.params.id;
        const result = await this.userUsecase.deleteUser({
          _id: id,
        });
        if (!result) {
          res.send({
            success: false,
            message: message.NO_DATA_FOUND,
          });
        } else {
          res.status(200).send({
            success: true,
            message: message.DATA_DELETED,
          });
        }
      } else {
        res.send({
          success: false,
          message: message.UNAUTHORIZED,
        });
      }
    } catch (err) {
      res.status(500).send({
        success: false,
        message: message.ERROR,
      });
    }
  }
}
