

'use strict';

const mongoose = require('mongoose');
const validator = require('validator');

const schema = mongoose.Schema(
  {
    images: { type: Array },
    videos: { type: Array },
    videoLink: { type: Array },
    createdBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    createdAt: { type: Date },
    updatedAt: { type: Date },
  },
  {
    collection: 'media',
    versionKey: false,
  }
);

schema.methods.toJSON = function () {
  const obj = this.toObject();
  return obj;
};

schema.pre('save', function (next) {
  const now = new Date();
  this.updatedAt = now;
  if (!this.createdAt) {
    this.createdAt = now;
  }
  next();
});

export const Media = mongoose.model('Media', schema);
