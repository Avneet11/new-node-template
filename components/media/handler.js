
const { validationResult } = require('express-validator');
import { Context } from '../../helpers/context';
import { MediaUseCase } from './usecase';
import * as message from '../../core/responseMessages';

const fs = require('fs');
const multer = require('multer');
const dir = './uploads/content';
const storage = multer.diskStorage({
  /* destination*/
  destination: function (req, file, cb) {
    cb(null, dir);
  },
  filename: function (req, file, cb) {
    cb(null, new Date().getTime().toString() + '-' + file.originalname);
  },
});
const upload = multer({ storage: storage }).fields([
  { name: 'images' },
  { name: 'videos' },
]);
/**
 * @export
 * @class MediaHandler
 */

/**
 * @export
 * @class ProductHandler
 **/

export class MediaHandler {
  /**
   * Creates an instance of MediaHandler.
   * @memberof MediaHandler
   */
  constructor() {
    this.mediaUsecase = new MediaUseCase();
    this.context = new Context();
  }

  async addMedia(req, res) {
    try {
      upload(req, res, async (err) => {
        if (!fs.existsSync(dir)) {
          fs.mkdirSync(dir);
        }
        if (err) {
          res.json({
            success: false,
            message: err,
          });
        } else {
          let images = [];
          let videos = [];
          const payload = {};
          let Files = req.files;
          if (Files) {
            if (Files.images) {
              Files.images.map((data) => {
                images.push(data.path);
              });
              payload.images = images;
            }
            if (Files.videos) {
              Files.videos.map((data) => {
                videos.push(data.path);
              });
              payload.videos = videos;
            }
          }

          const result = await this.mediaUsecase.Content(payload);
          if (!result) {
            res.send({
              success: false,
              message: message.WRONG_ROLE,
            });
          } else {
            res.send({
              success: true,
              message: message.CONTENT_ADDED,
              data: result,
            });
          }
        }
      });
    } catch (err) {
      res.send({
        success: false,
        message: err.message,
        data: '',
      });
    }
  }

  async getAllMedia(req, res) {
    try {
      const result = await this.mediaUsecase.getMedia();
      if (!result) {
        res.send({
          success: false,
          message: message.WRONG_ROLE,
        });
      } else {
        res.send({
          success: true,
          message: message.CONTENT_ADDED,
          data: result,
          count: result.length,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: err.message,
        data: '',
      });
    }
  }

  async updateMedia(req, res) {
    try {
      upload(req, res, async (err) => {
        if (!fs.existsSync(dir)) {
          fs.mkdirSync(dir);
        }
        if (err) {
          res.json({
            success: false,
            message: err,
          });
        } else {
          let images = [];
          let videos = [];
          const payload = {};

          let Files = req.files;
          if (Files) {
            if (Files.images) {
              Files.images.map((data) => {
                images.push(data.path);
              });
              payload.images = images;
            }
            if (Files.videos) {
              Files.videos.map((data) => {
                videos.push(data.path);
              });
              payload.videos = videos;
            }
          }
          let options = { new: true };
          let id = req.params.id;
          payloadData.updatedAt = new Date();
          const result = await this.mediaUsecase.updateMedia(
            id,
            payload,
            options
          );
          if (!result) {
            res.send({
              success: false,
              message: message.WRONG_ROLE,
            });
          } else {
            res.send({
              success: true,
              message: message.CONTENT_ADDED,
              data: result,
            });
          }
        }
      });
    } catch (err) {
      res.send({
        success: false,
        message: err.message,
        data: '',
      });
    }
  }

  async deleteMedia(req, res) {
    try {
      let id = req.params.id;
      const result = await this.mediaUsecase.delete({ _id: id });
      if (!result) {
        res.send({
          success: false,
          message: message.NO_USER_FOUND,
        });
      } else {
        res.send({
          message: message.DATA_DELETED,
          success: true,
          result,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  async allCities(req, res) {
    try {
      let strRegExPattern = req.query.search;
      const cities = await require('all-the-cities');
      let pattern = '/^' + strRegExPattern + '/';
      let filterCities = cities.filter((city) =>
        city.name.startsWith(strRegExPattern)
      );
      if (!cities) {
        res.send({
          success: false,
        });
      } else {
        res.send({
          success: true,
          data: filterCities,
          citiesLength: filterCities.length,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: err.message,
      });
    }
  }
}
