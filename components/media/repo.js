import { CoreRepo } from "../../core/service/repo";
import { Media } from "./model";

/**
 * @export
 * @class MediaRepo
 * @extends {CoreRepo}
 */
export class MediaRepo extends CoreRepo {
  /**
   * Creates an instance of ProductRepo.
   * @memberof MediaRepo
   */
  constructor() {
    super(Media);
  }
}
