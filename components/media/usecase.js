

import { CoreUsecase } from '../../core/service/usecase';

import { MediaRepo } from './repo';
/**
 * @export
 * @class MediaUseCase
 * @extends {MediaUseCase}
 */
export class MediaUseCase extends CoreUsecase {
  /**
   * Creates an instance of MediaUseCase.
   * @memberof MediaUseCase
   */
  constructor() {
    const mediaRepo = new MediaRepo();
    super(mediaRepo);
    this.mediaRepo = mediaRepo;
  }

  /**
   * @param {object}
   * @returns
   * @memberof ProductUseCase
   */
  Content(data) {
    return this.mediaRepo.create(data);
  }

  ContentToAdd(data) {
    return this.mediaRepo.create(data);
  }

  getMedia(data) {
    return this.mediaRepo.find(data);
  }
  updateMedia(id, payload, options) {
    return this.mediaRepo.findOneAndUpdate(id, payload, options);
  }
  updateByMediaId(criteria, dataToSet, option) {
    return this.mediaRepo.findByIdAndUpdate(criteria, dataToSet, option);
  }
  deleteMedia(id) {
    return this.mediaRepo.deleteOne(id);
  }
}
