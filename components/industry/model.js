

'use strict';

const mongoose = require('mongoose');
const validator = require('validator');
const mongoosePaginate = require('mongoose-paginate');

const schema = mongoose.Schema(
  {
    name: { type: String, required: true, text: true },
    image: { type: String },
    members: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    status: { type: String, enum: ['Live', 'Draft'], default: 'Draft' },
    peopleRelated: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Profile' }],
    createdBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    createdAt: { type: Date },
    updatedAt: { type: Date },
  },
  {
    collection: 'industry',
    versionKey: false,
  }
);

schema.plugin(mongoosePaginate);

schema.methods.toJSON = function () {
  const obj = this.toObject();
  return obj;
};

schema.pre('save', function (next) {
  const now = new Date();
  this.updatedAt = now;
  if (!this.createdAt) {
    this.createdAt = now;
  }
  next();
});

export const Industry = mongoose.model('Industry', schema);
