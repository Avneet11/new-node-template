

import { CoreUsecase } from '../../core/service/usecase';
import { IndustryRepo } from './repo';

/**
 * @export
 * @class IndustryUseCase
 * @extends {IndustryUseCase}
 */
export class IndustryUseCase extends CoreUsecase {
  /**
   * Creates an instance of IndustryUseCase.
   * @memberof IndustryUseCase
   */
  constructor() {
    const industryRepo = new IndustryRepo();
    super(industryRepo);
    this.industryRepo = industryRepo;
  }

  async paginate({ page, limit }) {
    const query = this.buildPaginateQuery({});
    const result = await this.industryRepo.paginate(query, page, limit);

    return {
      result: result.docs,
      total: result.total,
    };
  }

  buildPaginateQuery({}) {
    const query = {};
    return query;
  }

  /**
   * @param {object}
   * @returns
   * @memberof IndustryUseCase
   */
  addIndustry(data) {
    data.createdBy = this.context.currentUser._id;
    return this.industryRepo.create(data);
  }

  getIndustries(criteria) {
    return this.industryRepo.find(criteria);
  }

  getIndustry(id) {
    return this.industryRepo.findOne(id);
  }

  async getIndustryAccToProfession() {
    const result = await this.industryRepo.getIndustryAccToProfession();
    return result;
  }

  async byIndustryName(data) {
    const result = await this.industryRepo.byIndustryName(data);
    return result;
  }

  async resultByIndustryName(data, queryToSend) {
    const result = await this.industryRepo.resultByIndustryName(
      data,
      queryToSend
    );
    return result;
  }

  updateIndustry(criteria, dataToSet, option) {
    return this.industryRepo.findOneAndUpdate(criteria, dataToSet, option);
  }

  deleteIndustry(id) {
    return this.industryRepo.deleteOne(id);
  }
}
