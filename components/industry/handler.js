

const fs = require('fs');
const multer = require('multer');
const dir = './uploads/industry';
import { IndustryUseCase } from './usecase';
import * as message from '../../core/responseMessages';
import { Context } from '../../helpers/context';
/**
 * @export
 * @class IndustryHandler
 */

const storage = multer.diskStorage({
  /* destination*/
  destination: function (req, file, cb) {
    cb(null, dir);
  },
  filename: function (req, file, cb) {
    cb(null, new Date().getTime().toString() + '-' + file.originalname);
  },
});
const upload = multer({ storage: storage }).single('image');
/**
 * @export
 * @class IndustryHandler
 */
export class IndustryHandler {
  /**
   * Creates an instance of IndustryHandler.
   * @memberof IndustryHandler
   */
  constructor() {
    this.industryUseCase = new IndustryUseCase();
    this.context = new Context();
  }

  /* Add Industry function */
  async addIndustry(req, res) {
    try {
      let role = this.context.currentUser.role;
      if (role === 2 || role === 3) {
        let payloadData = req.body;
        const result = await this.industryUseCase.addIndustry(payloadData);
        if (!result) {
          res.send({
            success: false,
            message: message.NO_USER_FOUND,
          });
        } else {
          res.status(201).send({
            message: message.ADDED('Industry'),
            success: true,
            count: result.members.length,
            data: result,
          });
        }
      } else {
        res.send({
          success: false,
          message: message.UNAUTHORIZED,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  async editIndustry(req, res) {
    try {
      let role = this.context.currentUser.role;
      if (role === 2 || role === 3) {
        let id = req.params.id;
        let payloadData = req.body;
        let options = { new: true };
        payloadData.updatedAt = new Date();
        const result = await this.industryUseCase.updateIndustry(
          { _id: id },
          payloadData,
          options
        );
        if (!result) {
          res.send({
            success: false,
            message: message.NO_USER_FOUND,
          });
        } else {
          res.send({
            message: message.DATA_UPDATED,
            success: true,
            result,
          });
        }
      } else {
        res.send({
          success: false,
          message: message.UNAUTHORIZED,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  /*  Industry */
  async getIndustry(req, res) {
    try {
      let id = req.params.id;
      const result = await this.industryUseCase.getIndustry({ _id: id });
      if (!result) {
        res.send({
          success: false,
          message: message.NO_DATA_FOUND,
        });
      } else {
        res.status(200).send({
          success: true,
          message: message.FETCHED,
          result,
        });
      }
    } catch (err) {
      res.status(500).send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  /*  Industry List*/
  async getIndustryList(req, res) {
    try {
      let payloadData = { status: 'Live' };
      const result = await this.industryUseCase.getIndustries(payloadData);
      if (!result) {
        res.send({
          success: false,
          message: message.NO_DATA_FOUND,
        });
      } else {
        res.status(200).send({
          success: true,
          count: result.length,
          result,
        });
      }
    } catch (err) {
      res.status(500).send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  /*  Industry Pagination*/
  async getIndustryPagination(req, res) {
    try {
      const result = await this.industryUseCase.paginate(req.query);
      if (!result) {
        res.send({
          success: false,
          message: message.NO_DATA_FOUND,
        });
      } else {
        res.status(200).send({
          success: true,
          result: result.result,
          total: result.total,
        });
      }
    } catch (err) {
      res.status(500).send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  /* Delete Industry */
  async deleteIndustry(req, res) {
    try {
      let role = this.context.currentUser.role;
      if (role === 2 || role === 3) {
        let id = req.params.id;
        const result = await this.industryUseCase.deleteIndustry({
          _id: id,
        });
        if (!result) {
          res.send({
            success: false,
            message: message.NO_DATA_FOUND,
          });
        } else {
          res.status(200).send({
            success: true,
            message: message.DATA_DELETED,
          });
        }
      } else {
        res.send({
          success: false,
          message: message.UNAUTHORIZED,
        });
      }
    } catch (err) {
      res.status(500).send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  async getIndustryAccToProfession(req, res) {
    try {
      const result = await this.industryUseCase.getIndustryAccToProfession(
        req.query
      );
      if (!result) {
        res.send({
          success: false,
          message: message.NO_DATA_FOUND,
        });
      } else {
        res.status(200).send({
          success: true,
          message: message.FETCHED,
          result,
        });
      }
    } catch (err) {
      res.status(500).send({
        success: false,
        message: message.ERROR,
      });
    }
  }
}
