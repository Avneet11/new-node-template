

import { CoreUsecase } from '../../core/service/usecase';
import { ProfileRepo } from './repo';
import { PayloadTooLarge } from 'http-errors';

/**
 * @export
 * @class ProfileUseCase
 * @extends {ProfileUseCase}
 */
export class ProfileUseCase extends CoreUsecase {
  /**
   * Creates an instance of ProfileUseCase.
   * @memberof ProfileUseCase
   */
  constructor() {
    const profileRepo = new ProfileRepo();
    super(profileRepo);
    this.profileRepo = profileRepo;
  }

  async paginate({ page, limit }) {
    const query = this.buildPaginateQuery({});
    const result = await this.profileRepo.paginate(query, page, limit);

    return {
      result: result.docs,
      total: result.total,
    };
  }

  async paginateUserRequest(data) {
    const result = await this.profileRepo.paginate(data, data.page, data.limit);
    return {
      result: result.docs,
      total: result.total,
    };
  }

  buildPaginateQuery({}) {
    const query = {};
    return query;
  }

  async paginateLang(query, { page, limit }) {
    const result = await this.profileRepo.paginate(query, page, limit);
    return {
      result: result.docs,
      total: result.total,
    };
  }

  async userRequest(data) {
    const result = await this.profileRepo.userRequest(data);
    return result;
  }

  async getSimilarProfile() {
    const result = await this.profileRepo.getSimilarProfile();
    return { result };
  }

  addProfile(data) {
    return this.profileRepo.create(data);
  }

  updateProfile(criteria, dataToSet, option) {
    return this.profileRepo.findOneAndUpdate(criteria, dataToSet, option);
  }

  async getAllProfileFilter(data) {
    const result = await this.profileRepo.filter(data);
    return result;
  }

  async accToAge(data) {
    const result = await this.profileRepo.accToAge(data);
    return result;
  }

  async resultAccToAge(data, queryToSend) {
    const result = await this.profileRepo.resultAccToAge(data, queryToSend);
    return result;
  }

  async groupAccToMonth(data) {
    const result = await this.profileRepo.groupAccToMonth(data);
    return result;
  }

  async recordGroupAccToMonth(data, queryToSend) {
    const result = await this.profileRepo.recordGroupAccToMonth(
      data,
      queryToSend
    );
    return result;
  }

  async groupAccToYear(data) {
    const result = await this.profileRepo.groupAccToYear(data);
    return result;
  }

  async recordGroupAccToYear(data, queryToSend) {
    const result = await this.profileRepo.recordGroupAccToYear(
      data,
      queryToSend
    );
    return result;
  }

  async accToLang(data) {
    const result = await this.profileRepo.accToLang(data);
    return result;
  }

  async resultAccToLang(data, queryToSend) {
    const result = await this.profileRepo.resultAccToLang(data, queryToSend);
    return result;
  }

  async birthDay(data) {
    const result = await this.profileRepo.birthDay(data);
    return result;
  }

  async resultBirthDay(data, queryToSend) {
    const result = await this.profileRepo.resultBirthDay(data, queryToSend);
    return result;
  }

  async accToCurrentCity(data) {
    const result = await this.profileRepo.accToCurrentCity(data);
    return result;
  }

  async resultAccToCurrentCity(data, queryToSend) {
    const result = await this.profileRepo.resultAccToCurrentCity(
      data,
      queryToSend
    );
    return result;
  }

  async byName(data) {
    const result = await this.profileRepo.byName(data);
    return result;
  }

  async byGroupName(data) {
    const result = await this.profileRepo.byGroupName(data);
    return result;
  }

  async resultByName(data, queryToSend) {
    const result = await this.profileRepo.resultByName(data, queryToSend);
    return result;
  }

  async accToBirthCity(data) {
    const result = await this.profileRepo.accToBirthCity(data);
    return result;
  }

  async resultAccToBirthCity(data, queryToSend) {
    const result = await this.profileRepo.resultAccToBirthCity(
      data,
      queryToSend
    );
    return result;
  }

  async byIndustryName(data) {
    const result = await this.profileRepo.byIndustryName(data);
    return result;
  }

  async byType(data) {
    const result = await this.profileRepo.byType(data);
    return result;
  }

  async accCurrentCountry(data) {
    const result = await this.profileRepo.accCurrentCountry(data);
    return result;
  }

  async resultAccCurrentCountry(data, queryToSend) {
    const result = await this.profileRepo.resultAccCurrentCountry(
      data,
      queryToSend
    );
    return result;
  }

  async countryOfBirth(data) {
    const result = await this.profileRepo.countryOfBirth(data);
    return result;
  }

  async resultByCountryOfBirth(data, queryToSend) {
    const result = await this.profileRepo.resultByCountryOfBirth(
      data,
      queryToSend
    );
    return result;
  }

  async provinceOfBirth(data) {
    const result = await this.profileRepo.provinceOfBirth(data);
    return result;
  }

  async resultByProvinceOfBirth(data, queryToSend) {
    const result = await this.profileRepo.resultByProvinceOfBirth(
      data,
      queryToSend
    );
    return result;
  }

  async currentProvince(data) {
    const result = await this.profileRepo.currentProvince(data);
    return result;
  }

  async resultByCurrentProvince(data, queryToSend) {
    const result = await this.profileRepo.resultByCurrentProvince(
      data,
      queryToSend
    );
    return result;
  }

  async zodiacSign(data) {
    const result = await this.profileRepo.zodiacSign(data);
    return result;
  }

  async resultZodiacSign(data, queryToSend) {
    const result = await this.profileRepo.resultZodiacSign(data, queryToSend);
    return result;
  }

  async searchProfile(data) {
    const result = await this.profileRepo.searchProfile(data);
    return result;
  }

  async netWorth(data) {
    const result = await this.profileRepo.netWorth(data);
    return result;
  }

  async netWorthFilter(data) {
    const result = await this.profileRepo.netWorthFilter(data);
    return result;
  }

  async paginateHouseTour(query) {
    const result = await this.profileRepo.paginateHouseTour(query);
    return {
      result: result.docs,
      total: result.total,
    };
  }

  async profileAccToProfession(data) {
    const result = await this.profileRepo.profileAccToProfession(data);
    return result;
  }

  async birthYearNumber() {
    const result = await this.profileRepo.birthYearNumber();
    return result;
  }

  async getProfileAccToSearch(valueToSearch) {
    const result = await this.profileRepo.getProfileAccToSearch(valueToSearch);
    return result;
  }

  async birthDateNumber(valueToSearch) {
    const result = await this.profileRepo.birthDateNumber(valueToSearch);
    return result;
  }

  async ageCount() {
    const result = await this.profileRepo.ageCount();
    return result;
  }

  async getProfileGroup(data) {
    const result = await this.profileRepo.filterGroup(data);
    return result;
  }

  deleteProfile(id) {
    return this.profileRepo.deleteOne(id);
  }
  getProfile(data) {
    
    return this.profileRepo.find(data);
  }

  getProfileData(data) {
    return this.profileRepo.getProfileData(data);
  }

  getOneProfileData(data) {
    return this.profileRepo.getOneProfileData(data);
  }

  async similarBirthDay(data, id) {
    const result = await this.profileRepo.similarBirthDay(data, id);
    return result;
  }

  getOneProfile(data) {
    return this.profileRepo.findOne(data);
  }

  async resultByType(data, queryToSend) {
    const result = await this.profileRepo.resultByType(data, queryToSend);
    return result;
  }

  async resultByGroupName(data, queryToSend) {
    const result = await this.profileRepo.resultByGroupName(data, queryToSend);
    return result;
  }

  async resultByIndustryName(data, queryToSend) {
    const result = await this.profileRepo.resultByIndustryName(
      data,
      queryToSend
    );
    return result;
  }
}
