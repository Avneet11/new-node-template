

const fs = require('fs');
const multer = require('multer');
const dir = './uploads/profile';
const mongoose = require('mongoose');
import { ProfileUseCase } from './usecase';
import { MediaUseCase } from '../media/usecase';
//import { SocialLinkUseCase } from '../socialLink/usecase';
import { GroupUseCase } from '../group/usecase';
import { ProfessionUseCase } from '../profession/usecase';
import { IndustryUseCase } from '../industry/usecase';
import { UserUsecase } from '../auth-manager/usecase';
//import { PaymentUseCase } from '../payment/usecase';
import { AppError } from '../../core/errors';
import { Common } from '../../helpers/common';
import * as message from '../../core/responseMessages';
import { Context } from '../../helpers/context';
const awsMailer = require('../../helpers/awsMailer');

/**
 * @export
 * @class ProductHandler
 */

const storage = multer.diskStorage({
  /* destination*/
  destination: function (req, file, cb) {
    cb(null, dir);
  },
  filename: function (req, file, cb) {
    cb(null, new Date().getTime().toString() + '-' + file.originalname);
  },
});
const upload = multer({ storage: storage }).fields([
  { name: 'profileImg' },
  { name: 'images' },
  { name: 'videos' },
]);
/**
 * @export
 * @class ProfileHandler
 */
export class ProfileHandler {
  /**
   * Creates an instance of ProfileHandler.
   * @memberof ProfileHandler
   */
  constructor() {
    this.profileUseCase = new ProfileUseCase();
    this.mediaUsecase = new MediaUseCase();
    //this.socialLinkUseCase = new SocialLinkUseCase();
    this.groupUseCase = new GroupUseCase();
    this.userUsecase = new UserUsecase();
    this.industryUseCase = new IndustryUseCase();
    this.professionUseCase = new ProfessionUseCase();
    //this.paymentUseCase = new PaymentUseCase();
    this.context = new Context();
  }

  /* Add Profile function */
  async addProfile(req, res) {
    try {
      if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
      }
      upload(req, res, async (err) => {
        if (err) {
          res.json({
            success: false,
            message: err,
          });
        } else {
          let payloadData = req.body;
          let images = [];
          let videos = [];
          let Media = req.files;
          
          if (Media) {
            if (Media.images) {
              Media.images.map((data) => {
                images.push(data.path);
              });
              payloadData.images = images;
            }
            if (Media.videos) {
              Media.videos.map((data) => {
                videos.push(data.path);
              });
              payloadData.videos = videos;
            }
            if (Media.profileImg) {
              payloadData.profileImg = Media.profileImg[0].path;
            }
          }
          
          if(payloadData.videoLink && payloadData.videoLink!=''){
            payloadData.videoLink = payloadData.videoLink;
          }else{
            payloadData.videoLink=[];
          }
          if (payloadData.otherLang && payloadData.otherLang!='') {
            payloadData.otherLang = payloadData.otherLang.split(',');
          }else{
            payloadData.otherLang = [];
          }
          if(payloadData.profession && payloadData.profession!=''){
            payloadData.profession = payloadData.profession.split(',');
          }else{
            payloadData.profession = [];
          }
          if(payloadData.industry  && payloadData.industry!=''){
            payloadData.industry = payloadData.industry.split(',');
          }else{
            payloadData.industry = [];
          }
          if(payloadData.group && payloadData.group!=''){
            payloadData.group = payloadData.group.split(',');
          }else{
            payloadData.group = [];
          }
          if (payloadData.mainResidence) {
            payloadData.mainResidence = JSON.parse(payloadData.mainResidence);
          }
          if (payloadData.home1) {
            payloadData.home1 = JSON.parse(payloadData.home1);
          }
          if (payloadData.home2) {
            payloadData.home2 = JSON.parse(payloadData.home2);
          }
          if (payloadData.home3) {
            payloadData.home3 = JSON.parse(payloadData.home3);
          }
          if (payloadData.home4) {
            payloadData.home4 = JSON.parse(payloadData.home4);
          }
          if (payloadData.home5) {
            payloadData.home5 = JSON.parse(payloadData.home5);
          }
          payloadData.dob = payloadData.dob ? new Date(payloadData.dob) : '';
          payloadData.isVerified = true;
          payloadData.requestStatus = 'Completed';
          const mediaResult = await this.mediaUsecase.Content(payloadData);
          payloadData.media = mediaResult._id;
          let socialResult = await this.socialLinkUseCase.SocialLink(
            payloadData
          );
          payloadData.createdBy = this.context.currentUser._id;
          payloadData.social = socialResult._id;
          let iterator=[];
          if(payloadData.group && payloadData.group!=''){
            iterator = payloadData.group.values();

          }
          let industryIterator =[];
          if(payloadData.group && payloadData.industry!=''){
            industryIterator = payloadData.industry.values();
          }
          let professionIterator=[];
          if(payloadData.group && payloadData.profession!=''){
            professionIterator = payloadData.profession.values();
          }
          let result = await this.profileUseCase.addProfile(payloadData);
          if (!result) {
            res.send({
              success: false,
              message: message.ERROR,
            });
          } else {
            let idToSet = result._id;
            let options = { new: true };
            if(payloadData.group && payloadData.group!=''){
              for (const value of iterator) {
                await this.groupUseCase.updateGroup(
                  { _id: value },
                  { $addToSet: { peopleRelated: idToSet } },
                  options
                );
              }
            }
            if(payloadData.group && payloadData.industry!=''){
              for (const value of industryIterator) {
                await this.industryUseCase.updateIndustry(
                  { _id: value },
                  { $addToSet: { peopleRelated: idToSet } },
                  options
                );
              }
            }
            if(payloadData.group && payloadData.profession!=''){
              for (const value of professionIterator) {
                await this.professionUseCase.updateProfession(
                  { _id: value },
                  { $addToSet: { peopleRelated: idToSet } },
                  options
                );
              }
            }
            res.send({
              message: message.ADDED('Profile'),
              success: true,
              result,
            });
          }
        }
      });
    } catch (err) {
      res.send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  async editProfile(req, res) {
    try {
      let role = this.context.currentUser.role;
      if (role === 2 || role === 3) {
        if (!fs.existsSync(dir)) {
          fs.mkdirSync(dir);
        }
        upload(req, res, async (err) => {
          let id = req.params.id;
          let payloadData = req.body;
          let image = payloadData.images ? payloadData.images : [];
          let video = payloadData.videos ? payloadData.videos : [];
          let Media = req.files;
          let images = [];
          let videos = [];
          if (Media) {
            if (Media.images) {
              Media.images.map((data) => {
                images.push(data.path);
              });
              if (!Array.isArray(image)) {
                image = [image];
              }
              let imageToPush = [...image, ...images];
              payloadData.images = imageToPush;
            }
            if (Media.videos) {
              Media.videos.map((data) => {
                videos.push(data.path);
              });
              if (!Array.isArray(video)) {
                video = [video];
              }
              let videoToPush = [...video, ...videos];
              payloadData.videos = videoToPush;
            }
            if (Media.profileImg) {
              payloadData.profileImg = Media.profileImg[0].path;
            }
          }
          
          if (payloadData.profession && payloadData.profession!='') {
            payloadData.profession = payloadData.profession.split(',');
          }else{
            payloadData.profession =[]
          }
          if (payloadData.otherLang && payloadData.otherLang!='') {
            payloadData.otherLang = payloadData.otherLang.split(',');
          }else{
            payloadData.otherLang =[]
          }
          if (payloadData.industry && payloadData.industry!='') {
            payloadData.industry = payloadData.industry.split(',');
          }else{
            payloadData.industry=[]
          }
          if (payloadData.group && payloadData.group!='') {
            payloadData.group = payloadData.group.split(',');
          }else{
            payloadData.group=[]
          }
          if (payloadData.mainResidence && payloadData.mainResidence!='') {
            payloadData.mainResidence = JSON.parse(payloadData.mainResidence);
          }
          if (payloadData.home1 && payloadData.home1!='') {
            payloadData.home1 = JSON.parse(payloadData.home1);
          }
          if (payloadData.home2 && payloadData.home2!='') {
            payloadData.home2 = JSON.parse(payloadData.home2);
          }
          if (payloadData.home3 && payloadData.home3!='') {
            payloadData.home3 = JSON.parse(payloadData.home3);
          }
          if (payloadData.home4 && payloadData.home4!='') {
            payloadData.home4 = JSON.parse(payloadData.home4);
          }
          if (payloadData.home5 && payloadData.home5!='') {
            payloadData.home5 = JSON.parse(payloadData.home5);
          }
          if (payloadData.dob && payloadData.dob!='') {
            payloadData.dob = new Date(payloadData.dob);
          }
          payloadData.updatedBy = this.context.currentUser._id;
          let options = { new: true };
          payloadData.updatedAt = new Date();
          let result = await this.profileUseCase.updateProfile(
            { _id: id },
            payloadData,
            options
          );
          if (!result) {
            res.send({
              success: false,
              message: message.NO_USER_FOUND,
            });
          } else {
            if (result.social) {
              let result1 = await this.socialLinkUseCase.updateSocialLink(
                { _id: result.social },
                payloadData,
                options
              );
            }
            if (result.media) {
              await this.mediaUsecase.updateByMediaId(
                { _id: result.media },
                payloadData,
                options
              );
            }
            let idToSet = result._id;
            let options = { new: true };
            if (payloadData.group) {
              const iterator = payloadData.group.values();
              for (const value of iterator) {
                await this.groupUseCase.updateGroup(
                  { _id: value },
                  { $addToSet: { peopleRelated: idToSet } },
                  options
                );
              }
            }
            if (payloadData.industry) {
              const industryIterator = payloadData.industry.values();
              for (const value of industryIterator) {
                await this.industryUseCase.updateIndustry(
                  { _id: value },
                  { $addToSet: { peopleRelated: idToSet } },
                  options
                );
              }
            }
            if (payloadData.profession) {
              const professionIterator = payloadData.profession.values();
              for (const value of professionIterator) {
                await this.professionUseCase.updateProfession(
                  { _id: value },
                  { $addToSet: { peopleRelated: idToSet } },
                  options
                );
              }
            }
            res.send({
              message: message.DATA_UPDATED,
              success: true,
              result,
            });
          }
        });
      } else {
        res.send({
          success: false,
          message: message.UNAUTHORIZED,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  async getProfile(req, res) {
    try {
      let dataToSend = {
        id: req.params.id,
        userId: req.query.userId,
      };
      let result = await this.profileUseCase.getOneProfileData(dataToSend);
      let similarZodiacSign = await this.profileUseCase
        .getProfile({
          zodiacSign: result[0].zodiacSign,
          _id: { $ne: mongoose.Types.ObjectId(result[0]._id) },
          status: 'Live',
        })
        .limit(5)
        .populate("social")
        // .populate('profession')
        // .where('profession')
        // .ne([]);
      let similarBirthDay = await this.profileUseCase.similarBirthDay(
        result[0].dob,
        result[0]._id
      );

      if (!result) {
        res.send({
          success: false,
          message: message.NO_USER_FOUND,
        });
      } else {
        res.status(200).send({
          success: true,
          message: message.SUCCESS_MESSAGE,
           result: result[0],
          similarZodiacSign,
          similarBirthDay,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  async getAllProfile(req, res) {
    try {
      let payloadData = req.query;
      let result = await this.profileUseCase.paginate(payloadData);
      if (!result) {
        res.send({
          success: false,
          message: message.NO_DATA_FOUND,
        });
      } else {
        res.send({
          success: true,
          message: message.FETCHED,
          data: result,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: err.message,
      });
    }
  }

  async allProfile(req, res) {
    try {
      let payloadData = req.query;
      let result = await this.profileUseCase
        .getProfile(payloadData)
        .select(
          'fullName _id profileImg nickName industry profession status updatedAt'
        )
        .populate('profession')
        .populate('industry');
      if (!result) {
        res.send({
          success: false,
          message: message.NO_DATA_FOUND,
        });
      } else {
        res.send({
          success: true,
          message: message.FETCHED,
          data: result,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: err.message,
      });
    }
  }

  async getProfileAccBtwf(req, res) {
    try {
      let payloadData = req.query;
      let result = await this.profileUseCase
        .getProfileData(payloadData)
        .limit(10);
      if (!result) {
        res.send({
          success: false,
          message: message.NO_DATA_FOUND,
        });
      } else {
        res.send({
          success: true,
          message: message.FETCHED,
          data: result,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: err.message,
      });
    }
  }

  async getSimilarProfile(req, res) {
    try {
      let payloadData = req.query;
      let result = await this.profileUseCase
        .getProfile(payloadData)
        .limit(5)
        .populate('social')
        .populate({
          path: 'profession',
          match: { professionType: { $in: [req.query.type] } },
        })
        .sort({ btwfScore: -1 });
      if (!result) {
        res.send({
          success: false,
          message: message.NO_DATA_FOUND,
        });
      } else {
        res.send({
          success: true,
          message: message.FETCHED,
          data: result,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: err.message,
      });
    }
  }

  async getProfileAccToSimilar(req, res) {
    try {
      let id = req.params.id;
      let result = await this.profileUseCase
        .getOneProfile({ _id: id })
        .populate('social')
        .populate('profession')
        .populate('media');
      let datatosend;
      for (let element of result.profession) {
        datatosend = element.professionType;
      }
      let similarResult = await this.profileUseCase
        .getProfile({
          _id: { $ne: mongoose.Types.ObjectId(id) },
          status: 'Live',
        })
        .limit(5)
        .populate('social')
        .where('profession')
        .ne([])
        .populate({
          path: 'profession',
          match: { professionType: { $in: [datatosend] } },
        })

        .sort({ btwfScore: -1 });
      if (!result) {
        res.send({
          success: false,
          message: message.NO_DATA_FOUND,
        });
      } else {
        res.send({
          success: true,
          message: message.FETCHED,
          data: result,
          similarResult,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: err.message,
      });
    }
  }

  async getSimilarProfileAcc(req, res) {
    try {
      let payloadData = req.query;
      let result = await this.profileUseCase
        .getProfile(payloadData)
        .limit(5)
        .populate('social')
        .populate({
          path: 'profession',
          match: { professionType: { $in: [req.query.type] } },
        })
        .sort({ btwfScore: -1 });
      if (!result) {
        res.send({
          success: false,
          message: message.NO_DATA_FOUND,
        });
      } else {
        res.send({
          success: true,
          message: message.FETCHED,
          data: result,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: err.message,
      });
    }
  }

  async deleteProfile(req, res) {
    try {
      let role = this.context.currentUser.role;
      if (role == 2 || role == 3) {
        let id = req.params.id;
        let result = await this.profileUseCase.getOneProfile({
          _id: id,
        });
        if (result) {
          if (result.social) {
            let result1 = await this.socialLinkUseCase.delete({
              _id: result.social,
            });
            if (!result1) {
              res.send({
                sucess: false,
                message: 'Social Links not deleted properly',
              });
            }
          }
          let result2 = await this.profileUseCase.deleteProfile({
            _id: id,
          });
          if (!result2) {
            res.send({
              success: false,
              message: message.NO_USER_FOUND,
            });
          } else {
            res.send({
              message: message.DATA_DELETED,
              success: true,
            });
          }
        }
      }
    } catch (err) {
      res.send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  async getProfileById(req, res) {
    try {
      let role = this.context.currentUser.role;
      if (role === 2 || role === 3) {
        let id = req.params.id;
        let result = await this.profileUseCase
          .getOneProfile({ _id: id })
          .populate('social');
        if (!result) {
          res.send({
            success: false,
            message: message.NO_USER_FOUND,
          });
        } else {
          res.status(200).send({
            success: true,
            message: message.SUCCESS_MESSAGE,
            result,
          });
        }
      } else {
        res.send({
          success: false,
          message: message.UNAUTHORIZED,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  async getRecentProfile(req, res) {
    try {
      let result = await this.profileUseCase
        .getProfile()
        .sort({ updatedAt: -1 })
        .limit(10)
        .select('fullName updatedAt');
      if (!result) {
        res.send({
          success: false,
          message: message.NO_USER_FOUND,
        });
      } else {
        res.status(200).send({
          success: true,
          message: message.SUCCESS_MESSAGE,
          result,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  async getAllProfileLang(req, res) {
    try {
      let payloadData = req.query;
      let queryToSend = {
        primaryLang: 'Spanish',
      };
      let result = await this.profileUseCase.paginateLang(
        queryToSend,
        payloadData
      );
      if (!result) {
        res.send({
          success: false,
          message: message.NO_DATA_FOUND,
        });
      } else {
        res.send({
          success: true,
          message: message.FETCHED,
          data: result,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: err.message,
      });
    }
  }

  async getAllProfileFilter(req, res) {
    try {
      let payloadData = req.query;
      const result = await this.profileUseCase.getAllProfileFilter(payloadData);
      if (!result) {
        res.send({
          success: false,
          message: message.NO_DATA_FOUND,
        });
      } else {
        res.send({
          success: true,
          message: message.FETCHED,
          data: result[0].paginatedResults,
          total: result[0].totalCount[0] ? result[0].totalCount[0].count : 0,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: err.message,
      });
    }
  }

  async accToAge(req, res) {
    try {
      let payloadData = req.query.keyword;
      let ageToSend = parseInt(payloadData.split(' ')[0]);
      const ageResult = await this.profileUseCase.accToAge(ageToSend);
      const groupByMonth = await this.profileUseCase.groupAccToMonth(
        payloadData
      );
      const accToCurrentCity = await this.profileUseCase.accToCurrentCity(
        payloadData
      );
      const accToBirthCity = await this.profileUseCase.accToBirthCity(
        payloadData
      );
      const currentCountry = await this.profileUseCase.accCurrentCountry(
        payloadData
      );
      const birthPlaceCountry = await this.profileUseCase.countryOfBirth(
        payloadData
      );
      const astrology = await this.profileUseCase.zodiacSign(payloadData);
      const groupByYear = await this.profileUseCase.groupAccToYear(payloadData);
      const accToLang = await this.profileUseCase.accToLang(payloadData);
      const birthDay = await this.profileUseCase.birthDay(payloadData);
      const name = await this.profileUseCase.byName(payloadData);
      const groupName = await this.groupUseCase.byGroupName(payloadData);
      const industryName = await this.industryUseCase.byIndustryName(
        payloadData
      );
      const professionType = await this.professionUseCase.byType(payloadData);
      const provinceOfBirth = await this.profileUseCase.provinceOfBirth(
        payloadData
      );
      const provinceOfResidence = await this.profileUseCase.currentProvince(
        payloadData
      );
      if (!ageResult) {
        res.send({
          success: false,
          message: message.NO_DATA_FOUND,
        });
      } else {
        res.send({
          success: true,
          message: message.FETCHED,
          ageResult,
          groupByMonth,
          accToCurrentCity,
          accToBirthCity,
          currentCountry,
          birthPlaceCountry,
          astrology,
          groupByYear,
          birthDay,
          accToLang,
          name,
          groupName,
          industryName,
          professionType,
          provinceOfBirth,
          provinceOfResidence,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: err.message,
      });
    }
  }

  async getProfileGroup(req, res) {
    try {
      let payloadData = req.query;
      const result = await this.profileUseCase.getProfileGroup(payloadData);
      if (!result) {
        res.send({
          success: false,
          message: message.NO_DATA_FOUND,
        });
      } else {
        res.send({
          success: true,
          message: message.FETCHED,
          data: result,
          total: result.length,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: err.message,
      });
    }
  }

  async searchProfile(req, res) {
    try {
      let queryToSearch = req.query;
      let result = await this.profileUseCase.searchProfile(queryToSearch);
      if (!result) {
        res.send({
          success: false,
          message: message.NO_USER_FOUND,
        });
      } else {
        res.status(200).send({
          success: true,
          message: message.SUCCESS_MESSAGE,
          result: result[0].paginatedResults,
          total: result[0].totalCount[0] ? result[0].totalCount[0].count : 0,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  async netWorth(req, res) {
    try {
      let queryToSearch = req.query;
      let result = await this.profileUseCase.netWorth(queryToSearch);
      if (!result) {
        res.send({
          success: false,
          message: message.NO_USER_FOUND,
        });
      } else {
        res.status(200).send({
          success: true,
          message: message.SUCCESS_MESSAGE,
          result: result[0].paginatedResults,
          total: result[0].totalCount[0] ? result[0].totalCount[0].count : 0,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  async netWorthFilter(req, res) {
    try {
      let queryToSearch = req.query;
      let result = await this.profileUseCase.netWorthFilter(queryToSearch);
      if (!result) {
        res.send({
          success: false,
          message: message.NO_USER_FOUND,
        });
      } else {
        res.status(200).send({
          success: true,
          message: message.SUCCESS_MESSAGE,
          result: result[0].paginatedResults,
          total: result[0].totalCount[0] ? result[0].totalCount[0].count : 0,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  async houseTourFamousList(req, res) {
    try {
      let queryToSearch = req.query;
      let result = await this.profileUseCase.paginateHouseTour(queryToSearch);
      if (!result) {
        res.send({
          success: false,
          message: message.NO_USER_FOUND,
        });
      } else {
        res.status(200).send({
          success: true,
          message: message.SUCCESS_MESSAGE,
          data: result.result,
          total: result.total,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  async updateBtwfScore(req, res) {
    try {
      let idToSet = req.body.userId;
      let idToUpdate = req.params.id;
      let profileResult = await this.profileUseCase.getOneProfile({
        _id: idToUpdate,
        btwfCount: { $in: idToSet },
      });
      let datatoset;
      if (!profileResult) {
        datatoset = {
          $addToSet: { btwfCount: idToSet },
          $inc: { btwfScore: 1 },
          $set: { isBtwf: true },
        };
      } else {
        datatoset = {
          $pull: { btwfCount: idToSet },
          $inc: { btwfScore: -1 },
          $set: { isBtwf: false },
        };
      }
      datatoset.updatedAt = new Date();
      let result = await this.profileUseCase.updateProfile(
        { _id: idToUpdate },
        datatoset,
        { new: true }
      );
      if (!result) {
        res.send({
          success: false,
          message: message.NO_DATA_FOUND,
        });
      } else {
        res.status(200).send({
          success: true,
          message: message.SUCCESS_MESSAGE,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  async profileAccToProfession(req, res) {
    try {
      let queryToSearch = req.query;
      let result = await this.profileUseCase.profileAccToProfession(
        queryToSearch
      );
      if (!result) {
        res.send({
          success: false,
          message: message.NO_USER_FOUND,
        });
      } else {
        res.status(200).send({
          success: true,
          message: message.SUCCESS_MESSAGE,
          result: result[0].paginatedResults,
          total: result[0].totalCount[0] ? result[0].totalCount[0].count : 0,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  async checkRandomNumber(req, res) {
    try {
      let queryToSearch = {
        btwfCount: {
          $all: [req.query.userId],
        },
      };
      let result = await this.profileUseCase.getProfile(queryToSearch);
      if (!result.length) {
        res.send({
          success: false,
          message: message.NO_DATA_FOUND,
        });
      } else {
        res.status(200).send({
          success: true,
          message: message.SUCCESS_MESSAGE,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  async accToResult(req, res) {
    try {
      let payloadData = req.query.keyword.split(' ')[0];
      let splitDataToSend = req.query.keyword.split('(')[1];
      if (splitDataToSend == 'Years Old)') {
        let payloadData1 = parseInt(payloadData);
        const ageResult = await this.profileUseCase.resultAccToAge(
          payloadData1,
          req.query
        );
        res.send({
          success: true,
          message: message.FETCHED,
          data: ageResult[0].paginatedResults,
          total: ageResult[0].totalCount[0]
            ? ageResult[0].totalCount[0].count
            : 0,
        });
      }
      if (splitDataToSend == 'Language Spoken by Famous People)') {
        const accToLang = await this.profileUseCase.resultAccToLang(
          payloadData,
          req.query
        );
        res.send({
          success: true,
          message: message.FETCHED,
          data: accToLang[0].paginatedResults,
          total: accToLang[0].totalCount[0]
            ? accToLang[0].totalCount[0].count
            : 0,
        });
      }
      if (splitDataToSend == 'City of Birth)') {
        const accToBirthCity = await this.profileUseCase.resultAccToBirthCity(
          payloadData,
          req.query
        );
        res.send({
          success: true,
          message: message.FETCHED,
          data: accToBirthCity[0].paginatedResults,
          total: accToBirthCity[0].totalCount[0]
            ? accToBirthCity[0].totalCount[0].count
            : 0,
        });
      }
      if (splitDataToSend == 'Famous People Birthdays this Month)') {
        const groupByMonth = await this.profileUseCase.recordGroupAccToMonth(
          payloadData,
          req.query
        );
        res.send({
          success: true,
          message: message.FETCHED,
          data: groupByMonth[0].paginatedResults,
          total: groupByMonth[0].totalCount[0]
            ? groupByMonth[0].totalCount[0].count
            : 0,
        });
      }
      if (splitDataToSend == 'City of Residence)') {
        const accToCurrentCity = await this.profileUseCase.resultAccToCurrentCity(
          payloadData,
          req.query
        );
        res.send({
          success: true,
          message: message.FETCHED,
          data: accToCurrentCity[0].paginatedResults,
          total: accToCurrentCity[0].totalCount[0]
            ? accToCurrentCity[0].totalCount[0].count
            : 0,
        });
      }
      if (splitDataToSend == 'Birthday)') {
        const birthDay = await this.profileUseCase.resultBirthDay(
          payloadData,
          req.query
        );
        res.send({
          success: true,
          message: message.FETCHED,
          data: birthDay[0].paginatedResults,
          total: birthDay[0].totalCount[0]
            ? birthDay[0].totalCount[0].count
            : 0,
        });
      }
      if (splitDataToSend == 'year)') {
        const groupByYear = await this.profileUseCase.recordGroupAccToYear(
          payloadData,
          req.query
        );
        res.send({
          success: true,
          message: message.FETCHED,
          data: groupByYear[0].paginatedResults,
          total: groupByYear[0].totalCount[0]
            ? groupByYear[0].totalCount[0].count
            : 0,
        });
      }
      if (splitDataToSend == 'Industry)') {
        const industryName = await this.industryUseCase.resultByIndustryName(
          payloadData,
          req.query
        );
        res.send({
          success: true,
          message: message.FETCHED,
          data: industryName[0].paginatedResults,
          total: industryName[0].totalCount[0]
            ? industryName[0].totalCount[0].count
            : 0,
        });
      }
      if (splitDataToSend == 'Profession)') {
        const professionType = await this.professionUseCase.resultByType(
          payloadData,
          req.query
        );
        res.send({
          success: true,
          message: message.FETCHED,
          data: professionType[0].paginatedResults,
          total: professionType[0].totalCount[0]
            ? professionType[0].totalCount[0].count
            : 0,
        });
      }
      if (splitDataToSend == 'Famous Person’s Name)') {
        const name = await this.profileUseCase.resultByName(
          payloadData,
          req.query
        );
        res.send({
          success: true,
          message: message.FETCHED,
          data: name[0].paginatedResults,
          total: name[0].totalCount[0] ? name[0].totalCount[0].count : 0,
        });
      }
      if (splitDataToSend.split(' ')[0] == 'Group') {
        req.query.id = splitDataToSend.split('-')[1];
        const groupName = await this.groupUseCase.resultByGroupName(
          payloadData,
          req.query
        );
        res.send({
          success: true,
          message: message.FETCHED,
          data: groupName[0].paginatedResults,
          total: groupName[0].totalCount[0]
            ? groupName[0].totalCount[0].count
            : 0,
        });
      }
      if (splitDataToSend == 'astrology)') {
        const astrology = await this.profileUseCase.resultZodiacSign(
          payloadData,
          req.query
        );
        res.send({
          success: true,
          message: message.FETCHED,
          data: astrology[0].paginatedResults,
          total: astrology[0].totalCount[0]
            ? astrology[0].totalCount[0].count
            : 0,
        });
      }
      if (splitDataToSend == 'Country of Residence)') {
        const currentCountry = await this.profileUseCase.resultAccCurrentCountry(
          payloadData,
          req.query
        );
        res.send({
          success: true,
          message: message.FETCHED,
          data: currentCountry[0].paginatedResults,
          total: currentCountry[0].totalCount[0]
            ? currentCountry[0].totalCount[0].count
            : 0,
        });
      }
      if (splitDataToSend == 'Province of Birth)') {
        const provinceOfBirth = await this.profileUseCase.resultByProvinceOfBirth(
          payloadData,
          req.query
        );
        res.send({
          success: true,
          message: message.FETCHED,
          data: provinceOfBirth[0].paginatedResults,
          total: provinceOfBirth[0].totalCount[0]
            ? provinceOfBirth[0].totalCount[0].count
            : 0,
        });
      }
      if (splitDataToSend == 'Province of Residence)') {
        const provinceOfResidence = await this.profileUseCase.resultByCurrentProvince(
          payloadData,
          req.query
        );
        res.send({
          success: true,
          message: message.FETCHED,
          data: provinceOfResidence[0].paginatedResults,
          total: provinceOfResidence[0].totalCount[0]
            ? provinceOfResidence[0].totalCount[0].count
            : 0,
        });
      }
      if (splitDataToSend == 'Country of Birth)') {
        const birthPlaceCountry = await this.profileUseCase.resultByCountryOfBirth(
          payloadData,
          req.query
        );
        res.send({
          success: true,
          message: message.FETCHED,
          data: birthPlaceCountry[0].paginatedResults,
          total: birthPlaceCountry[0].totalCount[0]
            ? birthPlaceCountry[0].totalCount[0].count
            : 0,
        });
      } else {
        res.send({
          success: false,
          message: message.NO_DATA_FOUND,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: err.message,
      });
    }
  }

  /* Add Profile function */
  async addProfileByUser(req, res) {
    try {
      if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
      }
      upload(req, res, async (err) => {
        if (err) {
          res.json({
            success: false,
            message: err,
          });
        } else {
          let payloadData = req.body;
          if (!payloadData.user_id) {
            res.json({
              success: false,
              message: 'User Id Is Required',
            });
            return false;
          }
          if (payloadData.isFree) {
            if (!payloadData.promocode) {
              res.json({
                success: false,
                message: 'Promo Code Is Required',
              });
              return false;
            }
          }
          let images = [];
          let videos = [];
          let Media = req.files;
          if (Media) {
            if (Media.images) {
              Media.images.map((data) => {
                images.push(data.path);
              });
              payloadData.images = images;
            }
            if (Media.videos) {
              Media.videos.map((data) => {
                videos.push(data.path);
              });
              payloadData.videos = videos;
            }
            if (Media.profileImg) {
              payloadData.profileImg = Media.profileImg[0].path;
            }
          }
          payloadData.videoLink = payloadData.videoLink;
          if (payloadData.otherLang) {
            payloadData.otherLang = payloadData.otherLang.split(',');
          }
          payloadData.profession = payloadData.profession.split(',');
          payloadData.industry = payloadData.industry.split(',');
          payloadData.group = payloadData.group.split(',');
          if (payloadData.mainResidence) {
            payloadData.mainResidence = JSON.parse(payloadData.mainResidence);
          }
          if (payloadData.home1) {
            payloadData.home1 = JSON.parse(payloadData.home1);
          }
          if (payloadData.home2) {
            payloadData.home2 = JSON.parse(payloadData.home2);
          }
          if (payloadData.home3) {
            payloadData.home3 = JSON.parse(payloadData.home3);
          }
          if (payloadData.home4) {
            payloadData.home4 = JSON.parse(payloadData.home4);
          }
          if (payloadData.home5) {
            payloadData.home5 = JSON.parse(payloadData.home5);
          }
          payloadData.dob = payloadData.dob ? new Date(payloadData.dob) : '';
          const mediaResult = await this.mediaUsecase.Content(payloadData);
          payloadData.media = mediaResult._id;
          let socialResult = await this.socialLinkUseCase.SocialLink(
            payloadData
          );
          payloadData.createdBy = payloadData.user_id;
          payloadData.social = socialResult._id;
          const iterator = payloadData.group.values();
          const industryIterator = payloadData.industry.values();
          const professionIterator = payloadData.profession.values();
          let result = await this.profileUseCase.addProfile(payloadData);
          if (!result) {
            res.send({
              success: false,
              message: message.ERROR,
            });
          } else {
            let idToSet = result._id;
            let options = { new: true };
            for (const value of iterator) {
              await this.groupUseCase.updateGroup(
                { _id: value },
                { $addToSet: { peopleRelated: idToSet } },
                options
              );
            }
            for (const value of industryIterator) {
              await this.industryUseCase.updateIndustry(
                { _id: value },
                { $addToSet: { peopleRelated: idToSet } },
                options
              );
            }
            for (const value of professionIterator) {
              await this.professionUseCase.updateProfession(
                { _id: value },
                { $addToSet: { peopleRelated: idToSet } },
                options
              );
            }
            res.send({
              message: message.PROFILE_SENT,
              success: true,
              result,
            });
            let userdata = await this.userUsecase.getUserById(
              payloadData.user_id
            );
            let subject = 'Add Famous Profile';
            let userName = userdata.name;
            let userEmail = userdata.email;
            let adminEmail = process.env.ADMINEMAIL;
            let type = 'Add';
            let html;
            if (userdata.isFirst) {
              html = awsMailer.newUserRequestHtml(
                userName,
                result._id,
                userEmail
              );
            } else {
              html = awsMailer.userRequestHtml(userName, result._id, type);
            }
            if (payloadData.isFree) {
              let promocode = payloadData.promocode;
              let Uhtml = awsMailer.freeHtml(promocode, type, result._id);
              awsMailer.sendMail(adminEmail, subject, Uhtml);
            } else {
              let Uhtml = awsMailer.adminHtml(userName, type, result._id);
              awsMailer.sendMail(userEmail, subject, html);
              awsMailer.sendMail(adminEmail, subject, Uhtml);
              await this.userUsecase.updateUserData(
                { _id: payloadData.user_id },
                { isFirst: false },
                { new: true }
              );
            }
          }
          if (result.media) {
            await this.mediaUsecase.updateByMediaId(
              { _id: result.media },
              payloadData,
              options
            );
          }
          let idToSet = result._id;
          let options = { new: true };
          if (payloadData.group) {
            const iterator = payloadData.group.values();
            for (const value of iterator) {
              await this.groupUseCase.updateGroup(
                { _id: value },
                { $addToSet: { peopleRelated: idToSet } },
                options
              );
            }
          }
          if (payloadData.industry) {
            const industryIterator = payloadData.industry.values();
            for (const value of industryIterator) {
              await this.industryUseCase.updateIndustry(
                { _id: value },
                { $addToSet: { peopleRelated: idToSet } },
                options
              );
            }
          }
          if (payloadData.profession) {
            const professionIterator = payloadData.profession.values();
            for (const value of professionIterator) {
              await this.professionUseCase.updateProfession(
                { _id: value },
                { $addToSet: { peopleRelated: idToSet } },
                options
              );
            }
          }
          let paymentId = payloadData.paymentId;
          let dataToSet = { $set: { profile_id: result._id } };
          const editPayment = await this.paymentUseCase.updatePayment(
            dataToSet,
            paymentId
          );
          res.send({
            message: message.DATA_UPDATED,
            success: true,
            result,
          });
        }
      });
    } catch (err) {
      res.send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  /* Edit Profile function */
  async editProfileByUser(req, res) {
    try {
      if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
      }
      upload(req, res, async (err) => {
        let payloadData = req.body;
        let id = payloadData.id;
        if (!payloadData.user_id) {
          res.json({
            success: false,
            message: 'User Id Is Required',
          });
          return false;
        }
        if (payloadData.isFree) {
          if (!payloadData.promocode) {
            res.json({
              success: false,
              message: 'Promo Code Is Required',
            });
            return false;
          }
        }
        let image = payloadData.images ? payloadData.images : [];
        let video = payloadData.videos ? payloadData.videos : [];
        let Media = req.files;
        let images = [];
        let videos = [];
        if (Media) {
          if (Media.images) {
            Media.images.map((data) => {
              images.push(data.path);
            });
            if (!Array.isArray(image)) {
              image = [image];
            }
            let imageToPush = [...image, ...images];
            payloadData.images = imageToPush;
          }
          if (Media.videos) {
            Media.videos.map((data) => {
              videos.push(data.path);
            });
            if (!Array.isArray(video)) {
              video = [video];
            }
            let videoToPush = [...video, ...videos];
            payloadData.videos = videoToPush;
          }
          if (Media.profileImg) {
            payloadData.profileImg = Media.profileImg[0].path;
          }
        }
        if (payloadData.profession) {
          payloadData.profession = payloadData.profession.split(',');
        }
        if (payloadData.otherLang) {
          payloadData.otherLang = payloadData.otherLang.split(',');
        }
        if (payloadData.industry) {
          payloadData.industry = payloadData.industry.split(',');
        }
        if (payloadData.group) {
          payloadData.group = payloadData.group.split(',');
        }
        if (payloadData.mainResidence) {
          payloadData.mainResidence = JSON.parse(payloadData.mainResidence);
        }
        if (payloadData.home1) {
          payloadData.home1 = JSON.parse(payloadData.home1);
        }
        if (payloadData.home2) {
          payloadData.home2 = JSON.parse(payloadData.home2);
        }
        if (payloadData.home3) {
          payloadData.home3 = JSON.parse(payloadData.home3);
        }
        if (payloadData.home4) {
          payloadData.home4 = JSON.parse(payloadData.home4);
        }
        if (payloadData.home5) {
          payloadData.home5 = JSON.parse(payloadData.home5);
        }
        if (payloadData.dob) {
          payloadData.dob = new Date(payloadData.dob);
        }
        payloadData.updatedBy = payloadData.user_id;
        payloadData.requestStatus = 'In Progress';
        payloadData.isVerified = false;
        let options = { new: true };
        payloadData.updatedAt = new Date();
        let result = await this.profileUseCase.updateProfile(
          { _id: id },
          payloadData,
          options
        );
        if (!result) {
          res.send({
            success: false,
            message: message.NO_USER_FOUND,
          });
        } else {
          if (result.social) {
            let result1 = await this.socialLinkUseCase.updateSocialLink(
              { _id: result.social },
              payloadData,
              options
            );
          }
          if (result.media) {
            await this.mediaUsecase.updateByMediaId(
              { _id: result.media },
              payloadData,
              options
            );
          }
          let idToSet = result._id;
          let options = { new: true };
          if (payloadData.group) {
            const iterator = payloadData.group.values();
            for (const value of iterator) {
              await this.groupUseCase.updateGroup(
                { _id: value },
                { $addToSet: { peopleRelated: idToSet } },
                options
              );
            }
          }
          if (payloadData.industry) {
            const industryIterator = payloadData.industry.values();
            for (const value of industryIterator) {
              await this.industryUseCase.updateIndustry(
                { _id: value },
                { $addToSet: { peopleRelated: idToSet } },
                options
              );
            }
          }
          if (payloadData.profession) {
            const professionIterator = payloadData.profession.values();
            for (const value of professionIterator) {
              await this.professionUseCase.updateProfession(
                { _id: value },
                { $addToSet: { peopleRelated: idToSet } },
                options
              );
            }
          }
          res.send({
            message: message.DATA_UPDATED,
            success: true,
            result,
          });
          let userdata = await this.userUsecase.getUserById(
            payloadData.user_id
          );
          let subject = 'Edit Famous Profile';
          let userName = userdata.name;
          let userEmail = userdata.email;
          let adminEmail = process.env.ADMINEMAIL;
          let type = 'Edit';
          let html;
          if (userdata.isFirst) {
            html = awsMailer.newUserRequestHtml(
              userName,
              result._id,
              userEmail,
              type
            );
          } else {
            html = awsMailer.userRequestHtml(userName, result._id, type);
          }
          if (payloadData.isFree) {
            let promocode = payloadData.promocode;
            let Uhtml = awsMailer.freeHtml(promocode, type, result._id);
            awsMailer.sendMail(adminEmail, subject, Uhtml);
          } else {
            let Uhtml = awsMailer.adminHtml(userName, type, result._id);
            await awsMailer.sendMail(userEmail, subject, html);
            await awsMailer.sendMail(adminEmail, subject, Uhtml);
            await this.userUsecase.updateUserData(
              { _id: payloadData.user_id },
              { isFirst: false },
              { new: true }
            );
          }
        }
      });
    } catch (err) {
      res.send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  /* Get Request List*/
  async requestList(req, res) {
    try {
      let role = this.context.currentUser.role;
      if (role === 2 || role === 3) {
        let payloadData = {
          isVerified: false,
          page: req.query.page,
          limit: req.query.limit,
        };
        let result = await this.profileUseCase.paginateUserRequest(payloadData);
        if (!result) {
          res.send({
            success: false,
            message: message.NO_DATA_FOUND,
          });
        } else {
          res.send({
            success: true,
            message: message.FETCHED,
            data: result,
          });
        }
      } else {
        res.send({
          success: false,
          message: message.UNAUTHORIZED,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: err.message,
      });
    }
  }

  /* Get User Request */
  async userRequest(req, res) {
    try {
      let queryToUse = req.query;
      let role = this.context.currentUser.role;
      if (role === 2) {
        queryToUse.user_id = queryToUse.user_id;
      } else {
        queryToUse.user_id = this.context.currentUser._id;
      }
      if (!queryToUse.user_id) {
        res.json({
          success: false,
          message: 'User Id Is Required',
        });
      }
      let result = await this.profileUseCase.userRequest(queryToUse);
      if (!result) {
        res.send({
          success: false,
          message: message.NO_DATA_FOUND,
        });
      } else {
        res.send({
          success: true,
          message: message.FETCHED,
          data: result[0].paginatedResults,
          total: result[0].totalCount[0] ? result[0].totalCount[0].count : 0,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: err.message,
      });
    }
  }

  /* Edit Request Status */
  async editRequestStatus(req, res) {
    try {
      let role = this.context.currentUser.role;
      if (role === 2) {
        let payloadData = req.body;
        payloadData.requestStatus = payloadData.requestStatus;
        let id = req.body.id;
        let options = { new: true };
        payloadData.updatedAt = new Date();
        let result = await this.profileUseCase.updateProfile(
          { _id: id },
          payloadData,
          options
        );
        if (!result) {
          res.send({
            success: false,
            message: message.NO_DATA_FOUND,
          });
        } else {
          res.send({
            success: true,
            message: message.DATA_UPDATED,
            data: result,
          });
        }
      } else {
        res.send({
          success: false,
          message: message.UNAUTHORIZED,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: err.message,
      });
    }
  }

  async profileGet(req, res) {
    try {
      let dataToSend = {
        id: req.params.id,
        userId: req.query.userId,
      };
      let result = await this.profileUseCase.getOneProfileData(dataToSend);
      let similarZodiacSign = await this.profileUseCase
        .getProfile({ zodiacSign: result[0].zodiacSign })
        .limit(5)
        .populate('social')
        .sort({ updatedAt: -1 })
        .where('profession')
        .ne([]);
      if (!result) {
        res.send({
          success: false,
          message: message.NO_USER_FOUND,
        });
      } else {
        res.status(200).send({
          success: true,
          message: message.SUCCESS_MESSAGE,
          result: result[0],
          similarZodiacSign,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: message.ERROR,
      });
    }
  }

  async getAll(req, res) {
    try {
      let payloadData = req.query;
      let result = await this.profileUseCase.paginate(payloadData);
      if (!result) {
        res.send({
          success: false,
          message: message.NO_DATA_FOUND,
        });
      } else {
        res.send({
          success: true,
          message: message.FETCHED,
          data: result,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: err.message,
      });
    }
  }

  async profileAll(req, res) {
    try {
      let payloadData = req.query;
      let result = await this.profileUseCase
        .getProfile(payloadData)
        .select(
          'fullName _id profileImg nickName industry profession status updatedAt'
        )
        .populate('profession')
        .populate('industry');
      if (!result) {
        res.send({
          success: false,
          message: message.NO_DATA_FOUND,
        });
      } else {
        res.send({
          success: true,
          message: message.FETCHED,
          data: result,
        });
      }
    } catch (err) {
      res.send({
        success: false,
        message: err.message,
      });
    }
  }

  async deleteProfession(req, res) {
    try {
      let role = this.context.currentUser.role;
      if (role === 2 || role === 3) {
        let id = req.params.id;
        const result = await this.professionUseCase.deleteProfession({
          _id: id,
        });
        if (!result) {
          res.send({
            success: false,
            message: message.NO_DATA_FOUND,
          });
        } else {
          res.status(200).send({
            success: true,
            message: message.DATA_DELETED,
          });
        }
      } else {
        res.send({
          success: false,
          message: message.UNAUTHORIZED,
        });
      }
    } catch (err) {
      res.status(500).send({
        success: false,
        message: message.ERROR,
      });
    }
  }
}
