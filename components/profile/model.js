

'use strict';

const mongoose = require('mongoose');
const validator = require('validator');
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const schema = mongoose.Schema(
  {
    fullName: { type: String, required: true, text: true },
    dob: { type: Date },
    gender: { type: String },
    countryOfBirth: { type: String },
    cityOfBirth: { type: String },
    currentCity: { type: String },
    currentCountry: { type: String },
    description: { type: String },
    btwfScore: { type: Number, default: 0 },
    btwfCount: [{ type: String }],
    primaryLang: { type: String },
    otherLang: { type: Array },
    profileImg: { type: String },
    nickName: { type: String },
    nationality: { type: String },
    height: { type: String },
    politicalIdeology: { type: String },
    netWorth: { type: Number },
    zodiacSign: { type: String },
    isBtwf: { type: Boolean, default: false },
    status: { type: String, enum: ['Live', 'Draft'], default: 'Draft' },
    netWorthUnit: { type: String, default: 1 },
    goatStatus: { type: String },
    currentProvince: { type: String },
    provinceOfBirth: { type: String },
    cobForfiter: { type: String },
    pobForfiter: { type: String },
    cityofBirthForfiter: { type: String },
    currCForfiter: { type: String },
    currPForfiter: { type: String },
    currCityForfiter: { type: String },
    isVerified: { type: Boolean, default: false },
    requestStatus: {
      type: String,
      enum: ['Completed', 'In Progress', 'Cancelled'],
      default: 'In Progress',
    },
    media: { type: mongoose.Schema.Types.ObjectId, ref: 'Media' },
    social: { type: mongoose.Schema.Types.ObjectId, ref: 'SocialLink'},
    group: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Group', text: true }],
    profession: [
      { type: mongoose.Schema.Types.ObjectId, ref: 'Profession', text: true,default:'' },
    ],
    industry: [
      { type: mongoose.Schema.Types.ObjectId, ref: 'Industry', text: true },
    ],
    mainResidence: { type: Object },
    home1: { type: Object },
    home2: { type: Object },
    home3: { type: Object },
    home4: { type: Object },
    home5: { type: Object },
    comments: { type: String },
    facts: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Fact' }],
    news: [{ type: mongoose.Schema.Types.ObjectId, ref: 'News' }],
    interview: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Interview' }],
    movie: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Movie' }],
    createdBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    updatedBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    createdAt: { type: Date },
    updatedAt: { type: Date },
  },
  {
    collection: 'profile',
    versionKey: false,
  }
);

schema.plugin(mongoosePaginate);
schema.plugin(mongooseAggregatePaginate);

schema.methods.toJSON = function () {
  const obj = this.toObject();
  return obj;
};

schema.pre('save', function (next) {
  const now = new Date();
  this.updatedAt = now;
  if (!this.createdAt) {
    this.createdAt = now;
  }
  next();
});

export const Profile = mongoose.model('Profile', schema);
