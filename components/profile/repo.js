

import { CoreRepo } from '../../core/service/repo';
import { Profile } from './model';
import { object } from 'joi';
import { isValidObjectId } from 'mongoose';
const mongoose = require('mongoose');

/**
 * @export
 * @class ProfileRepo
 * @extends {CoreRepo}
 */
export class ProfileRepo extends CoreRepo {
  /**
   * Creates an instance of ProfileRepo.
   * @memberof ProfileRepo
   */
  constructor() {
    super(Profile);
  }

  /**
   * @param {object} query
   * @param {number} [page]
   * @param {number} [limit]
   * @returns
   * @memberof ContentRepo
   */
  paginate(query, page, limit) {
    return this.model.paginate(query, {
      page: parseInt(page) || 1,
      limit: parseInt(limit) || 10,
      sort: { updatedAt: -1 },
      lean: true,
      new: true,
      leanWithId: false,
      populate: [
        'social',
        'media',
        {
          path: 'profession',
          model: 'Profession',
        },
        {
          path: 'industry',
          model: 'Industry',
        },
      ],
    });
  }

  paginateHouseTour(query) {
    let btwfScore;
    if (query.popularity == 'highestFirst') {
      btwfScore = -1;
    }
    if (query.popularity == 'lowestFirst') {
      btwfScore = 1;
    }
    return this.model.paginate(
      {},
      {
        page: parseInt(query.page) || 1,
        limit: parseInt(query.limit) || 10,
        sort: { btwfScore: btwfScore },
        lean: true,
        new: true,
        leanWithId: false,
        populate: [
          'social',
          'media',
          {
            path: 'profession',
            model: 'Profession',
          },
          {
            path: 'industry',
            model: 'Industry',
          },
        ],
      }
    );
  }

  getSimilarProfile(criteria) {
    return this.model.aggregate([
      {
        $lookup: {
          from: 'Profession.professionType',
          localField: 'as',
          foreignField: 'professionType',
          as: 'asdf',

          matching: { professionType: { $in: ['funny', 'politics'] } },
        },
      },
    ]);
  }

  filter(data) {
    let userId = data.userId;
    let pageNo = parseInt(data.page) || 1;
    let pageSize = parseInt(data.limit) || 10;
    if (pageNo <= 0) {
      return;
    }
    let btwfScore;
    if (data.popularity == 'mostPopular') {
      btwfScore = -1;
    }
    if (data.popularity == 'leastPopular') {
      btwfScore = 1;
    }
    let professionName = data.professionName
      ? data.professionName
      : { $exists: true };
    let gender = data.gender ? data.gender : { $exists: true };
    let currentCountry = data.currentCountry
      ? data.currentCountry
      : { $exists: true };
    let currentCity = data.currentCity ? data.currentCity : { $exists: true };
    let industryName = data.industryName
      ? data.industryName
      : { $exists: true };
    let minAge = data.minAge ? data.minAge : 0;
    let queryLang = data.catagory ? data.catagory : { $exists: true };
    let maxAge = data.maxAge ? data.maxAge : 1000;
    return this.model.aggregate([
      {
        $match: {
          primaryLang: queryLang,
          status: 'Live',
          gender: gender,
          currentCity: currentCity,
          currentCountry: currentCountry,
        },
      },
      { $sort: { btwfScore: btwfScore } },
      {
        $lookup: {
          from: 'profession',
          let: { profession: '$profession' },
          pipeline: [
            {
              $match: {
                $expr: { $in: ['$_id', { $ifNull: ['$$profession', []] }] },
                professionType: professionName,
              },
            },
          ],
          as: 'professionData',
        },
      },
      {
        $lookup: {
          from: 'industry',
          let: { industry: '$industry' },
          pipeline: [
            {
              $match: {
                $expr: { $in: ['$_id', { $ifNull: ['$$industry', []] }] },
                name: industryName,
              },
            },
          ],
          as: 'industryData',
        },
      },
      {
        $addFields: {
          date: '$dob',
          age: {
            $divide: [
              { $subtract: [new Date(), '$dob'] },
              365 * 24 * 60 * 60 * 1000,
            ],
          },
        },
      },
      {
        $addFields: {
          agetocalculate: { $ceil: '$age' },
          ageToCalculate: { $floor: '$age' },
        },
      },
      {
        $match: {
          professionData: { $ne: [] },
          industryData: { $ne: [] },
          age: {
            $gte: parseInt(minAge),
            $lte: parseInt(maxAge),
          },
        },
      },
      {
        $addFields: {
          isBtwf: {
            $cond: {
              if: { $in: [userId, '$btwfCount'] },
              then: true,
              else: false,
            },
          },
        },
      },
      {
        $facet: {
          paginatedResults: [
            { $skip: pageSize * (pageNo - 1) },
            { $limit: pageSize },
          ],
          totalCount: [
            {
              $count: 'count',
            },
          ],
        },
      },
    ]);
  }

  filterGroup(data) {
    return this.model.aggregate([
      {
        $lookup: {
          from: 'group',
          let: { group: '$group' },
          pipeline: [
            {
              $match: {
                $expr: { $in: ['$_id', { $ifNull: ['$$group', []] }] },
                name: 'dgfds',
              },
            },
          ],
          as: 'groupData',
        },
      },
      {
        $match: {
          groupData: { $ne: [] },
        },
      },
      {
        $group: {
          _id: 'null',
          count: { $sum: 1 },
        },
      },
    ]);
  }

  getProfileData(data) {
    let userId = data.userId;
    return this.model.aggregate([
      {
        $match: {
          status: 'Live',
        },
      },
      {
        $addFields: {
          date: '$dob',
          age: {
            $divide: [
              { $subtract: [new Date(), '$dob'] },
              365 * 24 * 60 * 60 * 1000,
            ],
          },
        },
      },
      {
        $addFields: {
          agetocalculate: { $ceil: '$age' },
          ageToCalculate: { $floor: '$age' },
        },
      },
      {
        $addFields: {
          isBtwf: {
            $cond: {
              if: { $in: [userId, '$btwfCount'] },
              then: true,
              else: false,
            },
          },
        },
      },
      {
        $lookup: {
          from: 'profession',
          localField: 'profession',
          foreignField: '_id',
          as: 'profession',
        },
      },
      { $sort: { btwfScore: -1 } },
    ]);
  }

  getOneProfileData(data) {
    let userId = data.userId;
    return this.model.aggregate([
      { $match: { _id: mongoose.Types.ObjectId(data.id) } },
      {
        $addFields: {
          isBtwf: {
            $cond: {
              if: { $in: [userId, '$btwfCount'] },
              then: true,
              else: false,
            },
          },
        },
      },
      {
        $lookup: {
          from: 'profession',
          localField: 'profession',
          foreignField: '_id',
          as: 'profession',
        },
      },
      {
        $lookup: {
          from: 'industry',
          localField: 'industry',
          foreignField: '_id',
          as: 'industry',
        },
      },
      {
        $lookup: {
          from: 'group',
          localField: 'group',
          foreignField: '_id',
          as: 'group',
        },
      },
      {
        $lookup: {
          from: 'media',
          localField: 'media',
          foreignField: '_id',
          as: 'media',
        },
      },
      { $unwind: '$media' },
      {
        $lookup: {
          from: 'socialLink',
          localField: 'social',
          foreignField: '_id',
          as: 'social',
        },
      },
      { $unwind: '$social' },
    ]);
  }

  profileAccToProfession(data) {
    let pageNo = parseInt(data.page) || 1;
    let pageSize = parseInt(data.limit) || 10;
    let ObjectId = mongoose.Types.ObjectId;
    let userId = data.userId;
    if (pageNo <= 0) {
      return;
    }
    let btwfScore;
    if (data.popularity == 'mostPopular') {
      btwfScore = -1;
    }
    if (data.popularity == 'leastPopular') {
      btwfScore = 1;
    }
    let professionName = data.professionName
      ? data.professionName
      : { $exists: true };
    let gender = data.gender ? data.gender : { $exists: true };
    let currentCountry = data.currentCountry
      ? data.currentCountry
      : { $exists: true };
    let currentCity = data.currentCity ? data.currentCity : { $exists: true };
    let industryName = data.industryName
      ? data.industryName
      : { $exists: true };
    let minAge = data.minAge ? data.minAge : 0;
    let maxAge = data.maxAge ? data.maxAge : 1000;
    let queryLang = data.catagory ? data.catagory : { $exists: true };
    return this.model.aggregate([
      {
        $match: {
          profession: { $in: [ObjectId(data._id)] },
          primaryLang: queryLang,
          status: 'Live',
          gender: gender,
          currentCity: currentCity,
          currentCountry: currentCountry,
        },
      },
      {
        $lookup: {
          from: 'profession',
          localField: 'profession',
          foreignField: '_id',
          as: 'profession',
        },
      },
      {
        $lookup: {
          from: 'industry',
          localField: 'industry',
          foreignField: '_id',
          as: 'industry',
        },
      },
      {
        $lookup: {
          from: 'group',
          localField: 'group',
          foreignField: '_id',
          as: 'group',
        },
      },
      {
        $addFields: {
          date: '$dob',
          age: {
            $divide: [
              { $subtract: [new Date(), '$dob'] },
              365 * 24 * 60 * 60 * 1000,
            ],
          },
        },
      },
      {
        $addFields: {
          agetocalculate: { $ceil: '$age' },
          ageToCalculate: { $floor: '$age' },
        },
      },
      {
        $match: {
          profession: { $ne: [] },
          industry: { $ne: [] },
          age: {
            $gte: parseInt(minAge),
            $lte: parseInt(maxAge),
          },
        },
      },
      { $sort: { btwfScore: btwfScore } },
      {
        $addFields: {
          isBtwf: {
            $cond: {
              if: { $in: [userId, '$btwfCount'] },
              then: true,
              else: false,
            },
          },
        },
      },
      {
        $facet: {
          paginatedResults: [
            { $skip: pageSize * (pageNo - 1) },
            { $limit: pageSize },
          ],
          totalCount: [
            {
              $count: 'count',
            },
          ],
        },
      },
    ]);
  }

  netWorth(data) {
    let pageNo = parseInt(data.page) || 1;
    let pageSize = parseInt(data.limit) || 10;
    if (pageNo <= 0) {
      return;
    }
    let btwfScore;
    if (data.netWorth == 'highestOne') {
      btwfScore = -1;
    }
    if (data.netWorth == 'lowestOne') {
      btwfScore = 1;
    }
    return this.model.aggregate([
      {
        $lookup: {
          from: 'profession',
          let: { profession: '$profession' },
          pipeline: [
            {
              $match: {
                $expr: { $in: ['$_id', { $ifNull: ['$$profession', []] }] },
              },
            },
          ],
          as: 'profession',
        },
      },
      {
        $lookup: {
          from: 'industry',
          let: { industry: '$industry' },
          pipeline: [
            {
              $match: {
                $expr: { $in: ['$_id', { $ifNull: ['$$industry', []] }] },
              },
            },
          ],
          as: 'industry',
        },
      },
      {
        $addFields: {
          netWorthUnit: {
            $switch: {
              branches: [
                {
                  case: { $eq: ['$netWorthUnit', 'Thousand'] },
                  then: 1000,
                },
                {
                  case: { $eq: ['$netWorthUnit', 'Million'] },
                  then: 1000000,
                },
                {
                  case: { $eq: ['$netWorthUnit', 'Billion'] },
                  then: 100000000,
                },
              ],
              default: 1,
            },
          },
        },
      },
      {
        $addFields: {
          netWorth: { $multiply: ['$netWorthUnit', '$netWorth'] },
        },
      },
      { $sort: { netWorth: btwfScore } },
      {
        $facet: {
          paginatedResults: [
            { $skip: pageSize * (pageNo - 1) },
            { $limit: pageSize },
          ],
          totalCount: [
            {
              $count: 'count',
            },
          ],
        },
      },
    ]);
  }

  accToAge(data) {
    return this.model.aggregate([
      {
        $match: {
          status: 'Live',
        },
      },
      {
        $addFields: {
          date: '$dob',
          age: {
            $divide: [
              { $subtract: [new Date(), '$dob'] },
              365 * 24 * 60 * 60 * 1000,
            ],
          },
        },
      },
      {
        $addFields: {
          ageToCalculate: { $ceil: '$age' },
          ageToCaslculate: { $floor: '$age' },
        },
      },
      {
        $match: {
          $or: [
            { ageToCalculate: { $eq: data } },
            { ageToCaslculate: { $eq: data } },
          ],
        },
      },
      {
        $project: {
          group: 0,
          industry: 0,
          profession: 0,
          status: 0,
          ageToCalculate: 0,
          age: 0,
          date: 0,
        },
      },
    ]);
  }

  resultAccToAge(data, queryToSend) {
    let pageNo = parseInt(queryToSend.page) || 1;
    let pageSize = parseInt(queryToSend.limit) || 10;
    if (pageNo <= 0) {
      return;
    }
    return this.model.aggregate([
      {
        $match: {
          status: 'Live',
        },
      },
      {
        $addFields: {
          date: '$dob',
          age: {
            $divide: [
              { $subtract: [new Date(), '$dob'] },
              365 * 24 * 60 * 60 * 1000,
            ],
          },
        },
      },
      {
        $addFields: {
          ageToCalculate: { $ceil: '$age' },
          ageToCaslculate: { $floor: '$age' },
        },
      },
      {
        $match: {
          $or: [
            { ageToCalculate: { $eq: data } },
            { ageToCaslculate: { $eq: data } },
          ],
        },
      },
      {
        $lookup: {
          from: 'profession',
          localField: 'profession',
          foreignField: '_id',
          as: 'profession',
        },
      },
      {
        $facet: {
          paginatedResults: [
            { $skip: pageSize * (pageNo - 1) },
            { $limit: pageSize },
          ],
          totalCount: [
            {
              $count: 'count',
            },
          ],
        },
      },
      {
        $project: {
          group: 0,
          industry: 0,
          status: 0,
          ageToCalculate: 0,
          age: 0,
          date: 0,
        },
      },
    ]);
  }

  groupAccToMonth(data) {
    return this.model.aggregate([
      {
        $match: {
          status: 'Live',
        },
      },
      {
        $addFields: {
          month: { $month: '$dob' },
        },
      },
      {
        $addFields: {
          month: {
            $let: {
              vars: {
                monthsInString: [
                  'January',
                  'January',
                  'February',
                  'March',
                  'April',
                  'May',
                  'June',
                  'July',
                  'August',
                  'September',
                  'October',
                  'November',
                  'December',
                ],
              },
              in: {
                $arrayElemAt: ['$$monthsInString', '$month'],
              },
            },
          },
        },
      },
      {
        $group: {
          _id: {
            day: { $dayOfMonth: '$dob' },
            month: '$month',
          },
          dob: { $first: '$dob' },
          id: { $first: '$_id' },
          fullName: { $first: '$fullName' },
          currentCountry: { $first: '$currentCountry' },
          countryOfBirth: { $first: '$countryOfBirth' },
          cityOfBirth: { $first: '$cityOfBirth' },
          description: { $first: '$description' },
          profession: { $first: '$profession' },
          zodiacSign: { $first: '$zodiacSign' },
          btwfScore: { $first: '$btwfScore' },
        },
      },
      { $sort: { '_id.day': 1 } },
      {
        $match: {
          '_id.month': { $regex: data, $options: 'si' },
        },
      },
    ]);
  }

  recordGroupAccToMonth(data, queryToSend) {
    let pageNo = parseInt(queryToSend.page) || 1;
    let pageSize = parseInt(queryToSend.limit) || 10;
    if (pageNo <= 0) {
      return;
    }
    return this.model.aggregate([
      {
        $match: {
          status: 'Live',
        },
      },
      {
        $addFields: {
          month: { $month: '$dob' },
        },
      },
      {
        $addFields: {
          month: {
            $let: {
              vars: {
                monthsInString: [
                  'January',
                  'January',
                  'February',
                  'March',
                  'April',
                  'May',
                  'June',
                  'July',
                  'August',
                  'September',
                  'October',
                  'November',
                  'December',
                ],
              },
              in: {
                $arrayElemAt: ['$$monthsInString', '$month'],
              },
            },
          },
        },
      },
      {
        $group: {
          _id: {
            day: { $dayOfMonth: '$dob' },
            month: '$month',
          },
          dob: { $first: '$dob' },
          id: { $first: '$_id' },
          fullName: { $first: '$fullName' },
          currentCountry: { $first: '$currentCountry' },
          countryOfBirth: { $first: '$countryOfBirth' },
          cityOfBirth: { $first: '$cityOfBirth' },
          description: { $first: '$description' },
          profession: { $first: '$profession' },
          zodiacSign: { $first: '$zodiacSign' },
          btwfScore: { $first: '$btwfScore' },
          profileImg: { $first: '$profileImg' },
          profession: { $first: '$profession' },
          netWorth: { $first: '$netWorth' },
          netWorthUnit: { $first: '$netWorthUnit' },
        },
      },
      {
        $lookup: {
          from: 'profession',
          localField: 'profession',
          foreignField: '_id',
          as: 'profession',
        },
      },
      { $sort: { '_id.day': 1 } },
      {
        $match: {
          '_id.month': { $regex: data, $options: 'si' },
        },
      },
      {
        $facet: {
          paginatedResults: [
            { $skip: pageSize * (pageNo - 1) },
            { $limit: pageSize },
          ],
          totalCount: [
            {
              $count: 'count',
            },
          ],
        },
      },
    ]);
  }

  searchProfile(data) {
    let pageNo = parseInt(data.page) || 1;
    let pageSize = parseInt(data.limit) || 10;
    let searchText = new RegExp(data.search);
    return this.model.aggregate([
      {
        $match: {
          status: 'Live',
        },
      },
      {
        $lookup: {
          from: 'group',
          let: { group: '$group' },
          pipeline: [
            {
              $match: {
                $expr: { $in: ['$_id', { $ifNull: ['$$group', []] }] },
              },
            },
          ],
          as: 'groupDetail',
        },
      },
      {
        $lookup: {
          from: 'industry',
          let: { industry: '$industry' },
          pipeline: [
            {
              $match: {
                $expr: { $in: ['$_id', { $ifNull: ['$$industry', []] }] },
              },
            },
          ],
          as: 'industryDetail',
        },
      },
      {
        $lookup: {
          from: 'profession',
          let: { profession: '$profession' },
          pipeline: [
            {
              $match: {
                $expr: { $in: ['$_id', { $ifNull: ['$$profession', []] }] },
              },
            },
          ],
          as: 'professionDetail',
        },
      },
      {
        $addFields: {
          month: { $month: '$dob' },
          year: { $year: '$dob' },
          day: { $dayOfMonth: '$dob' },
          age: {
            $divide: [
              { $subtract: [new Date(), '$dob'] },
              365 * 24 * 60 * 60 * 1000,
            ],
          },
        },
      },
      {
        $addFields: {
          month: {
            $let: {
              vars: {
                monthsInString: [
                  'Jan',
                  'Feb',
                  'Mar',
                  'Apr',
                  'May',
                  'Jun',
                  'Jul',
                  'Aug',
                  'Sept',
                  'Oct',
                  'Nov',
                  'Dec',
                ],
              },
              in: {
                $arrayElemAt: ['$$monthsInString', '$month'],
              },
            },
          },
          ageToCalculate: { $ceil: '$age' },
        },
      },
      {
        $match: {
          $or: [
            { fullName: { $regex: searchText, $options: 'si' } },
            { 'groupDetail.name': { $regex: searchText, $options: 'si' } },
            { 'industryDetail.name': { $regex: searchText, $options: 'si' } },
            {
              'professionDetail.professionType': {
                $regex: searchText,
                $options: 'si',
              },
            },
          ],
        },
      },
      {
        $project: {
          group: 0,
          industry: 0,
          profession: 0,
          status: 0,
          'professionDetail.peopleRelated': 0,
          age: 0,
        },
      },
      {
        $facet: {
          paginatedResults: [
            { $skip: pageSize * (pageNo - 1) },
            { $limit: pageSize },
          ],
          totalCount: [
            {
              $count: 'count',
            },
          ],
        },
      },
    ]);
  }

  netWorthFilter(data) {
    let pageNo = parseInt(data.page) || 1;
    let pageSize = parseInt(data.limit) || 10;
    let userId = data.userId;
    if (pageNo <= 0) {
      return;
    }
    let btwfScore;
    if (data.popularity == 'highestOne') {
      btwfScore = -1;
    }
    if (data.popularity == 'lowestOne') {
      btwfScore = 1;
    }
    let professionName = data.professionName
      ? data.professionName
      : { $exists: true };
    let gender = data.gender ? data.gender : { $exists: true };
    let currentCountry = data.currentCountry
      ? data.currentCountry
      : { $exists: true };
    let currentCity = data.currentCity ? data.currentCity : { $exists: true };
    let industryName = data.industryName
      ? data.industryName
      : { $exists: true };
    let minAge = data.minAge ? data.minAge : 0;
    let maxAge = data.maxAge ? data.maxAge : 1000;
    return this.model.aggregate([
      {
        $match: {
          status: 'Live',
          gender: gender,
          currentCity: currentCity,
          currentCountry: currentCountry,
        },
      },
      {
        $lookup: {
          from: 'profession',
          let: { profession: '$profession' },
          pipeline: [
            {
              $match: {
                $expr: { $in: ['$_id', { $ifNull: ['$$profession', []] }] },
                professionType: professionName,
              },
            },
          ],
          as: 'profession',
        },
      },
      {
        $lookup: {
          from: 'industry',
          let: { industry: '$industry' },
          pipeline: [
            {
              $match: {
                $expr: { $in: ['$_id', { $ifNull: ['$$industry', []] }] },
                name: industryName,
              },
            },
          ],
          as: 'industry',
        },
      },
      {
        $addFields: {
          date: '$dob',
          age: {
            $divide: [
              { $subtract: [new Date(), '$dob'] },
              365 * 24 * 60 * 60 * 1000,
            ],
          },
        },
      },
      {
        $addFields: {
          isBtwf: {
            $cond: {
              if: { $in: [userId, '$btwfCount'] },
              then: true,
              else: false,
            },
          },
        },
      },
      {
        $addFields: {
          agetocalculate: { $ceil: '$age' },
          ageToCalculate: { $floor: '$age' },
        },
      },
      {
        $addFields: {
          netWorthUnitq: {
            $switch: {
              branches: [
                {
                  case: { $eq: ['$netWorthUnit', 'Thousand'] },
                  then: 1000,
                },
                {
                  case: { $eq: ['$netWorthUnit', 'Million'] },
                  then: 1000000,
                },
                {
                  case: { $eq: ['$netWorthUnit', 'Billion'] },
                  then: 100000000,
                },
              ],
              default: 1,
            },
          },
        },
      },
      {
        $addFields: {
          netWorthq: { $multiply: ['$netWorthUnitq', '$netWorth'] },
        },
      },
      { $sort: { netWorthq: btwfScore } },
      {
        $project: {
          netWorthUnitq: 0,
          netWorthq: 0,
        },
      },
      {
        $match: {
          profession: { $ne: [] },
          industry: { $ne: [] },
          age: {
            $gte: parseInt(minAge),
            $lte: parseInt(maxAge),
          },
        },
      },
      {
        $facet: {
          paginatedResults: [
            { $skip: pageSize * (pageNo - 1) },
            { $limit: pageSize },
          ],
          totalCount: [
            {
              $count: 'count',
            },
          ],
        },
      },
    ]);
  }

  accToCurrentCity(data) {
    let det = '^' + data;
    return this.model.aggregate([
      {
        $match: {
          status: 'Live',
          currCityForfiter: { $regex: det, $options: 'si' },
        },
      },
    ]);
  }

  resultAccToCurrentCity(data, queryToSend) {
    let det = '^' + data;
    let pageNo = parseInt(queryToSend.page) || 1;
    let pageSize = parseInt(queryToSend.limit) || 10;
    if (pageNo <= 0) {
      return;
    }
    return this.model.aggregate([
      {
        $match: {
          status: 'Live',
          currCityForfiter: { $regex: det, $options: 'si' },
        },
      },
      {
        $lookup: {
          from: 'profession',
          localField: 'profession',
          foreignField: '_id',
          as: 'profession',
        },
      },
      {
        $facet: {
          paginatedResults: [
            { $skip: pageSize * (pageNo - 1) },
            { $limit: pageSize },
          ],
          totalCount: [
            {
              $count: 'count',
            },
          ],
        },
      },
    ]);
  }

  accToBirthCity(data) {
    let det = '^' + data;
    return this.model.aggregate([
      {
        $match: {
          status: 'Live',
          cityofBirthForfiter: { $regex: det, $options: 'si' },
        },
      },
    ]);
  }

  resultAccToBirthCity(data, queryToSend) {
    let det = '^' + data;
    let pageNo = parseInt(queryToSend.page) || 1;
    let pageSize = parseInt(queryToSend.limit) || 10;
    if (pageNo <= 0) {
      return;
    }
    return this.model.aggregate([
      {
        $match: {
          status: 'Live',
          cityofBirthForfiter: { $regex: det, $options: 'si' },
        },
      },
      {
        $lookup: {
          from: 'profession',
          localField: 'profession',
          foreignField: '_id',
          as: 'profession',
        },
      },
      {
        $facet: {
          paginatedResults: [
            { $skip: pageSize * (pageNo - 1) },
            { $limit: pageSize },
          ],
          totalCount: [
            {
              $count: 'count',
            },
          ],
        },
      },
    ]);
  }

  countryOfBirth(data) {
    return this.model.aggregate([
      {
        $match: {
          status: 'Live',
          cobForfiter: { $regex: data, $options: 'si' },
        },
      },
    ]);
  }

  accCurrentCountry(data) {
    let dataToSendFurther = '^' + data;
    return this.model.aggregate([
      {
        $match: {
          status: 'Live',
          currCForfiter: { $regex: dataToSendFurther, $options: 'si' },
        },
      },
    ]);
  }

  resultAccCurrentCountry(data, queryToSend) {
    let dataToSendFurther = '^' + data;
    let pageNo = parseInt(queryToSend.page) || 1;
    let pageSize = parseInt(queryToSend.limit) || 10;
    if (pageNo <= 0) {
      return;
    }
    return this.model.aggregate([
      {
        $match: {
          status: 'Live',
          currCForfiter: { $regex: dataToSendFurther, $options: 'si' },
        },
      },
      {
        $lookup: {
          from: 'profession',
          localField: 'profession',
          foreignField: '_id',
          as: 'profession',
        },
      },
      {
        $facet: {
          paginatedResults: [
            { $skip: pageSize * (pageNo - 1) },
            { $limit: pageSize },
          ],
          totalCount: [
            {
              $count: 'count',
            },
          ],
        },
      },
    ]);
  }

  accToLang(data) {
    let det = '^' + data;
    let pageNo = parseInt(data.page) || 1;
    let pageSize = parseInt(data.limit) || 10;
    if (pageNo <= 0) {
      return;
    }
    return this.model.aggregate([
      {
        $match: {
          status: 'Live',
          primaryLang: { $regex: det, $options: 'si' },
        },
      },
    ]);
  }

  countryOfBirth(data) {
    let dataToSendFurther = '^' + data;
    return this.model.aggregate([
      {
        $match: {
          status: 'Live',
          cobForfiter: { $regex: dataToSendFurther, $options: 'si' },
        },
      },
    ]);
  }

  resultByCountryOfBirth(data, queryToSend) {
    let dataToSendFurther = '^' + data;
    let pageNo = parseInt(queryToSend.page) || 1;
    let pageSize = parseInt(queryToSend.limit) || 10;
    if (pageNo <= 0) {
      return;
    }
    return this.model.aggregate([
      {
        $match: {
          status: 'Live',
          cobForfiter: { $regex: dataToSendFurther, $options: 'si' },
        },
      },
      {
        $addFields: {
          month: { $month: '$dob' },
        },
      },
      {
        $addFields: {
          month: {
            $let: {
              vars: {
                monthsInString: [
                  'January',
                  'January',
                  'February',
                  'March',
                  'April',
                  'May',
                  'June',
                  'July',
                  'August',
                  'September',
                  'October',
                  'November',
                  'December',
                ],
              },
              in: {
                $arrayElemAt: ['$$monthsInString', '$month'],
              },
            },
          },
        },
      },
      {
        $addFields: {
          age: {
            $divide: [
              { $subtract: [new Date(), '$dob'] },
              365 * 24 * 60 * 60 * 1000,
            ],
          },
        },
      },
      {
        $addFields: {
          ageToCalculate: { $ceil: '$age' },
          ageToCaslculate: { $floor: '$age' },
        },
      },
      {
        $addFields: {
          ageToCalculate: { $toString: '$ageToCalculate' },
          ageToCaslculate: { $toString: '$ageToCaslculate' },
        },
      },
      {
        $addFields: {
          birthday: { $concat: ['$ageToCalculate', ' ', '$month'] },
          birthday2: { $concat: ['$ageToCaslculate', ' ', '$month'] },
        },
      },
      {
        $lookup: {
          from: 'profession',
          localField: 'profession',
          foreignField: '_id',
          as: 'profession',
        },
      },
      {
        $facet: {
          paginatedResults: [
            { $skip: pageSize * (pageNo - 1) },
            { $limit: pageSize },
          ],
          totalCount: [
            {
              $count: 'count',
            },
          ],
        },
      },
    ]);
  }

  zodiacSign(data) {
    let det = '^' + data;
    return this.model.aggregate([
      {
        $match: {
          status: 'Live',
          zodiacSign: { $regex: det, $options: 'si' },
        },
      },
    ]);
  }

  resultZodiacSign(data, queryToSend) {
    let dataToSendFurther = '^' + data;
    let pageNo = parseInt(queryToSend.page) || 1;
    let pageSize = parseInt(queryToSend.limit) || 10;
    if (pageNo <= 0) {
      return;
    }
    return this.model.aggregate([
      {
        $match: {
          status: 'Live',
          zodiacSign: { $regex: dataToSendFurther, $options: 'si' },
        },
      },
      {
        $lookup: {
          from: 'profession',
          localField: 'profession',
          foreignField: '_id',
          as: 'profession',
        },
      },
      {
        $facet: {
          paginatedResults: [
            { $skip: pageSize * (pageNo - 1) },
            { $limit: pageSize },
          ],
          totalCount: [
            {
              $count: 'count',
            },
          ],
        },
      },
    ]);
  }

  accToLang(data) {
    let det = '^' + data;
    return this.model.aggregate([
      {
        $match: {
          status: 'Live',
          primaryLang: { $regex: det, $options: 'si' },
        },
      },
    ]);
  }

  resultAccToLang(data, queryToSend) {
    let det = '^' + data;
    let pageNo = parseInt(queryToSend.page) || 1;
    let pageSize = parseInt(queryToSend.limit) || 10;
    if (pageNo <= 0) {
      return;
    }
    return this.model.aggregate([
      {
        $lookup: {
          from: 'profession',
          let: { profession: '$profession' },
          pipeline: [
            {
              $match: {
                $expr: { $in: ['$_id', { $ifNull: ['$$profession', []] }] },
              },
            },
          ],
          as: 'profession',
        },
      },
      {
        $match: {
          status: 'Live',
          primaryLang: { $regex: det, $options: 'si' },
        },
      },
      {
        $lookup: {
          from: 'profession',
          localField: 'profession',
          foreignField: '_id',
          as: 'profession',
        },
      },
      {
        $facet: {
          paginatedResults: [
            { $skip: pageSize * (pageNo - 1) },
            { $limit: pageSize },
          ],
          totalCount: [
            {
              $count: 'count',
            },
          ],
        },
      },
    ]);
  }

  birthDay(data) {
    let dataToSendFurther = '^' + data;
    return this.model.aggregate([
      {
        $match: {
          status: 'Live',
        },
      },
      {
        $addFields: {
          month: { $month: '$dob' },
        },
      },
      {
        $addFields: {
          day: { $dayOfMonth: '$dob' },
        },
      },
      {
        $addFields: {
          month: {
            $let: {
              vars: {
                monthsInString: [
                  'January',
                  'January',
                  'February',
                  'March',
                  'April',
                  'May',
                  'June',
                  'July',
                  'August',
                  'September',
                  'October',
                  'November',
                  'December',
                ],
              },
              in: {
                $arrayElemAt: ['$$monthsInString', '$month'],
              },
            },
          },
        },
      },
      {
        $addFields: {
          age: {
            $divide: [
              { $subtract: [new Date(), '$dob'] },
              365 * 24 * 60 * 60 * 1000,
            ],
          },
        },
      },
      {
        $addFields: {
          ageToCalculate: { $ceil: '$age' },
          ageToCaslculate: { $floor: '$age' },
        },
      },
      {
        $addFields: {
          birthDate: { $toString: '$day' },
          ageToCaslculate: { $toString: '$ageToCaslculate' },
        },
      },
      {
        $addFields: {
          birthday: { $concat: ['$birthDate', ' ', '$month'] },
        },
      },
      {
        $match: {
          birthday: { $regex: dataToSendFurther, $options: 'si' },
        },
      },
    ]);
  }

  resultBirthDay(data, queryToSend) {
    let dataToSendFurther = '^' + data;
    let pageNo = parseInt(queryToSend.page) || 1;
    let pageSize = parseInt(queryToSend.limit) || 10;
    if (pageNo <= 0) {
      return;
    }
    return this.model.aggregate([
      {
        $match: {
          status: 'Live',
        },
      },
      {
        $addFields: {
          month: { $month: '$dob' },
        },
      },
      {
        $addFields: {
          day: { $dayOfMonth: '$dob' },
        },
      },
      {
        $addFields: {
          month: {
            $let: {
              vars: {
                monthsInString: [
                  'January',
                  'January',
                  'February',
                  'March',
                  'April',
                  'May',
                  'June',
                  'July',
                  'August',
                  'September',
                  'October',
                  'November',
                  'December',
                ],
              },
              in: {
                $arrayElemAt: ['$$monthsInString', '$month'],
              },
            },
          },
        },
      },
      {
        $addFields: {
          age: {
            $divide: [
              { $subtract: [new Date(), '$dob'] },
              365 * 24 * 60 * 60 * 1000,
            ],
          },
        },
      },
      {
        $addFields: {
          ageToCalculate: { $ceil: '$age' },
          ageToCaslculate: { $floor: '$age' },
        },
      },
      {
        $addFields: {
          birthDate: { $toString: '$day' },
          ageToCaslculate: { $toString: '$ageToCaslculate' },
        },
      },
      {
        $addFields: {
          birthday: { $concat: ['$birthDate', ' ', '$month'] },
        },
      },
      {
        $match: {
          birthday: { $regex: dataToSendFurther, $options: 'si' },
        },
      },
      {
        $lookup: {
          from: 'profession',
          localField: 'profession',
          foreignField: '_id',
          as: 'profession',
        },
      },
      {
        $facet: {
          paginatedResults: [
            { $skip: pageSize * (pageNo - 1) },
            { $limit: pageSize },
          ],
          totalCount: [
            {
              $count: 'count',
            },
          ],
        },
      },
    ]);
  }

  recordGroupAccToYear(data, queryToSend) {
    let pageNo = parseInt(queryToSend.page) || 1;
    let pageSize = parseInt(queryToSend.limit) || 10;
    if (pageNo <= 0) {
      return;
    }
    return this.model.aggregate([
      {
        $match: {
          status: 'Live',
        },
      },
      {
        $addFields: {
          year: { $year: '$dob' },
        },
      },
      {
        $group: {
          _id: {
            year: '$year',
          },
          dob: { $first: '$dob' },
          id: { $first: '$_id' },
          fullName: { $first: '$fullName' },
          currentCountry: { $first: '$currentCountry' },
          countryOfBirth: { $first: '$countryOfBirth' },
          cityOfBirth: { $first: '$cityOfBirth' },
          description: { $first: '$description' },
          profession: { $first: '$profession' },
          zodiacSign: { $first: '$zodiacSign' },
          btwfScore: { $first: '$btwfScore' },
          profileImg: { $first: '$profileImg' },
          profession: { $first: '$profession' },
          netWorth: { $first: '$netWorth' },
          netWorthUnit: { $first: '$netWorthUnit' },
        },
      },
      { $sort: { '_id.year': 1 } },
      {
        $addFields: {
          convertedZipCode: { $toString: '$_id.year' },
        },
      },
      {
        $match: {
          convertedZipCode: { $regex: data, $options: 'si' },
        },
      },
      {
        $lookup: {
          from: 'profession',
          localField: 'profession',
          foreignField: '_id',
          as: 'profession',
        },
      },
      {
        $facet: {
          paginatedResults: [
            { $skip: pageSize * (pageNo - 1) },
            { $limit: pageSize },
          ],
          totalCount: [
            {
              $count: 'count',
            },
          ],
        },
      },
    ]);
  }

  groupAccToYear(data) {
    return this.model.aggregate([
      {
        $match: {
          status: 'Live',
        },
      },
      {
        $addFields: {
          year: { $year: '$dob' },
        },
      },
      {
        $group: {
          _id: {
            year: '$year',
          },
          dob: { $first: '$dob' },
          id: { $first: '$_id' },
          fullName: { $first: '$fullName' },
          currentCountry: { $first: '$currentCountry' },
          countryOfBirth: { $first: '$countryOfBirth' },
          cityOfBirth: { $first: '$cityOfBirth' },
          description: { $first: '$description' },
          profession: { $first: '$profession' },
          zodiacSign: { $first: '$zodiacSign' },
          btwfScore: { $first: '$btwfScore' },
        },
      },
      { $sort: { '_id.year': 1 } },
      {
        $addFields: {
          convertedZipCode: { $toString: '$_id.year' },
        },
      },
      {
        $match: {
          convertedZipCode: { $regex: data, $options: 'si' },
        },
      },
    ]);
  }

  resultByName(data, queryToSend) {
    let det = '^' + data;
    let pageNo = parseInt(queryToSend.page) || 1;
    let pageSize = parseInt(queryToSend.limit) || 10;
    if (pageNo <= 0) {
      return;
    }
    return this.model.aggregate([
      {
        $match: {
          status: 'Live',
          fullName: { $regex: det, $options: 'si' },
        },
      },
      {
        $lookup: {
          from: 'profession',
          localField: 'profession',
          foreignField: '_id',
          as: 'profession',
        },
      },
      {
        $facet: {
          paginatedResults: [
            { $skip: pageSize * (pageNo - 1) },
            { $limit: pageSize },
          ],
          totalCount: [
            {
              $count: 'count',
            },
          ],
        },
      },
    ]);
  }

  byName(data) {
    let det = '^' + data;
    return this.model.aggregate([
      {
        $match: {
          status: 'Live',
          fullName: { $regex: det, $options: 'si' },
        },
      },
    ]);
  }

  byGroupName(data) {
    let det = '^' + data;
    return this.model.aggregate([
      {
        $lookup: {
          from: 'group',
          let: { group: '$group' },
          pipeline: [
            {
              $match: {
                $expr: { $in: ['$_id', { $ifNull: ['$$group', []] }] },
              },
            },
          ],
          as: 'group',
        },
      },
      {
        $match: {
          status: 'Live',
          'group.name': { $regex: det, $options: 'si' },
        },
      },
    ]);
  }

  byIndustryName(data) {
    let det = '^' + data;
    return this.model.aggregate([
      {
        $lookup: {
          from: 'industry',
          let: { industry: '$industry' },
          pipeline: [
            {
              $match: {
                $expr: { $in: ['$_id', { $ifNull: ['$$industry', []] }] },
              },
            },
          ],
          as: 'industry',
        },
      },
      {
        $match: {
          status: 'Live',
          'industry.name': { $regex: det, $options: 'si' },
        },
      },
    ]);
  }

  byType(data) {
    let det = '^' + data;
    return this.model.aggregate([
      {
        $lookup: {
          from: 'profession',
          let: { profession: '$profession' },
          pipeline: [
            {
              $match: {
                $expr: { $in: ['$_id', { $ifNull: ['$$profession', []] }] },
              },
            },
          ],
          as: 'profession',
        },
      },
      {
        $match: {
          status: 'Live',
          'profession.professionType': { $regex: det, $options: 'si' },
        },
      },
    ]);
  }

  resultByGroupName(data, queryToSend) {
    let det = '^' + data;
    let pageNo = parseInt(queryToSend.page) || 1;
    let pageSize = parseInt(queryToSend.limit) || 10;
    if (pageNo <= 0) {
      return;
    }
    return this.model.aggregate([
      {
        $lookup: {
          from: 'group',
          let: { group: '$group' },
          pipeline: [
            {
              $match: {
                $expr: { $in: ['$_id', { $ifNull: ['$$group', []] }] },
              },
            },
          ],
          as: 'group',
        },
      },
      {
        $match: {
          status: 'Live',
          'group.name': { $regex: det, $options: 'si' },
        },
      },
      {
        $facet: {
          paginatedResults: [
            { $skip: pageSize * (pageNo - 1) },
            { $limit: pageSize },
          ],
          totalCount: [
            {
              $count: 'count',
            },
          ],
        },
      },
    ]);
  }

  resultByIndustryName(data, queryToSend) {
    let det = '^' + data;
    let pageNo = parseInt(queryToSend.page) || 1;
    let pageSize = parseInt(queryToSend.limit) || 10;
    if (pageNo <= 0) {
      return;
    }
    return this.model.aggregate([
      {
        $lookup: {
          from: 'industry',
          let: { industry: '$industry' },
          pipeline: [
            {
              $match: {
                $expr: { $in: ['$_id', { $ifNull: ['$$industry', []] }] },
              },
            },
          ],
          as: 'industry',
        },
      },
      {
        $match: {
          status: 'Live',
          'industry.name': { $regex: det, $options: 'si' },
        },
      },
      {
        $facet: {
          paginatedResults: [
            { $skip: pageSize * (pageNo - 1) },
            { $limit: pageSize },
          ],
          totalCount: [
            {
              $count: 'count',
            },
          ],
        },
      },
    ]);
  }

  birthYearNumber() {
    return this.model.aggregate([
      {
        $addFields: {
          year: { $year: '$dob' },
        },
      },
      {
        $group: {
          _id: { year: '$year' },
          count: { $sum: 1 },
        },
      },
      {
        $project: {
          _id: 0,
          year: '$_id.year',
          count: 1,
        },
      },
    ]);
  }

  ageCount() {
    return this.model.aggregate([
      {
        $addFields: {
          age: {
            $divide: [
              { $subtract: [new Date(), '$dob'] },
              365 * 24 * 60 * 60 * 1000,
            ],
          },
        },
      },
      {
        $addFields: {
          age: { $floor: '$age' },
        },
      },
      {
        $group: {
          _id: { age: '$age' },
          count: { $sum: 1 },
        },
      },
      {
        $project: {
          _id: 0,
          age: {
            $cond: {
              if: { $lte: ['$_id.age', 0] },
              then: 'Infant',
              else: { $concat: [{ $toString: '$_id.age' }, ' ', 'Years Old'] },
            },
          },
          count: 1,
        },
      },
    ]);
  }

  birthDateNumber(data) {
    return this.model.aggregate([
      {
        $addFields: {
          year: { $year: '$dob' },
          month: { $month: '$dob' },
          day: { $dayOfMonth: '$dob' },
        },
      },
      {
        $group: {
          _id: { year: '$year', month: '$month', day: '$day' },
          count: { $sum: 1 },
        },
      },
      {
        $addFields: {
          month: {
            $let: {
              vars: {
                monthsInString: [
                  'January',
                  'January',
                  'February',
                  'March',
                  'April',
                  'May',
                  'June',
                  'July',
                  'August',
                  'September',
                  'October',
                  'November',
                  'December',
                ],
              },
              in: {
                $arrayElemAt: ['$$monthsInString', '$_id.month'],
              },
            },
          },
        },
      },
      {
        $project: {
          _id: 0,
          birthDate: {
            $concat: [
              { $toString: '$_id.day' },
              ' ',
              { $toString: '$month' },
              ' ',
              { $toString: '$_id.year' },
            ],
          },
          count: 1,
        },
      },
      {
        $match: {
          birthDate: { $regex: data, $options: 'si' },
        },
      },
    ]);
  }

  getProfileAccToSearch(valueToSearch) {
    let keyToSearch = '$' + valueToSearch;
    return this.model.aggregate([
      {
        $group: {
          _id: keyToSearch,
          count: { $sum: 1 },
        },
      },
      {
        $project: {
          _id: 0,
          name: '$_id',
          count: 1,
        },
      },
    ]);
  }

  resultByType(data, queryToSend) {
    let det = '^' + data;
    let pageNo = parseInt(queryToSend.page) || 1;
    let pageSize = parseInt(queryToSend.limit) || 10;
    if (pageNo <= 0) {
      return;
    }
    return this.model.aggregate([
      {
        $lookup: {
          from: 'profession',
          let: { profession: '$profession' },
          pipeline: [
            {
              $match: {
                $expr: { $in: ['$_id', { $ifNull: ['$$profession', []] }] },
              },
            },
          ],
          as: 'profession',
        },
      },
      {
        $match: {
          status: 'Live',
          'profession.professionType': { $regex: det, $options: 'si' },
        },
      },
      {
        $addFields: {
          year: { $year: '$dob' },
        },
      },
      {
        $group: {
          _id: {
            year: '$year',
          },
          dob: { $first: '$dob' },
          id: { $first: '$_id' },
          fullName: { $first: '$fullName' },
          currentCountry: { $first: '$currentCountry' },
          countryOfBirth: { $first: '$countryOfBirth' },
          cityOfBirth: { $first: '$cityOfBirth' },
          description: { $first: '$description' },
          profession: { $first: '$profession' },
          zodiacSign: { $first: '$zodiacSign' },
          btwfScore: { $first: '$btwfScore' },
        },
      },
      { $sort: { '_id.year': 1 } },
      {
        $addFields: {
          convertedZipCode: { $toString: '$_id.year' },
        },
      },
      {
        $match: {
          convertedZipCode: { $regex: data, $options: 'si' },
        },
      },
    ]);
  }

  byName(data) {
    let det = '^' + data;
    let pageNo = parseInt(data.page) || 1;
    let pageSize = parseInt(data.limit) || 10;
    if (pageNo <= 0) {
      return;
    }
    return this.model.aggregate([
      {
        $match: {
          status: 'Live',
          fullName: { $regex: det, $options: 'si' },
        },
      },
    ]);
  }

  userRequest(criteria) {
    let pageNo = parseInt(criteria.page) || 1;
    let pageSize = parseInt(criteria.limit) || 10;
    if (pageNo <= 0) {
      return;
    }
    return this.model.aggregate([
      {
        $match: {
          $or: [
            { createdBy: mongoose.Types.ObjectId(criteria.user_id) },
            { updatedBy: mongoose.Types.ObjectId(criteria.user_id) },
          ],
        },
      },
      {
        $addFields: {
          requestType: {
            $switch: {
              branches: [
                {
                  case: {
                    $eq: [
                      '$createdBy',
                      mongoose.Types.ObjectId(criteria.user_id),
                    ],
                  },
                  then: 'Created',
                },
                {
                  case: {
                    $eq: [
                      '$updatedBy',
                      mongoose.Types.ObjectId(criteria.user_id),
                    ],
                  },
                  then: 'Updated',
                },
              ],
              default: 'Created',
            },
          },
        },
      },
      {
        $lookup: {
          from: 'profession',
          localField: 'profession',
          foreignField: '_id',
          as: 'profession',
        },
      },
      {
        $lookup: {
          from: 'payments',
          localField: '_id',
          foreignField: 'profile_id',
          as: 'payment',
        },
      },
      {
        $addFields: {
          payment: '$payment.amount',
        },
      },
      {
        $facet: {
          paginatedResults: [
            { $skip: pageSize * (pageNo - 1) },
            { $limit: pageSize },
          ],
          totalCount: [
            {
              $count: 'count',
            },
          ],
        },
      },
    ]);
  }

  /* Birth Day on same date */
  similarBirthDay(data, id) {
    return this.model.aggregate([
      {
        $match: {
          status: 'Live',
          _id: { $ne: mongoose.Types.ObjectId(id) },
        },
      },
      {
        $addFields: {
          month1: { $month: data },
        },
      },
      {
        $addFields: {
          day1: { $dayOfMonth: data },
        },
      },
      {
        $addFields: {
          month1: {
            $let: {
              vars: {
                monthsInString1: [
                  'January',
                  'January',
                  'February',
                  'March',
                  'April',
                  'May',
                  'June',
                  'July',
                  'August',
                  'September',
                  'October',
                  'November',
                  'December',
                ],
              },
              in: {
                $arrayElemAt: ['$$monthsInString1', '$month1'],
              },
            },
          },
        },
      },
      {
        $addFields: {
          birthDate1: { $toString: '$day1' },
        },
      },
      {
        $addFields: {
          birthday1: { $concat: ['$birthDate1', ' ', '$month1'] },
        },
      },
      {
        $addFields: {
          month: { $month: '$dob' },
        },
      },
      {
        $addFields: {
          day: { $dayOfMonth: '$dob' },
        },
      },
      {
        $addFields: {
          month: {
            $let: {
              vars: {
                monthsInString: [
                  'January',
                  'January',
                  'February',
                  'March',
                  'April',
                  'May',
                  'June',
                  'July',
                  'August',
                  'September',
                  'October',
                  'November',
                  'December',
                ],
              },
              in: {
                $arrayElemAt: ['$$monthsInString', '$month'],
              },
            },
          },
        },
      },
      {
        $addFields: {
          birthDate: { $toString: '$day' },
        },
      },
      {
        $addFields: {
          birthday: { $concat: ['$birthDate', ' ', '$month'] },
        },
      },
      { $match: { $expr: { $eq: ['$birthday1', '$birthday'] } } },
      {
        $lookup: {
          from: 'profession',
          localField: 'profession',
          foreignField: '_id',
          as: 'profession',
        },
      },
     
      { $limit: 5 },
    ]);
  }

  provinceOfBirth(data) {
    let dataToSendFurther = '^' + data;
    return this.model.aggregate([
      {
        $match: {
          status: 'Live',
          pobForfiter: { $regex: dataToSendFurther, $options: 'si' },
        },
      },
    ]);
  }

  resultByProvinceOfBirth(data, queryToSend) {
    let dataToSendFurther = '^' + data;
    let pageNo = parseInt(queryToSend.page) || 1;
    let pageSize = parseInt(queryToSend.limit) || 10;
    if (pageNo <= 0) {
      return;
    }
    return this.model.aggregate([
      {
        $match: {
          status: 'Live',
          pobForfiter: { $regex: dataToSendFurther, $options: 'si' },
        },
      },
      {
        $addFields: {
          month: { $month: '$dob' },
        },
      },
      {
        $addFields: {
          month: {
            $let: {
              vars: {
                monthsInString: [
                  'January',
                  'January',
                  'February',
                  'March',
                  'April',
                  'May',
                  'June',
                  'July',
                  'August',
                  'September',
                  'October',
                  'November',
                  'December',
                ],
              },
              in: {
                $arrayElemAt: ['$$monthsInString', '$month'],
              },
            },
          },
        },
      },
      {
        $addFields: {
          age: {
            $divide: [
              { $subtract: [new Date(), '$dob'] },
              365 * 24 * 60 * 60 * 1000,
            ],
          },
        },
      },
      {
        $addFields: {
          ageToCalculate: { $ceil: '$age' },
          ageToCaslculate: { $floor: '$age' },
        },
      },
      {
        $addFields: {
          ageToCalculate: { $toString: '$ageToCalculate' },
          ageToCaslculate: { $toString: '$ageToCaslculate' },
        },
      },
      {
        $addFields: {
          birthday: { $concat: ['$ageToCalculate', ' ', '$month'] },
          birthday2: { $concat: ['$ageToCaslculate', ' ', '$month'] },
        },
      },
      {
        $lookup: {
          from: 'profession',
          localField: 'profession',
          foreignField: '_id',
          as: 'profession',
        },
      },
      {
        $facet: {
          paginatedResults: [
            { $skip: pageSize * (pageNo - 1) },
            { $limit: pageSize },
          ],
          totalCount: [
            {
              $count: 'count',
            },
          ],
        },
      },
    ]);
  }

  currentProvince(data) {
    let dataToSendFurther = '^' + data;
    return this.model.aggregate([
      {
        $match: {
          status: 'Live',
          currPForfiter: { $regex: dataToSendFurther, $options: 'si' },
        },
      },
    ]);
  }

  resultByCurrentProvince(data, queryToSend) {
    let dataToSendFurther = '^' + data;
    let pageNo = parseInt(queryToSend.page) || 1;
    let pageSize = parseInt(queryToSend.limit) || 10;
    if (pageNo <= 0) {
      return;
    }
    return this.model.aggregate([
      {
        $match: {
          status: 'Live',
          currPForfiter: { $regex: dataToSendFurther, $options: 'si' },
        },
      },
      {
        $addFields: {
          month: { $month: '$dob' },
        },
      },
      {
        $addFields: {
          month: {
            $let: {
              vars: {
                monthsInString: [
                  'January',
                  'January',
                  'February',
                  'March',
                  'April',
                  'May',
                  'June',
                  'July',
                  'August',
                  'September',
                  'October',
                  'November',
                  'December',
                ],
              },
              in: {
                $arrayElemAt: ['$$monthsInString', '$month'],
              },
            },
          },
        },
      },
      {
        $addFields: {
          age: {
            $divide: [
              { $subtract: [new Date(), '$dob'] },
              365 * 24 * 60 * 60 * 1000,
            ],
          },
        },
      },
      {
        $addFields: {
          ageToCalculate: { $ceil: '$age' },
          ageToCaslculate: { $floor: '$age' },
        },
      },
      {
        $addFields: {
          ageToCalculate: { $toString: '$ageToCalculate' },
          ageToCaslculate: { $toString: '$ageToCaslculate' },
        },
      },
      {
        $addFields: {
          birthday: { $concat: ['$ageToCalculate', ' ', '$month'] },
          birthday2: { $concat: ['$ageToCaslculate', ' ', '$month'] },
        },
      },
      {
        $lookup: {
          from: 'profession',
          localField: 'profession',
          foreignField: '_id',
          as: 'profession',
        },
      },
      {
        $facet: {
          paginatedResults: [
            { $skip: pageSize * (pageNo - 1) },
            { $limit: pageSize },
          ],
          totalCount: [
            {
              $count: 'count',
            },
          ],
        },
      },
    ]);
  }
}
