

const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
import httpContext from "express-http-context";
import logger from "morgan";
import mongoose from "mongoose";
import http from "http";
import config from "./config";
import cors from "cors";
import indexRouter from "./routes";

const app = express();
app.use(cors());
const port = config.server.port;

const server = http.createServer(app);

app.use(httpContext.middleware);
app.use((req, res, next) => {
  httpContext.ns.bindEmitter(req);
  httpContext.ns.bindEmitter(res);
  next();
});
app.use(logger("dev"));
app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({limit: '50mb', extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use("/uploads", express.static(path.join(__dirname, "/uploads/")));

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

app.use((err, req, res, next) => {
  const status = err.status || 500;
  if (status === 500 && process.env.NODE_ENV !== "prod") {
  }

  res.status(status).send({
    message: status === 500 ? "Internal server errors." : err.message,
  });
});

server.listen(port, () => {
  //connect to mongoDB
  const connectionStr = `${config.database.db_uri}/${config.database.db_name}`;
  mongoose
    .connect(connectionStr, {
      useCreateIndex: true,
      useNewUrlParser: true,
      useUnifiedTopology: true,
      autoIndex: false 
    })
    .catch((err) => console.log(err.reason));

  const db = mongoose.connection;
  db.on("error", (err) => {
    console.error("> error occurred from the database");
    console.error(err);
  });
  db.once('open', () => {
    console.log('database connected');
  });

  indexRouter(app);
});
console.log("app started on", port);
