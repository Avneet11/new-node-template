

import { Context } from '../helpers/context';
import * as message from '../core/responseMessages';
import { User } from '../components/auth-manager/model';
const jwt = require('jsonwebtoken');
const context = new Context();

const Auth = {
  /**
   * Verify Token
   * @param {object} req
   * @param {object} res
   * @param {object} next
   * @returns {object|void} response object
   */
  async verifyToken(req, res, next) {
    const token = req.headers['authorization'];
    if (!token) {
      res.status(404).send({
        success: false,
        message: message.TOKEN_NOT_PROVIDED
      });
      return false;
    }

    try {
      const secret = process.env.JWT_SECRET || 'Development';
      jwt.verify(token, secret, async (err, decoded) => {
        if (decoded) {
          context.currentUser = decoded;
          let checkValid = await User.findOne({ email: decoded.email });
          if (!checkValid) {
            res.status(204).send({
              success: false,
              message: message.NO_USER_FOUND
            });
            return false;
          }
          return next();
        } else {
          res.status(401).send({
            success: false,
            message: message.INVALID_TOKEN
          });
        }
      });
    } catch (err) {
      res.status(401).send({
        success: false,
        message: message.INVALID_TOKEN
      });
    }
  }
};

module.exports = { Auth };
