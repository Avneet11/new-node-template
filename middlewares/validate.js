

import { UserUsecase } from "../components/auth-manager/usecase";
const { checkSchema } = require("express-validator");
const userUsecase = new UserUsecase();
const baseSchema = {
  email: {
    in: ["params"],
    isEmail: {
      errorMessage: "Enter Valid Email"
    },
    required: true
  }
};
const checkEmail = {
  custom: {
    options: async (value, { req }) => {
      await userUsecase.checkEmail("email", value, req.params.email);
      return value;
    }
  }
};

const UserRegisterCheck = checkSchema(baseSchema);
const EmailCheck = checkSchema(checkEmail);

module.exports = {
  UserRegisterCheck,
  EmailCheck
};
