

const path = require('path');

const flatCache = require('flat-cache');
const jwt = require('jsonwebtoken');

export const Common = {
  /**
   * Hash Password Method
   * @param {string} password
   * @returns {string} returns hashed password
   */

  createjwtToken(result) {
    let token_Data = {
      email: result.email,
      role: result.role,
      _id: result._id
    };
    return jwt.sign(token_Data, process.env.JWT_SECRET);
  },
  /**
   * Parse array to string
   * @param {array} array
   * @returns {string} token
   */
  parseArrayToString(array) {
    let string = '';
    array.forEach(element => {
      string += string === '' ? element.msg : '<br>' + element.msg;
    });
    return string;
  },
  /**
   * Convert any type string to uppercase the first letter "capitalize "
   * @param {string} string
   * @returns {string} string
   */
  capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  },
  /**
   * Set key and value from object to another object
   * @param {object} object
   * @param {object} response
   * @param {array} array
   * @returns {object} string
   */
  setKeyForObject(object, response, array) {
    if (array.length > 0) {
      array.forEach(key => {
        response[key] = object[key];
      });
    }
    return response;
  }
};
