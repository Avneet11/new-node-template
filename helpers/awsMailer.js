

'use strict';
/**
 * require modules
 */
const config = require('../config/index');

const ses = require('node-ses'),
  client = ses.createClient({
    key: config.KEY,
    secret: config.SECRET_KEY,
  });

module.exports = {
  sendMail(to, subject, html) {
    console.log(process.env.EMAILUSER,"process.env.EMAILUSER")
    client.sendEmail(
      {
        to: to.toString(),
        from: process.env.EMAILUSER.toString(),
        subject: subject,
        message: html,
        altText: 'plain text',
      },
      async (err, resp, data) => {
        if (resp) {
          return 'Email Sent Successfully';
        } else {
          return err;
        }
      }
    );
  },

  userRequestHtml(userName, id, type) {
    return `<html>
      <body>
      <h1> Hello ${userName}</h1>
      <p>Your Request To ${type} Profile with id ${id} is Sent Sucessfully to Admin</p>
      </body>
      </html>`;
  },

  adminHtml(userName, id, type) {
    return `<html>
      <body>
      <h1> Hello</h1>
      <p> ${userName} has requested to ${type} Profile with id ${id}</p>
      <p> Kindly Check</p>
      </body>
      </html>`;
  },

  freeHtml(userName, id, type) {
    return `<html>
      <body>
      <h1> Hello</h1>
      <p> User having promocode ${userName} has requested to ${type} Profile with id ${id}</p>
      <p> Kindly Check</p>
      </body>
      </html>`;
  },

  newUserRequestHtml(userName, id, email, type) {
    return `<html>
      <body>
      <h1> Hello ${userName}</h1>
      <p>Your Request To ${type} Profile with id ${id} is Sent Sucessfully to Admin</p>
      <h3>For Login To Site Credentials are</h3>
      <p>Email : ${email}</p>
      <p>Password : user@123</p>
      </body>
      </html>`;
  },

  newUserRegisterHtml(name, email, password) {
    return `<html>
      <body>
      <h1> Hello ${name}</h1>
      <p>Your Account is Registered and Sent Sucessfully to Admin For Verification</p>
      <h3>For Login To Site Credentials are</h3>
      <p>Email : ${email}</p>
      <p>Password : ${password}</p>
      </body>
      </html>`;
  },

  SubscribeHtml(email) {
    return `<html>
      <body>
      <h3> Hello ${email}</h3>
      <p>Thank you for Subscribing Before They Were Famous</p>
      </body>
      </html>`;
  },
};
