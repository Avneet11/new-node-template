

const Joi = require('joi');

const login = {
  body: Joi.object({
    email: Joi.string().email().required(),
    password: Joi.string().required(),
  }).unknown(false),
};

const register = {
  body: Joi.object({
    email: Joi.string().email().required(),
    password: Joi.string().required(),
    isAdmin: Joi.boolean().optional(),
  }).unknown(true),
};

const contact = {
  body: Joi.object({
    email: Joi.string().email().required(),
    fullName: Joi.string().required(),
    mobile: Joi.string().optional(),
    comment: Joi.string().required(),
  }).unknown(false),
};

const contentPage = {
  body: Joi.object({
    title: Joi.any().required(),
    tag: Joi.any().optional(),
    description: Joi.any().required(),
    subHeading: Joi.any().required(),
  }).unknown(true),
};

const contentType = {
  query: Joi.object({
    type: Joi.any().required(),
    page: Joi.any().optional(),
    limit: Joi.any().optional(),
  }).unknown(true),
};

const city = {
  query: Joi.object({
    search: Joi.string().min(3).required(),
    page: Joi.any().optional(),
    limit: Joi.any().optional(),
  }).unknown(false),
};

const searchProfile = {
  query: Joi.any().required(),
};

const staticPage = {
  body: Joi.object({
    pageType: Joi.string().required(),
    title: Joi.string().optional(),
    description: Joi.string().optional(),
  }).unknown(false),
};

const btwf = {
  body: Joi.object({
    userId: Joi.string().min(24).max(24).required(),
  }).unknown(false),
};

const searchNo = {
  query: Joi.object({
    key: Joi.string().required(),
    value: Joi.string().required(),
    page: Joi.any().optional(),
    limit: Joi.any().optional(),
  }).unknown(false),
};

const enableDisable = {
  body: Joi.object({
    dataToDisable: Joi.object().required(),
  }).unknown(false),
};

module.exports = {
  login,
  register,
  contentPage,
  contentType,
  city,
  btwf,
  contact,
  searchNo,
  enableDisable,
  searchProfile,
  staticPage,
};
