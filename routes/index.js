import { ProfessionHandler } from '../components/profession/handler';
import { DatabaseHandler } from '../components/databasePage/handler';
import { AuthHandler } from '../components/auth-manager/handler';
import { GroupHandler } from '../components/group/handler';
import { MediaHandler } from '../components/media/handler';
import { ProfileHandler } from '../components/profile/handler';
import { IndustryHandler } from '../components/industry/handler';
import { Auth } from '../middlewares/auth';
import Validation from './validation';
const { validate, ValidationError, Joi } = require('express-validation');

export default (app) => {
  app.get('/', (req, res) =>
    res.status(200).send({
      message: 'Inventory API!',
    })
  );

  app.get('/api', (req, res) =>
    res.status(200).send({
      message: 'Welcome to the Inventory API!',
    })
  );

  /* For authentication */
  const authHandler = new AuthHandler();
  app.post('/register', authHandler.register.bind(authHandler));
  app.put(
    '/accountSetting',
    Auth.verifyToken,
    authHandler.accountSetting.bind(authHandler)
  );
  app.put(
    '/login',
    validate(Validation.login, {
      context: false,
      statusCode: 200,
      keyByField: true,
    }),
    authHandler.login.bind(authHandler)
  );
  app.get('/user', Auth.verifyToken, authHandler.getUser.bind(authHandler));
  app.get(
    '/user/:id',
    Auth.verifyToken,
    authHandler.getUserById.bind(authHandler)
  );
  app.put(
    '/user/:id',
    Auth.verifyToken,
    authHandler.userUpdate.bind(authHandler)
  );
  app.get('/users', Auth.verifyToken, authHandler.getUsers.bind(authHandler));
  app.delete(
    '/user/:id',
    Auth.verifyToken,
    authHandler.deleteUser.bind(authHandler)
  );


  /* For group */
  const groupHandler = new GroupHandler();
  app.post(
    '/group',
    Auth.verifyToken,
    groupHandler.addGroup.bind(groupHandler)
  );
  app.put(
    '/group/:id',
    Auth.verifyToken,
    groupHandler.editGroup.bind(groupHandler)
  );
  app.get('/group/:id', groupHandler.getGroup.bind(groupHandler));
  app.get(
    '/group',
    Auth.verifyToken,
    groupHandler.getAllGroup.bind(groupHandler)
  );
  app.get(
    '/groups',
    Auth.verifyToken,
    groupHandler.getGroups.bind(groupHandler)
  );
  app.get('/groupUser', groupHandler.groupUser.bind(groupHandler));
  app.delete(
    '/group/:id',
    Auth.verifyToken,
    groupHandler.deleteGroup.bind(groupHandler)
  );
  app.get('/groupWithType', groupHandler.getGroupWithType.bind(groupHandler));
  app.put(
    '/btwfScoreOfGroup/:id',
    validate(Validation.btwf, {
      context: false,
      statusCode: 200,
      keyByField: true,
    }),
    groupHandler.updateBtwfScoreGroup.bind(groupHandler)
  );

  //Media
  const mediaHandler = new MediaHandler();
  app.post(
    '/content-page/addMedia',
    Auth.verifyToken,
    mediaHandler.addMedia.bind(mediaHandler)
  );
  app.get(
    '/content-page/getAllMedia',
    Auth.verifyToken,
    mediaHandler.getAllMedia.bind(mediaHandler)
  );
  app.put(
    '/content-page/updateMedia/:id',
    Auth.verifyToken,
    mediaHandler.updateMedia.bind(mediaHandler)
  );
  app.delete(
    '/content-page/deleteMedia/:id',
    Auth.verifyToken,
    mediaHandler.deleteMedia.bind(mediaHandler)
  );

  /* For Industry */
  const industryHandler = new IndustryHandler();
  app.post(
    '/industry',
    Auth.verifyToken,
    industryHandler.addIndustry.bind(industryHandler)
  );
  app.put(
    '/industry/:id',
    Auth.verifyToken,
    industryHandler.editIndustry.bind(industryHandler)
  );
  app.get(
    '/industry/:id',
    Auth.verifyToken,
    industryHandler.getIndustry.bind(industryHandler)
  );
  app.get('/industry', industryHandler.getIndustryList.bind(industryHandler));
  app.delete(
    '/industry/:id',
    Auth.verifyToken,
    industryHandler.deleteIndustry.bind(industryHandler)
  );
  app.get(
    '/industryPagination',
    Auth.verifyToken,
    industryHandler.getIndustryPagination.bind(industryHandler)
  );
  app.get(
    '/industryAccToProfession',
    industryHandler.getIndustryAccToProfession.bind(industryHandler)
  );

  /* For Profile */
  const profileHandler = new ProfileHandler();
  app.post(
    '/profile',
    Auth.verifyToken,
    profileHandler.addProfile.bind(profileHandler)
  );
  app.put(
    '/profile/:id',
    Auth.verifyToken,
    profileHandler.editProfile.bind(profileHandler)
  );
  app.get('/profile/:id', profileHandler.getProfile.bind(profileHandler));
  app.get('/profile', profileHandler.getAllProfile.bind(profileHandler));
  app.get(
    '/profileAccBtwf',
    profileHandler.getProfileAccBtwf.bind(profileHandler)
  );
  app.get('/allProfile', profileHandler.allProfile.bind(profileHandler));
  app.get(
    '/similarProfile/:id',
    profileHandler.getProfileAccToSimilar.bind(profileHandler)
  );
  app.get('/accToAge', profileHandler.accToAge.bind(profileHandler));
  app.get('/accToResult', profileHandler.accToResult.bind(profileHandler));
  app.get(
    '/recentProfile',
    profileHandler.getRecentProfile.bind(profileHandler)
  );
  app.get(
    '/spanishProfile',
    profileHandler.getAllProfileLang.bind(profileHandler)
  );
  app.delete(
    '/profile/:id',
    Auth.verifyToken,
    profileHandler.deleteProfile.bind(profileHandler)
  );
  app.get(
    '/profileFilter',
    profileHandler.getAllProfileFilter.bind(profileHandler)
  );
  app.get(
    '/searchProfile',
    validate(Validation.city, {
      context: false,
      statusCode: 200,
      keyByField: true,
    }),
    profileHandler.searchProfile.bind(profileHandler)
  );
  app.get('/profileGroup', profileHandler.getProfileGroup.bind(profileHandler));
  app.get('/netWorth', profileHandler.netWorth.bind(profileHandler));
  app.get(
    '/netWorthFilter',
    profileHandler.netWorthFilter.bind(profileHandler)
  );
  app.get(
    '/houseTourFamousList',
    profileHandler.houseTourFamousList.bind(profileHandler)
  );
  app.put(
    '/updateBtwfScore/:id',
    validate(Validation.btwf, {
      context: false,
      statusCode: 200,
      keyByField: true,
    }),
    profileHandler.updateBtwfScore.bind(profileHandler)
  );
  app.get(
    '/profileAccToProfession',
    profileHandler.profileAccToProfession.bind(profileHandler)
  );
  app.get(
    '/checkRandomNumber',
    profileHandler.checkRandomNumber.bind(profileHandler)
  );
  app.post(
    '/profileByUser',
    profileHandler.addProfileByUser.bind(profileHandler)
  );
  app.put(
    '/profileByUser',
    profileHandler.editProfileByUser.bind(profileHandler)
  );
  app.get(
    '/requestList',
    Auth.verifyToken,
    profileHandler.requestList.bind(profileHandler)
  );
  app.get(
    '/userRequest',
    Auth.verifyToken,
    profileHandler.userRequest.bind(profileHandler)
  );
  app.put(
    '/requestStatus',
    Auth.verifyToken,
    profileHandler.editRequestStatus.bind(profileHandler)
  );

  /* For Database Pages */
  const databaseHandler = new DatabaseHandler();
  app.post(
    '/database',
    Auth.verifyToken,
    databaseHandler.addDatabase.bind(databaseHandler)
  );
  app.put(
    '/database/:id',
    Auth.verifyToken,
    databaseHandler.updateDatabase.bind(databaseHandler)
  );
  app.get(
    '/database/:id',
    Auth.verifyToken,
    databaseHandler.getDatabase.bind(databaseHandler)
  );
  app.get(
    '/searchNumber',
    Auth.verifyToken,
    validate(Validation.searchNo, {
      context: false,
      statusCode: 200,
      keyByField: true,
    }),
    databaseHandler.getSearchNumber.bind(databaseHandler)
  );
  app.put(
    '/enableDisable',
    Auth.verifyToken,
    databaseHandler.updateEnableDisable.bind(databaseHandler)
  );
  app.get(
    '/enableDisable',
    Auth.verifyToken,
    databaseHandler.getEnableDisableList.bind(databaseHandler)
  );
  app.get(
    '/database',
    Auth.verifyToken,
    databaseHandler.getDatabaseList.bind(databaseHandler)
  );
  app.delete(
    '/database/:id',
    Auth.verifyToken,
    databaseHandler.deleteDatabase.bind(databaseHandler)
  );
  app.put(
    '/databaseStatus/:id',
    Auth.verifyToken,
    databaseHandler.updateDatabaseStatus.bind(databaseHandler)
  );

  /* For Profession Pages */
  const professionHandler = new ProfessionHandler();
  app.post(
    '/profession',
    Auth.verifyToken,
    professionHandler.addProfession.bind(professionHandler)
  );
  app.put(
    '/profession/:id',
    Auth.verifyToken,
    professionHandler.updateProfession.bind(professionHandler)
  );
  app.get(
    '/profession/:id',
    Auth.verifyToken,
    professionHandler.getProfession.bind(professionHandler)
  );
  app.get(
    '/profession',
    professionHandler.getProfessionList.bind(professionHandler)
  );
  app.delete(
    '/profession/:id',
    Auth.verifyToken,
    professionHandler.deleteProfession.bind(professionHandler)
  );
  app.get(
    '/professionPagination',
    Auth.verifyToken,
    professionHandler.getProfessionPagination.bind(professionHandler)
  );

  app.get(
    '/allCities',
    validate(Validation.city, {
      context: false,
      statusCode: 200,
      keyByField: true,
    }),
    mediaHandler.allCities.bind(mediaHandler)
  );

  app.use((err, req, res, next) => {
    if (err instanceof ValidationError) {
      return res.status(err.statusCode).json({ message: err.details[0] });
    }
    return res.status(500).json(err);
  });


};
